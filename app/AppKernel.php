<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new \FOS\UserBundle\FOSUserBundle(),
            new \Greetik\BEInterfaceBundle\BEInterfaceBundle(),
            new \Greetik\DataimageBundle\DataimageBundle(),
            new \Greetik\YtvideoBundle\YtvideoBundle(),
            new \Greetik\SystemuserBundle\SystemuserBundle(),
            new Fresh\DoctrineEnumBundle\FreshDoctrineEnumBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new Greetik\FarmBundle\FarmBundle(),
            new Greetik\BlogBundle\BlogBundle(),
            new Greetik\EventsBundle\EventsBundle(),
            new Greetik\ItemlinkBundle\ItemlinkBundle(),
            new Greetik\TreesectionBundle\TreesectionBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Greetik\WebmodulesBundle\WebmodulesBundle(),
            new Greetik\WebformsBundle\WebformsBundle(),
            new \Greetik\CatalogBundle\CatalogBundle(),
            new \Greetik\GmapBundle\GmapBundle(),
            new \Greetik\PublicwebBundle\PublicwebBundle(),
            new Dizda\CloudBackupBundle\DizdaCloudBackupBundle(),
            new Greetik\WeightcontrolBundle\WeightcontrolBundle(),
            new Greetik\VisitsBundle\VisitsBundle(),
            new \Greetik\SystemlogBundle\SystemlogBundle
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
