<?php
namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class VisitanimalType extends AbstractEnumType
{
    const RU = 1;
    const CAL = 2;
    const BAJA = 3;
    const ALTA = 4;
    const FAIR = 5;
    const WEIGHT = 6;
    
    
    protected static $choices = [
        self::RU => 'Registro',
        self::CAL => 'Calificación',
        self::BAJA => 'Baja',
        self::ALTA => 'Alta',
        self::FAIR => 'Feria',
        self::WEIGHT => 'Peso'
    ];
    
}