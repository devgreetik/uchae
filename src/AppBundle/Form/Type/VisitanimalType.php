<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VisitsType
 *
 * @author Paco
 */
class VisitanimalType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $animals = array();
        foreach ($options['_animals'] as $k => $v) {
            $animals[$v['text']] = $v['id'];
        }

        $fairs = array();
        foreach ($options['_fairs'] as $k => $v) {
            $fairs[$v['name'].' ('.$v['fairdate']->format('d/m/Y').')'] = $v['id'];
        }
        
        if (!empty($options['_animal'])){
            $dataanimal = array('data'=>$options['_animal']);
        }else $dataanimal = array();

        if (!empty($options['_action'])){
            $dataaction = array('data'=>$options['_action']);
        }else $dataaction = array();

        if (count($animals) > 0) {
            $builder->add('animal', ChoiceType::class, array_merge($dataanimal, array('choices' => array_merge(array('Nuevo'=>''), $animals))))
            ->add('name')
            ->add('tattoo')
            ->add('birthdate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy'));
        }
        $builder
                ->add('enddate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy'))
                ->add('visitaction', ChoiceType::class, array_merge($dataaction, array('mapped'=>false, 'multiple'=>true, 'choices'=> \AppBundle\DBAL\Types\VisitanimalType::getChoices())))
                ->add('points')
                ->add('weight')
                ->add('register')
                ->add('tofair', ChoiceType::class, array('choices' => array_merge(array('-'=>''), $fairs)))
        ;
    }

    public function getName() {
        return 'Visitanimal';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array('data_class' => 'AppBundle\Entity\Visitanimal',
            '_animals' => array(),
            '_fairs'=>array(),
            '_animal'=>'',
            '_action'=>array()
        ));
    }

}
