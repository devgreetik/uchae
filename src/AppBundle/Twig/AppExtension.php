<?php

namespace AppBundle\Twig;

use AppBundle\DBAL\Types\WebmoduleType;
use Greetik\FarmBundle\DBAL\Types\AnimalgenderType;

class AppExtension extends \Twig_Extension {

    private $systemusers;

    public function __construct($_systemusers) {
        $this->systemusers = $_systemusers;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('isFarmer', array($this, 'isFarmer')), //Filtro para saber si un usuario es farmer (La vista de perfil será distinta)
            new \Twig_SimpleFilter('getAbbGender', array($this, 'getAbbGender')), //Filtro para saber si un usuario es farmer (La vista de perfil será distinta)
            new \Twig_SimpleFilter('getIconGender', array($this, 'getIconGender'))//get the icon of the gender of the animal
        );
    }


    /*Devuelve si el usuario es farmer*/
    public function isFarmer($user){
        return $this->systemusers->isLowUser($user);
    }
    
    //get the icon of the gender
    public function getIconGender($gender){
        return AnimalgenderType::getIcon($gender);
    }
    
    //get the abb of the gender
    public function getAbbGender($gender){
        return AnimalgenderType::getAbb($gender);
    }
    
    
    public function getName() {
        return 'app_extension';
    }

}
