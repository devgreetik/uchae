<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Visitanimal
 *
 * @ORM\Table(name="visitanimal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VisitanimalRepository")
 */
class Visitanimal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="animal", type="integer")
     */
    private $animal;

    /**
     * @var int
     *
     * @ORM\Column(name="visitaction", type="VisitanimalType", nullable=true)
     */
    private $visitaction;

    /**
     * @var int
     *
     * @ORM\Column(name="visit", type="integer")
     */
    private $visit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enddate", type="datetime", nullable=true)
     */
    private $enddate;

    /**
     * @var float
     *
     * @ORM\Column(name="points", type="float", nullable=true)
     */
    private $points;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="register", type="string", length=255, nullable=true)
     */
    private $register;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="tattoo", type="string", length=255, nullable=true)
     */
    private $tattoo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="datetime", nullable=true)
     */
    private $birthdate;


    /**
     * @var int
     *
     * @ORM\Column(name="tofair", type="integer", nullable=true)
     */
    private $tofair;

    /**
     * @var boolean
     *
     * @ORM\Column(name="indexanimal", type="boolean", nullable=true)
     */
    private $indexanimal;

    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set animal
     *
     * @param integer $animal
     *
     * @return Visitanimal
     */
    public function setAnimal($animal)
    {
        $this->animal = $animal;

        return $this;
    }

    /**
     * Get animal
     *
     * @return int
     */
    public function getAnimal()
    {
        return $this->animal;
    }

    /**
     * Set visit
     *
     * @param integer $visit
     *
     * @return Visitanimal
     */
    public function setVisit($visit)
    {
        $this->visit = $visit;

        return $this;
    }

    /**
     * Get visit
     *
     * @return int
     */
    public function getVisit()
    {
        return $this->visit;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Visitanimal
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set points
     *
     * @param float $points
     *
     * @return Visitanimal
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return float
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Visitanimal
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set register
     *
     * @param string $register
     *
     * @return Visitanimal
     */
    public function setRegister($register)
    {
        $this->register = $register;

        return $this;
    }

    /**
     * Get register
     *
     * @return string
     */
    public function getRegister()
    {
        return $this->register;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Visitanimal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return Visitanimal
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set tattoo
     *
     * @param string $tattoo
     *
     * @return Visitanimal
     */
    public function setTattoo($tattoo)
    {
        $this->tattoo = $tattoo;

        return $this;
    }

    /**
     * Get tattoo
     *
     * @return string
     */
    public function getTattoo()
    {
        return $this->tattoo;
    }

    /**
     * Set tofair
     *
     * @param integer $tofair
     *
     * @return Visitanimal
     */
    public function setTofair($tofair)
    {
        $this->tofair = $tofair;

        return $this;
    }

    /**
     * Get tofair
     *
     * @return integer
     */
    public function getTofair()
    {
        return $this->tofair;
    }

    /**
     * Set visitaction
     *
     * @param VisitanimalType $visitaction
     *
     * @return Visitanimal
     */
    public function setVisitaction($visitaction)
    {
        $this->visitaction = $visitaction;

        return $this;
    }

    /**
     * Get visitaction
     *
     * @return VisitanimalType
     */
    public function getVisitaction()
    {
        return $this->visitaction;
    }

    /**
     * Set indexanimal
     *
     * @param boolean $indexanimal
     *
     * @return Visitanimal
     */
    public function setIndexanimal($indexanimal)
    {
        $this->indexanimal = $indexanimal;

        return $this;
    }

    /**
     * Get indexanimal
     *
     * @return boolean
     */
    public function getIndexanimal()
    {
        return $this->indexanimal;
    }
}
