<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\SystemuserBundle\Form\Type\RegistrationFormType;
use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Greetik\FarmBundle\Form\Type\FarmpartialType;
use Greetik\FarmBundle\Form\Type\AnimalPartialType;

class SystemuserController extends Controller {
    /* ver el perfil del usuario */

    public function myuserAction() {
        $systemuserobject = $this->getUser();
        $systemuser = $this->get('app.systemusers')->getSystemuser($systemuserobject->getId());

        $editForm = $this->createForm(RegistrationFormType::class, $systemuserobject, array('_role' => $systemuser['type'], '_roles' => $this->get('systemuser.tools')->getRolesAccess($this->getUser()->getRoles(), true)));
        $editfarmForm = $this->createForm(FarmpartialType::class, $this->get('farms.tools')->getFarmObject($systemuserobject->getFarm()));
        $changepassform = $this->createForm(ChangePasswordFormType::class);

        return $this->render('AppBundle:Systemuser:view.html.twig', array(
                    'item' => $systemuser,
                    'new_form' => $editForm->createView(),
                    'new_form_farm' => $editfarmForm->createView(),
                    'new_form_pass' => $changepassform->createView(),
                    'modifyAllow' => false
        ));
    }

    /* ver los datos de un usuario */

    public function viewAction($id) {
        $systemuserobject = $this->get('systemuser.tools')->getSystemuserObject($id);

        $editForm = $this->createForm(RegistrationFormType::class, $systemuserobject, array('_farms' => $this->get('farms.tools')->getFarms(true), '_role' => $this->get('systemuser.tools')->grantrole($systemuserobject->getRoles()), '_roles' => $this->get('systemuser.tools')->getRolesAccess($this->getUser()->getRoles(), true)));
        $editfarmForm = $this->createForm(FarmpartialType::class, $this->get('farms.tools')->getFarmObject($systemuserobject->getFarm()));
        $changepassform = $this->createForm(ChangePasswordFormType::class);


        $systemuser = $this->get('app.systemusers')->getSystemuser($id);
        return $this->render('AppBundle:Systemuser:view.html.twig', array(
                    'item' => $systemuser,
                    'new_form' => $editForm->createView(),
                    'new_form_farm' => $editfarmForm->createView(),
                    'new_form_pass' => $changepassform->createView(),
                    'modifyAllow' => $this->get('systemuser.tools')->getSystemuserPerm('modify', $systemuserobject)
        ));
    }

    /* modificar los datos del usuario conectado */

    public function modifymyuserAction(Request $request) {
        $systemuserobject = $this->getUser();
        $farm = $systemuserobject->getFarm();
        $roles = $systemuserobject->getRoles();
        $editForm = $this->createForm(RegistrationFormType::class, $systemuserobject, array());

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $systemuserobject->setFarm($farm);
                    $systemuserobject->setRoles($roles);
                    $this->get('systemuser.tools')->modifySystemuser($systemuserobject);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage() . ' ' . $e->getLine() . '->' . $e->getFile());
                }
                return $this->redirect($this->generateUrl('fos_user_profile_show'));
            }
        }

        $this->addFlash('error', (string) $editForm->getErrors(true, false));
        return $this->redirect($this->generateUrl('fos_user_profile_show'));
    }

    /* modificar los datos de la ganadería del usuario conectado */

    public function modifymyfarmAction(Request $request) {
        $farmobject = $this->get('farms.tools')->getFarmObject($this->getUser()->getFarm());
        $oldfarm = $this->get('farms.tools')->getFarm($this->getUser()->getFarm());
        $editForm = $this->createForm(FarmpartialType::class, $farmobject, array());

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get('app.farms')->modifyFarm($farmobject, $oldfarm);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage() );
                }
                return $this->redirect($this->generateUrl('fos_user_profile_show'));
            }
        }

        $this->addFlash('error', (string) $editForm->getErrors(true, false));
        return $this->redirect($this->generateUrl('fos_user_profile_show'));
    }

    /* cambia el logo de la ganadería */

    public function changemyfarmavatarAction(Request $request) {
        try {
            $this->get('app.farms')->changeAvatar($request->files->all()['avatar'], $this->getUser()->getFarm());
        } catch (\Exception $e) {
            if ($e->getCode() == 11)
                $this->addFlash('warning', $e->getMessage());
            else
                $this->addFlash('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('fos_user_profile_show'));
    }

    /* modificar los datos de un animal del usuario conectado */

    public function modifymyanimalAction(Request $request, $id) {
        $farmobject = $this->get('farms.tools')->getFarmObject($this->getUser()->getFarm());
        foreach ($farmobject->getAnimals() as $k => $v) {
            if ($id == $v->getId()) {
                $animalObject = $v;
                break;
            }
        }
        if (!isset($animalObject)) {
            $this->addFlash('error', 'No se encontró el animal para modificar');
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }

        $editForm = $this->createForm(AnimalPartialType::class, $animalObject);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
               try {
                    $this->get('app.farms')->modifyAnimalPartial($animalObject);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }else{
                $this->addFlash('error', (string) $editForm->getErrors(true, true));        
            }
        }else{
            $this->addFlash('error', 'Operación no Permitida');                
        }
        return $this->redirect($this->generateUrl('farm_viewanimal', array('id'=>$id)));
    }

}
