<?php

namespace AppBundle\Controller;

use AppBundle\DBAL\Types\ProvinceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Greetik\SystemuserBundle\DBAL\Types\SpecialtyType;
use AppBundle\DBAL\Types\ShiftspecialtyType;
use AppBundle\Form\filterReportResidentes;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ReportsController extends Controller {

    /*Pantalla de informes preconfigurados*/
    public function indexAction() {
        return $this->render('AppBundle:Reports:index.html.twig', array());
    }
    
    
    /* INFORME "de animales" */

    public function animalsAction(Request $request) {
        ini_set('memory_limit', '2048M');

        if ($request->getMethod() != 'POST') {
            return $this->render('AppBundle:Reports:animals.html.twig', array(
                        'insertAllow' => $this->get($this->getParameter('farm.permsservice'))->getFarmPerm('insert')
            ));
        }
        $data = $this->get($this->getParameter('farm.permsservice'))->getAnimalsReportTable($request->get('exportoexcel'), $request->get('sSearch'), $request->get('iSortCol_0'), $request->get('sSortDir_0'), $request->get('exportoexcel') ? 0 : $request->get('iDisplayStart'), $request->get('exportoexcel') ? 0 : $request->get('iDisplayLength'), $request->get('farm'), $request->get('farmbirth'), $request->get('father'), $request->get('mother'), $request->get('birthfrom') ? $this->get('beinterface.tools')->dateFromPickDate($request->get('birthfrom')) : '', $request->get('birthto') ? $this->get('beinterface.tools')->dateFromPickDate($request->get('birthto')) : '', $request->get('sex'), $request->get('forsale'), $request->get('onlyactive'));
        $serializedEntity = $this->container->get('jms_serializer')->serialize(array_merge($data, array('sEcho' => intval($request->get('sEcho')))), 'json');

        if (!$request->get('exportoexcel')) {
            $serializedEntity = $this->container->get('jms_serializer')->serialize(array_merge($data, array('sEcho' => intval($request->get('sEcho')))), 'json');
            $response = new Response($serializedEntity);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        //throw new \Exception(count($data['aaData']));
        //exportar a Excel
        ini_set('max_execution_time', 300);
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator("Greetik")
                ->setLastModifiedBy($this->getParameter('projectnameabb'))
                ->setTitle("Animales " . date('d/m/Y'));

        $header = array('Tatuaje', 'Ganadería', 'Origen', 'Nombre', 'Crotal', 'Nacimiento', 'Venta', 'Precio', 'Baja', 'Padre', 'Padre Crotal', 'Padre Nombre', 'Padre Sexo', 'Madre', 'Madre Crotal', 'Madre Nombre', 'Madre Sexo', 'Sexo', 'RNU', 'RDU', 'Peso Nac.', 'Calificación', 'Estado', 'Número Parto', 'AC', 'AD', 'RN', 'AN', 'EPL', 'GC', 'LD', 'LP', 'AA', 'TMN', 'M', 'AAA', 'AAP', 'RD', 'CC', 'PP', 'AP', 'AT', 'LN');

        /*
          $body = array();
          foreach ($data as $row) {
          $newrow = array();
          foreach ($tableheader as $th => $v) {
          if (isset($row[$th]))
          $newrow[] = $row[$th];
          else
          $newrow[] = '0';
          }
          $body[] = $newrow;
          } */



        $this->get('beinterface.tools')->tableToExcel($phpExcelObject, $header, $data['aaData'], 'Informe Animales');

        $phpExcelObject->getActiveSheet()->setTitle('Informe Animales');
        $phpExcelObject->setActiveSheetIndex(0);

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'animales.xlsx'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    /* INFORME "de pesos" */

    public function weightsAction(Request $request) {
        ini_set('memory_limit', '4096M');
        ini_set('max_execution_time', 900);

        if ($request->getMethod() != 'POST') {
            return $this->render('AppBundle:Reports:weights.html.twig', array(
                        'insertAllow' => $this->get($this->getParameter('farm.permsservice'))->getFarmPerm('insert')
            ));
        }
        $data = $this->get($this->getParameter('farm.permsservice'))->getWeightsReportTable($request->get('exportoexcel')=='true',$request->get('exportoexcelgrouped')=='true', $request->get('sSearch'), $request->get('iSortCol_0'), $request->get('sSortDir_0'), $request->get('exportoexcel') ? 0 : $request->get('iDisplayStart'), $request->get('exportoexcel') ? 0 : $request->get('iDisplayLength'), $request->get('farm'), $request->get('datefrom') ? $this->get('beinterface.tools')->dateFromPickDate($request->get('datefrom')) : '', $request->get('dateto') ? $this->get('beinterface.tools')->dateFromPickDate($request->get('dateto')) : '', $request->get('sex'), $request->get('onlyactive'));
        $serializedEntity = $this->container->get('jms_serializer')->serialize(array_merge($data, array('sEcho' => intval($request->get('sEcho')))), 'json');

        if ($request->get('exportoexcel')!='true' && $request->get('exportoexcelgrouped')!='true') {
            $serializedEntity = $this->container->get('jms_serializer')->serialize(array_merge($data, array('sEcho' => intval($request->get('sEcho')))), 'json');
            $response = new Response($serializedEntity);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        //throw new \Exception(count($data['aaData']));
        //exportar a Excel
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator("Greetik")
                ->setLastModifiedBy($this->getParameter('projectnameabb'))
                ->setTitle("Pesadas " . date('d/m/Y'));

        if ($request->get('exportoexcel')=='true'){
            $header = array('Fecha', 'Peso', 'Nacimiento', 'Crotal', 'Tatuaje', 'RNU', 'RDU', 'Ganadería', 'Nombre', 'Sexo');
        }else{
            $header = array('Crotal', 'Tatuaje',  'Nombre', 'Sexo', 'Fecha Nacimiento', 'Peso Nacimiento', 'Fecha 1', 'Pesada 1', 'Fecha 2', 'Pesada 2', 'Fecha 3', 'Pesada 3', 'Fecha Calificación', 'AC', 'AD', 'RN', 'AN', 'EPL', 'GC', 'LD', 'LP', 'AA', 'TMN', 'M', 'AAA', 'AAP', 'RD', 'CC', 'PP', 'AP', 'AT', 'LN');
        }

        $this->get('beinterface.tools')->tableToExcel($phpExcelObject, $header, $data['aaData'], 'Informe de Pesadas');

        $phpExcelObject->getActiveSheet()->setTitle('Informe Animales');
        $phpExcelObject->setActiveSheetIndex(0);

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'pesadas.xlsx'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    /* Parte de cubriciones */

    public function cubricionesAction() {
        ini_set('memory_limit', '2048M');
        //exportar a Excel
        ini_set('max_execution_time', 300);

        //zip
        if (!file_exists('../var/cubriciones'))
            mkdir('../var/cubriciones');
        $filename = '../var/cubriciones/cubriciones.zip';
        @unlink($filename);


        //para cada ganadería activa
        $farms = $this->get('app.farms')->getFarms();
        foreach ($farms as $farm) {
            if ($farm['enddate'])
                continue;
            //hembras mayores de 18 meses a 31 de diciembre o a 31 de junio. Lo siguiente en el calendario, ya que se hace por anticipado.
            $today = new \Datetime();
            if ($today > new \Datetime(date('Y') . '-04-30') && $today < new \Datetime(date('Y').'-10-30')) {
                //Se hará con fecha de fin de mediado de año entre mayo y noviembre
                $startdate = new \Datetime('1900-01-01');
                $enddate = new \Datetime(date('Y') . '-06-30');
                $semester = 1;
            } else {
                //Se hará con fecha de fin de año entre noviembre y abril
                //A mitad de año
                $startdate = new \Datetime('1900-01-01');
                $enddate = new \Datetime((date('Y')-((date('M')<5)?1:0)) . '-12-31');//si los meses son de enero a abril se calcula con un año menos.
                $semester = 2;
            }
            $enddate->modify('-18 months');
            $enddate->modify('-1 day');
                
                
            $data = $this->get($this->getParameter('farm.permsservice'))->getAnimalsReportTable(1, '', '', '', 0, 999, $farm['id'], '', '', '', $startdate, $enddate, \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::HEMBRA, '', true);

            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('../web/bundles/app/xls/PARTECUBRICIONES.xls');
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D5', $farm['owner']);
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('G4', $farm['rega']);
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('J4', $farm['town']);
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('I5', $farm['abb']);
            if ($farm['province'])
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('K5', \AppBundle\DBAL\Types\ProvinceType::getReadableValue($farm['province']));

            $i = 9;
            $cont = 0;
            foreach ($data['aaData'] as $animal) {
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('B' . $i, $animal['5']);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('C' . $i, $animal['3']);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D' . $i, $animal['0']);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E' . $i, $animal['4']);
                $i++;
                $cont++;
            }

            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
            $writer->save('../var/cubriciones/cubriciones-' . $farm['abb'] . '.xlsx');

            //Añadir al zip
            $zip = new \ZipArchive();
            if ($zip->open($filename, \ZipArchive::CREATE) !== TRUE)
                throw new \Exception('no se pudo crear el zip');

            $this->get('app.print')->cubriciones($farm, $cont, 9, $semester);

            //return new BinaryFileResponse('../var/cubriciones/cubriciones-' . $farm['abb'] . '.pdf');

            if (file_exists('../var/cubriciones/cubriciones-' . $farm['abb'] . '.pdf'))
                $zip->addFile('../var/cubriciones/cubriciones-' . $farm['abb'] . '.pdf', 'cubriciones-' . $farm['abb'] . '.pdf');

            if (file_exists('../var/cubriciones/cubriciones-' . $farm['abb'] . '.xlsx'))
                $zip->addFile('../var/cubriciones/cubriciones-' . $farm['abb'] . '.xlsx', 'cubriciones-' . $farm['abb'] . '.xlsx');

            $zip->close();
        }

        return new BinaryFileResponse($filename);
    }

    /* Fichero de exportación para el ministerio */

    public function arcaAction() {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 300);
        $regions = ProvinceType::getRegions();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator("Uchae")
                ->setLastModifiedBy($this->getParameter('projectnameabb'))
                ->setTitle("ARCA " . date('d/m/Y'));

        $controws=1;
        //Encabezado
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('B1', 'Cantidad Ganaderías');
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('C1', 'RNU Hembras');
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D1', 'RNU Machos');
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E1', 'RDU Hembras');
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('F1', 'RDU Machos');
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('G1', 'Total Hembras');
        $phpExcelObject->setActiveSheetIndex(0)->setCellValue('H1', 'Total Machos');
        $phpExcelObject->getActiveSheet()
                ->getStyle('A'.$controws.':H'.$controws)
                ->getFill()
                ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('000000');
        $phpExcelObject->getActiveSheet()->getStyle('A'.$controws.':H'.$controws)->applyFromArray(array('font'=>array('color'=>array('rgb'=>'ffffff'))));
        
        foreach ($regions as $code => $name) {
            //if ($controws > 1)
              //  break;
            $controws++;
            $provinces = ProvinceType::getRegion($code);

            //Encabezado de la comunidad autónoma
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('A' . $controws, $name);
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('B' . $controws, '=SUM(B' . ($controws + 1) . ':B' . ($controws + count($provinces)) . ')');
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('C' . $controws, '=SUM(C' . ($controws + 1) . ':C' . ($controws + count($provinces)) . ')');
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D' . $controws, '=SUM(D' . ($controws + 1) . ':D' . ($controws + count($provinces)) . ')');
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E' . $controws, '=SUM(E' . ($controws + 1) . ':E' . ($controws + count($provinces)) . ')');
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('F' . $controws, '=SUM(F' . ($controws + 1) . ':F' . ($controws + count($provinces)) . ')');
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('G' . $controws, '=SUM(G' . ($controws + 1) . ':G' . ($controws + count($provinces)) . ')');
            $phpExcelObject->setActiveSheetIndex(0)->setCellValue('H' . $controws, '=SUM(H' . ($controws + 1) . ':H' . ($controws + count($provinces)) . ')');
            $phpExcelObject->getActiveSheet()
                    ->getStyle('A'.$controws.':H'.$controws)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('0c700f');
            $phpExcelObject->getActiveSheet()->getStyle('A'.$controws.':H'.$controws)->applyFromArray(array('font'=>array('color'=>array('rgb'=>'ffffff'))));

            foreach ($provinces as $province) {
                $controws++;
                $farms = $this->get('app.farms')->getPublicFarms('', 9999, $province);

                //Nombre de provincia y número de granjas
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('A' . $controws, ProvinceType::getReadableValue($province));
                
                list($num_farms, $num_females_d, $num_females_c, $num_males_d, $num_males_c) = array(0, 0, 0, 0, 0);
                foreach ($farms as $farm) {
                    if ($farm['enddate']) continue;
                    
                    $num_farms++;
                    
                    $female = $this->get($this->getParameter('farm.permsservice'))->getAnimalsReportTable(1, '', '', '', 0, 9999, $farm['id'], '', '', '', '', '', \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::HEMBRA, '', true);
                    foreach ($female['aaData'] as $animal) {
                        if ($animal[19])
                            $num_females_d++;
                        else
                            $num_females_c++;
                    }
                    $male = $this->get($this->getParameter('farm.permsservice'))->getAnimalsReportTable(1, '', '', '', 0, 9999, $farm['id'], '', '', '', '', '', \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::MACHO, '', true);
                    foreach ($male['aaData'] as $animal) {
                        if ($animal[19])
                            $num_males_d++;
                        else
                            $num_males_c++;
                    }
                }

                //Número de explotaciones
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('B' . $controws, $num_farms);
                //Conteo de animales por provincia
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('C' . $controws, $num_females_c);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D' . $controws, $num_males_c);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E' . $controws, $num_females_d);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('F' . $controws, $num_males_d);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('G' . $controws, '=SUM(C'.$controws.'+E'.$controws.')');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('H' . $controws, '=SUM(D'.$controws.'+F'.$controws.')');
            }
        }

        $phpExcelObject->getActiveSheet()->setTitle('ARCA');
        $phpExcelObject->setActiveSheetIndex(0);

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'arca.xlsx'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

}
