<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FarmerController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        if ($this->getUser()) {
            if ($this->get('systemuser.tools')->hasAccessToAdminPanel()) {
                return $this->render('AppBundle:_Default:index.html.twig');
            }

            //redirige al /usuario
            return $this->redirect($this->get('router')->generate('fos_user_profile_show'));
        }

        //redirige al /usuario
        return $this->redirect($this->get('router')->generate('fos_user_security_login'));
    }

  
    
}
