<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        if ($this->getUser()) {
            if ($this->get('systemuser.tools')->hasAccessToAdminPanel()) {
                if ($this->isGranted('ROLE_ADMIN')) {
                    $weights = $this->get('weightcontrol.tools')->getWeightcontrolByExtra('', 'animal', 'provisional');
                    foreach ($weights as $k => $v) {
                        $animal = $this->get('farms.tools')->getAnimal($v['itemid']);
                        $weights[$k]['animalname'] = $animal['name'];
                        $weights[$k]['animaltattoo'] = $animal['tattoo'];
                        $weights[$k]['farmname'] = $animal['farmdata']['name'];
                    }
                } else {
                    $weights = array();
                }

                $animalsprov = $this->get('app.farms')->getAnimals('', 0, 1, 0, 999, '', '', false, true);
                foreach($animalsprov as $k=>$v){
                    
                    $animalsprov[$k]['gendername'] = $v['gender'] ? \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::getReadableValue($v['gender']) : '-';
                    $father = $this->get('farms.tools')->getAnimal($v['father'], true);
                    $animalsprov[$k]['fathername'] = isset($father['name']) ? $father['name'] : '';
                    $mother = $this->get('farms.tools')->getAnimal($v['mother'], true);
                    $animalsprov[$k]['mothername'] = isset($mother['name']) ? $mother['name'] : '';
                }
                
                return $this->render('AppBundle:_Default:index.html.twig', array('weights' => $weights, 'animalsprov' => $animalsprov));
            }

            //redirige al /usuario
            return $this->redirect($this->get('router')->generate('fos_user_profile_show'));
        }

        //redirige al /usuario
        return $this->redirect($this->get('router')->generate('fos_user_security_login'));
    }

    /* Salvar los pesos */

    public function saveweightsAction(Request $request) {
        if (!$this->isGranted('ROLE_ADMIN'))
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'No tienes permiso')), 200, array('Content-Type' => 'application/json'));

        $ids = explode(',', $request->get('weights'));

        foreach ($ids as $id) {
            try {
                $weight = $this->get('weightcontrol.tools')->getWeightcontrolObject($id);
                if ($weight && $weight->getExtra() == 'provisional') {
                    $weight->setExtra(null);
                    $this->get('app.weightcontrol')->modifyWeightcontrol($weight);
                }
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        }

        $weights = $this->get('weightcontrol.tools')->getWeightcontrolByExtra('', 'animal', 'provisional');
        foreach ($weights as $k => $v) {
            $animal = $this->get('farms.tools')->getAnimal($v['itemid']);
            $weights[$k]['animalname'] = $animal['name'];
            $weights[$k]['animaltattoo'] = $animal['tattoo'];
            $weights[$k]['farmname'] = $animal['farmdata']['name'];
        }
        $html = $this->render('AppBundle:_Default:index_provisionalweights.html.twig', array('data' => $weights))->getContent();
        
        return new Response(json_encode(array('errorCode' => 0, 'data' => $html)), 200, array('Content-Type' => 'application/json'));
    }

    /* Salvar los animales */

    public function saveanimalsAction(Request $request) {
        if (!$this->isGranted('ROLE_ADMIN'))
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'No tienes permiso')), 200, array('Content-Type' => 'application/json'));

        $ids = explode(',', $request->get('animals'));

        foreach ($ids as $id) {
            try {
                $animal = $this->get('farms.tools')->getAnimalObject($id);
                if ($animal && ($animal->getProvnew() || $animal->getProvend() )) {
                    $animal->setProvnew(false);
                    $animal->setProvend(false);
                    $this->get('farms.tools')->modifyAnimal($animal, $animal->getMother(),$animal->getFather());
                }
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        }

        $html = $this->render('AppBundle:_Default:index_provisionalanimals.html.twig', array('data' => $this->get('app.farms')->getAnimals('', 0, 1, 0, 999, '', '', false, true)))->getContent();
        
        return new Response(json_encode(array('errorCode' => 0, 'data' => $html)), 200, array('Content-Type' => 'application/json'));
    }

    /* pantalla de configuración de la aplicación */

    public function configsAction() {
        return $this->render('AppBundle:_Default:configs.html.twig', array(
                    'btnusers' => $this->get('systemuser.tools')->getSystemuserPerm('list'),
                    'btnlog'=>$this->isGranted('ROLE_ADMIN')
        ));
    }

    /* pantalla de contenido web de la aplicación */

    public function webcontentAction() {
        return $this->render('AppBundle:_Default:webcontent.html.twig', array(
                    'btnmodules' => $this->get('app.webmodules')->getWebmodulePerm('insert'),
                    'webmodules' => $this->get('webmodules.tools')->getAllWebmodulesByProject()
        ));
    }

}
