<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Greetik\SystemuserBundle\DBAL\Types\SpecialtyType;
use AppBundle\DBAL\Types\ShiftspecialtyType;
use AppBundle\Form\filterReportResidentes;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class PrintController extends Controller {
    /* Carta genealógica */

    public function heritagecardAction(Request $request, $id) {
        if ($request->getMethod() != 'POST')
            throw new \Exception('No tienes permiso para hacer esta operación');

        $this->get('app.print')->heritageCard($id);

        $animal = $this->get('app.farms')->getAnimal($id);

        /* return $this->render('AppBundle:Print:heritagecard.html.twig', array(
          'item' => $animal
          ));

          /* Devolver en pdf */
        return new BinaryFileResponse('../var/animalcerts/' . $id . '/cartagenealogica.pdf');
    }

    public function birthregisterAction(Request $request, $id) {
        if ($request->getMethod() != 'POST')
            throw new \Exception('No tienes permiso para hacer esta operación');

        $this->get('app.print')->birthRegister($id);

        /* $animal = $this->get('app.farms')->getAnimal($id);
          return $this->render('AppBundle:Print:birthregister.html.twig', array(
          'item' => $animal
          ));
          /*Devolver el pdf */
        return new BinaryFileResponse('../var/animalcerts/' . $id . '/certificadonacimiento.pdf');
    }

    public function visitAction(Request $request, $id) {
        if ($request->getMethod() != 'POST')
            throw new \Exception('No tienes permiso para hacer esta operación');

        $visit = $this->get('app.visits')->getVisit($id);
        $num = '';
        /* return $this->render('AppBundle:Print:visitletter'.$num.'.html.twig', array(
          'visit' => $visit
          ));
          /*Devolver el pdf */
        $this->get('app.print')->visitletter($visit, $num);
        return new BinaryFileResponse('../var/visits/' . $id . '/carta' . $num . '.pdf');
    }

}
