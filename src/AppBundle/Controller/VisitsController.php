<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Greetik\VisitsBundle\Entity\Visit;
use Greetik\VisitsBundle\Form\Type\VisitType;
use AppBundle\Entity\Visitanimal;
use AppBundle\Form\Type\VisitanimalType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class VisitsController extends Controller {

    public function viewfarmerAction($id) {
        $visit = $this->get('app.visits')->getVisit($id);

        return $this->render('AppBundle:Visit:publicview.html.twig', array(
                    'item' => $visit
        ));
    }

    public function indexAction($item_type) {
        return $this->render('AppBundle:Visit:list.html.twig', array('data' => $this->get('app.visits')->getVisitsByItemtype($item_type)));
    }

    public function detailAction($id) {
        $editForm = $this->createForm(VisitType::class, $this->get('visits.tools')->getVisitObject($id));
        $visit = $this->get('app.visits')->getVisit($id);

        return $this->render('AppBundle:Visit:view.html.twig', array(
                    'item' => $visit,
            'animalsforenddate' => $this->get('app.visits')->animalsForEndDate($id),
            'animalsforweight' => $this->get('app.visits')->animalsForWeight($id),
            'animalsforregisterd' => $this->get('app.visits')->animalsForRegisterD($id),
                    'animalsforregisterc' => $this->get('app.visits')->animalsForRegisterC($id),
                    'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                    'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()),
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get('app.visits')->getVisitPerm('modify', $visit['itemtype'], $visit['itemid'])
        ));
    }

    public function generateAction(Request $request) {
        //if ($request->getMethod() != 'POST')
        //throw new \Exception('No tienes permiso para hacer esta operación');

        $id = $request->get('id');
        $visit = $this->get('app.visits')->getVisit($id);

        $filename = '../var/zips/' . $id . '/visita.zip';
        @unlink($filename);
        $zip = new \ZipArchive();
        if (!file_exists('../var/zips/'))
            mkdir('../var/zips/');
        if (!file_exists('../var/zips/' . $id . '/'))
            mkdir('../var/zips/' . $id . '/');
        if ($zip->open($filename, \ZipArchive::CREATE) !== TRUE)
            throw new \Exception('no se pudo crear el zip');
        $this->get('app.print')->visitletter($visit);
        $this->get('app.print')->visitletter($visit, 2);
        foreach ($visit['animals'] as $animal) {
            if (!$animal['register'] && !$animal['indexanimal'] && !$animal['points']) continue;
            $auxanimal = $this->get('app.farms')->getAnimal($animal['animal']);

            $pos = strpos($animal['register'], 'RN');
            if ($pos!==false) {
                $this->get('app.print')->birthRegister($animal['animal']);
                if (file_exists('../var/animalcerts/' . $animal['animal'] . '/certificadonacimiento.pdf'))
                    $zip->addFile('../var/animalcerts/' . $animal['animal'] . '/certificadonacimiento.pdf', $auxanimal['tattoo'] . '-certificadonacimiento.pdf');
            }else {
                $pos = strpos($animal['register'], 'RD');
                if ($animal['points']>0 || $animal['indexanimal'] || $pos!==false) {
                    $this->get('app.print')->heritageCard($animal['animal']);
                    if (file_exists('../var/animalcerts/' . $animal['animal'] . '/cartagenealogica.pdf'))
                        $zip->addFile('../var/animalcerts/' . $animal['animal'] . '/cartagenealogica.pdf', $auxanimal['tattoo'] . '-cartagenealogica.pdf');
                }
            }
        }
        if (file_exists('../var/visits/' . $visit['id'] . '/carta.pdf'))
            $zip->addFile('../var/visits/' . $visit['id'] . '/carta.pdf', 'ficha-de-trabajo-de-campo.pdf');
        if (file_exists('../var/visits/' . $visit['id'] . '/carta2.pdf'))
            $zip->addFile('../var/visits/' . $visit['id'] . '/carta2.pdf', 'carta.pdf');

        $zip->close();
        return new BinaryFileResponse($filename);
    }

    public function ModifyAction(Request $request, $id) {
        $visitobject = $this->get('visits.tools')->getVisitObject($id);
        $editForm = $this->createForm(VisitType::class, $visitobject);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get('app.visits')->modifyVisit($visitobject);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            } else {
                $this->addFlash('error', (string) $editForm->$editForm->getErrors(true, false));
            }
        }


        return $this->redirect($this->generateUrl('visits_detailvisit', array('id' => $id)));
    }

    /* Enviar email de la visita */

    public function sendmailAction(Request $request, $id) {
        if ($request->getMethod() != 'POST')
            throw new \Exception('No tiene permiso para realizar esta operación');

        $visit = $this->get('visits.tools')->getVisit($id);

        if (!$this->get('app.visits')->getVisitPerm('modify', $visit['itemtype'], $visit['itemid']))
            throw new \Exception('No tiene permiso para realizar esta operación');

        if ($visit['itemtype'] != 'farm')
            throw new \Exception('No tiene permiso para realizar esta operación');

        /* Poner el listado de animales en el email */
        $animals = $this->get($this->getParameter('farm.permsservice'))->getAnimals('', '', '', 0, 9999, $visit['itemid']);
        $textanimals = '<table><thead><th>Tatuaje</th><th>Nombre</th><th>Crotal</th><th>Nacimiento</th><th>RNU</th><th>RDU</th><th>Sexo</th><th>Padre</th><th>Padre Tatuaje</th><th>Madre</th><th>Madre Tatuaje</th></thead><tbody>';
        foreach ($animals as $v) {
            $animal = $this->get($this->getParameter('farm.permsservice'))->getAnimal($v['id']);
            $textanimals .= '<tr><td>' . $animal['tattoo'] . '</td><td>' . $animal['name'] . '</td><td>' . $animal['crotal'] . '</td><td>' . ($animal['birthdate'] ? $animal['birthdate']->format('d/m/Y') : '') . '</td>'
                    . '<td>' . $animal['registerc'] . '</td><td>' . $animal['registerd'] . '</td><td>' . $animal['gender'] ? \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::getReadableValue($animal['gender']):'-' . '</td>'
                    . '<td>' . ((isset($animal['fatherdata'])) ? $animal['fatherdata']['name'] : '') . '</td><td>' . ((isset($animal['fatherdata'])) ? $animal['fatherdata']['tattoo'] : '') . '</td>'
                    . '<td>' . ((isset($animal['motherdata'])) ? $animal['motherdata']['name'] : '') . '</td><td>' . ((isset($animal['motherdata'])) ? $animal['motherdata']['tattoo'] : '') . '</td>'
                    . '</tr>';
        }
        $textanimals .= '</tbody></table>';

        $message = (new \Swift_Message('Visita de ' . $this->getParameter('projectnameabb')))
                ->setFrom($this->getParameter('projectemail'))
                ->setTo($this->get('farms.tools')->getFarmObject($visit['itemid'])->getEmail())
                ->setBody('<p>El día ' . $visit['visitdate']->format('d/m/Y') . ' le visitará ' . $this->getParameter('projectnameabb') . '. Estos son los detalles de la visita: </p>' . $visit['description'] . '<p><strong>Listado de Animales: </strong></p>' . $textanimals, 'text/html');

        if ($this->get('mailer')->send($message))
            $this->addFlash('success', 'Email enviado correctamente');
        else
            $this->addFlash('error', 'No se pudo enviar el email');

        return $this->redirect($this->generateUrl('visits_detailvisit', array('id' => $id)));
    }

    public function insertformanimalAction($visit, $id = 0, $animal = 0, $command = '') {
        $visitdata = $this->get('app.visits')->getVisit($visit);

        if (!empty($animal) && $animal > 0) {
            $visitanimal = $this->get('visitanimals.tools')->getVisitanimalByAnimalAndVisit($visit, $animal);
            if ($visitanimal)
                $id = $visitanimal['id'];
        }


        if (!empty($id) && $id > 0) {
            $visitanimal = $this->get('visitanimals.tools')->getVisitanimalObject($id);
            $command = explode(",", $command);
            if ($visitanimal->getEnddate())
                $command[] = \AppBundle\DBAL\Types\VisitanimalType::BAJA;
            if ($visitanimal->getPoints())
                $command[] = \AppBundle\DBAL\Types\VisitanimalType::CAL;
            if ($visitanimal->getRegister())
                $command[] = \AppBundle\DBAL\Types\VisitanimalType::RU;
            if ($visitanimal->getTofair())
                $command[] = \AppBundle\DBAL\Types\VisitanimalType::FAIR;
            if ($visitanimal->getWeight())
                $command[] = \AppBundle\DBAL\Types\VisitanimalType::WEIGHT;
        } else {
            $visitanimal = new Visitanimal();
            $command = explode(',', $command);
        }

        if (!empty($animal) && $animal > 0) {
            $dataanimal = $this->get('farms.tools')->getAnimal($animal);
        }

        $twoyears = new \Datetime();
        $twoyears->modify('+2 years');
        $newForm = $this->createForm(VisitanimalType::class, $visitanimal, array('_action' => $command, '_animal' => isset($dataanimal) ? $dataanimal['id'] : '', '_fairs' => $this->get('fairs.tools')->getFairs(new \Datetime(), $twoyears), '_animals' => ((empty($id)) ? $this->get('farms.tools')->getAnimalsSelect('', '', $visitdata['itemid']) : array())));

        return $this->render('AppBundle:Visit:insertanimal.html.twig', array('new_form' => $newForm->createView(), 'animal' => isset($dataanimal) ? $dataanimal : '', 'visit' => $visit, 'id' => $id));
    }

    public function insertmodifyanimalAction(Request $request, $visit, $id = 0) {
        $item = $request->get('visitanimal');

        if (!empty($id) && $id > 0) {
            $oldvisitanimal = $this->get('visitanimals.tools')->getVisitanimal($id);
            $visitanimal = $this->get('visitanimals.tools')->getVisitanimalObject($id);
            $visitdata = $this->get('app.visits')->getVisit($visitanimal->getVisit());
            $editing = true;
        } else {
            $visitanimal = new Visitanimal();
            $editing = false;
            $visitdata = $this->get('app.visits')->getVisit($visit);
        }

        $twoyears = new \Datetime();
        $twoyears->modify('+2 years');
        $editForm = $this->createForm(VisitanimalType::class, $visitanimal, array('_fairs' => $this->get('fairs.tools')->getFairs(new \Datetime(), $twoyears), '_animals' => ((empty($id)) ? $this->get('farms.tools')->getAnimalsSelect('', '', $visitdata['itemid']) : array())));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($editing) {
                try {
                    $warning = $this->get('app.visits')->modifyAnimal($visitanimal, $oldvisitanimal);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage()/* .'->'.$e->getFile().'->'.$e->getLine() */)), 200, array('Content-Type' => 'application/json'));
                }
            } else {
                try {
                    $visitanimal->setVisit($visit);
                    $warning = $this->get('app.visits')->insertAnimal($visitanimal);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage()/* .'->'.$e->getFile().'->'.$e->getLine() */)), 200, array('Content-Type' => 'application/json'));
                }
            }
            $visit = $this->get('app.visits')->getVisit($visitanimal->getVisit());
            $data = array('table1' => $this->render('AppBundle:Visit:indexanimals.html.twig', array('data' => $this->get('visitanimals.tools')->getVisitanimalsByVisit($visitanimal->getVisit()), '_visit' => $visitanimal->getVisit(), 'modifyAllow' => true))->getContent(),
                'table2' => $this->render('AppBundle:Visit:animalsforweight.html.twig', array('animalsforweight' => $this->get('app.visits')->animalsForWeight($visitanimal->getVisit()), 'item' => $visit, 'modifyAllow' => true))->getContent(),
                'table3' => $this->render('AppBundle:Visit:animalsforregisterd.html.twig', array('animalsforregisterd' => $this->get('app.visits')->animalsForRegisterD($visitanimal->getVisit()), 'item' => $visit, 'modifyAllow' => true))->getContent(),
                'table4' => $this->render('AppBundle:Visit:animalsforregisterc.html.twig', array('animalsforregisterc' => $this->get('app.visits')->animalsForRegisterC($visitanimal->getVisit()), 'item' => $visit, 'modifyAllow' => true))->getContent(),
                    'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                    'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()));
            return new Response(json_encode(array('errorCode' => 0, 'warning' => (strlen($warning) > 10 ? $warning : ''), 'data' => $data)), 200, array('Content-Type' => 'application/json'));
        } else {
            $errors = (string) $editForm->getErrors(true, true);
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
    }

    public function dropanimalAction($id) {
        $visitanimal = $this->get('visitanimals.tools')->getVisitanimalObject($id);

        try {
            $this->get('app.visits')->deleteAnimal($visitanimal);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        $visit = $this->get('app.visits')->getVisit($visitanimal->getVisit());
        $data = array('table1' => $this->render('AppBundle:Visit:indexanimals.html.twig', array('data' => $this->get('visitanimals.tools')->getVisitanimalsByVisit($visitanimal->getVisit()), '_visit' => $visitanimal->getVisit(), 'modifyAllow' => true))->getContent(),
            'table2' => $this->render('AppBundle:Visit:animalsforweight.html.twig', array('animalsforweight' => $this->get('app.visits')->animalsForWeight($visitanimal->getVisit()), 'item' => $visit, 'modifyAllow' => true))->getContent(),
            'table3' => $this->render('AppBundle:Visit:animalsforregisterd.html.twig', array('animalsforregisterd' => $this->get('app.visits')->animalsForRegisterD($visitanimal->getVisit()), 'item' => $visit, 'modifyAllow' => true))->getContent(),
            'table4' => $this->render('AppBundle:Visit:animalsforregisterc.html.twig', array('animalsforregisterc' => $this->get('app.visits')->animalsForRegisterC($visitanimal->getVisit()), 'item' => $visit, 'modifyAllow' => true))->getContent(),
                    'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                    'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()));
        return new Response(json_encode(array('errorCode' => 0, 'data' => $data)), 200, array('Content-Type' => 'application/json'));
    }

    public function registercanimalsAction(Request $request) {

        $warning = '';
        $animalsids = explode(',', $request->get('animals'));
        try {
            $register = intval($this->get('farms.tools')->getMaxRegisterC());
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }
        foreach ($animalsids as $animalid) {
            $visit = $request->get('visit');
            $visitanimal = $this->get('visitanimals.tools')->getVisitanimalByAnimalAndVisit($visit, $animalid);
            if (!$visitanimal) {
                $visitanimal = new Visitanimal();
                $visitanimal->setAnimal($animalid);
            } else {
                $visitanimal = $this->get('visitanimals.tools')->getVisitanimalObject($visitanimal['id']);
            }
            $visitanimal->setRegister('RNU ' . $register);
            $visitanimal->setVisit($visit);

            try {
                $warning = $this->get('app.visits')->insertAnimal($visitanimal) . (($warning == '') ? '. ' : '');
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }

            $register++;
        }

        $visit = $this->get('app.visits')->getVisit($visit);
        $data = array('table1' => $this->render('AppBundle:Visit:indexanimals.html.twig', array('data' => $this->get('visitanimals.tools')->getVisitanimalsByVisit($visit['id']), '_visit' => $visit['id'], 'modifyAllow' => true))->getContent(),
            'table2' => $this->render('AppBundle:Visit:animalsforweight.html.twig', array('animalsforweight' => $this->get('app.visits')->animalsForWeight($visit['id']), 'item' => $visit, 'modifyAllow' => true))->getContent(),
            'table3' => $this->render('AppBundle:Visit:animalsforregisterd.html.twig', array('animalsforregisterd' => $this->get('app.visits')->animalsForRegisterD($visit['id']), 'item' => $visit, 'modifyAllow' => true))->getContent(),
            'table4' => $this->render('AppBundle:Visit:animalsforregisterc.html.twig', array('animalsforregisterc' => $this->get('app.visits')->animalsForRegisterC($visit['id']), 'item' => $visit, 'modifyAllow' => true))->getContent(),
                    'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                    'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()));
        return new Response(json_encode(array('errorCode' => 0, 'warning' => (strlen($warning) > 12 ? $warning : ''), 'data' => $data)), 200, array('Content-Type' => 'application/json'));
    }

    public function registerdanimalsAction(Request $request) {

        $warning = '';
        $animalsids = explode(',', $request->get('animals'));
        $register = intval($this->get('farms.tools')->getMaxRegisterD());

        foreach ($animalsids as $animalid) {
            $visit = $request->get('visit');
            $visitanimal = $this->get('visitanimals.tools')->getVisitanimalByAnimalAndVisit($visit, $animalid);
            if (!$visitanimal) {
                $visitanimal = new Visitanimal();
                $visitanimal->setAnimal($animalid);
            } else {
                $visitanimal = $this->get('visitanimals.tools')->getVisitanimalObject($visitanimal['id']);
            }
            $visitanimal->setRegister('RDU ' . $register);
            $visitanimal->setVisit($visit);


            try {
                $warning = $this->get('app.visits')->insertAnimal($visitanimal) . (($warning == '') ? '. ' : '');
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() . '->' . $e->getFile() . '->' . $e->getLine())), 200, array('Content-Type' => 'application/json'));
            }

            $register++;
        }

        $visit = $this->get('app.visits')->getVisit($visit);
        $data = array('table1' => $this->render('AppBundle:Visit:indexanimals.html.twig', array('data' => $this->get('visitanimals.tools')->getVisitanimalsByVisit($visit['id']), '_visit' => $visit['id'], 'modifyAllow' => true))->getContent(),
            'table2' => $this->render('AppBundle:Visit:animalsforweight.html.twig', array('animalsforweight' => $this->get('app.visits')->animalsForWeight($visit['id']), 'item' => $visit, 'modifyAllow' => true))->getContent(),
            'table3' => $this->render('AppBundle:Visit:animalsforregisterd.html.twig', array('animalsforregisterd' => $this->get('app.visits')->animalsForRegisterD($visit['id']), 'item' => $visit, 'modifyAllow' => true))->getContent(),
            'table4' => $this->render('AppBundle:Visit:animalsforregisterc.html.twig', array('animalsforregisterc' => $this->get('app.visits')->animalsForRegisterC($visit['id']), 'item' => $visit, 'modifyAllow' => true))->getContent(),
                    'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                    'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()));
        return new Response(json_encode(array('errorCode' => 0, 'warning' => (strlen($warning) > 12 ? $warning : ''), 'data' => $data)), 200, array('Content-Type' => 'application/json'));
    }

    /* exportar visitas */

    public function exportAction(Request $request, $ids = '') {
        // ask the service for a excel object
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("UCHAE")
                ->setLastModifiedBy("UCHAE")
                ->setTitle("Visitas - " . date('d/m/Y'))
                ->setSubject("Exportación de las visitas");

        $this->get('app.visits')->exportVisits($phpExcelObject, explode(',', $ids));

        $phpExcelObject->getActiveSheet()->setTitle('Visitas');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'visitas.xlsx'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

}
