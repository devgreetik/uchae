<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class PublicwebController extends \Greetik\PublicwebBundle\Controller\DefaultController {

    public function farmAction(Request $request, $name, $id) {
        $farm = $this->get('app.farms')->getPublicFarm($id);

        if ($name != $this->get('beinterface.tools')->spaceencodeurl($farm['name'], true)) {
            return $this->redirectToRoute('publicweb_farm', array('name' => $this->get('beinterface.tools')->spaceencodeurl($farm['name'], true), 'id' => $farm['id']), 301);
        }
        try {
            $response = $this->get('publicweb.tools')->sendheader();
        } catch (\Exception $e) {
            $response = new Response();
        }
        return $this->render('AppBundle:PublicPages:farm.html.twig', $this->get('app.publicweb')->getParamsFarm($request, $id), $response);
    }
    public function animalAction(Request $request, $name, $id) {
        $animal = $this->get('app.farms')->getPublicAnimal($id);

        if ($name != $this->get('beinterface.tools')->spaceencodeurl($animal['name'], true)) {
            return $this->redirectToRoute('publicweb_animal', array('name' => $this->get('beinterface.tools')->spaceencodeurl($animal['name'], true), 'id' => $animal['id']), 301);
        }
        try {
            $response = $this->get('publicweb.tools')->sendheader();
        } catch (\Exception $e) {
            $response = new Response();
        }
        return $this->render('AppBundle:PublicPages:animal.html.twig', $this->get('app.publicweb')->getParamsAnimal($request, $id), $response);
    }
    public function farmsprovinceAction(Request $request, $province, $page = '', $category = '') {
        $id=9;
        $sendmail = $this->checksendmail($request);
        if ($sendmail)
            return $this->redirectToRoute('publicweb_farmsprovince', array('province' => $province));

        try {
            $response = $this->get($this->getParameter('publicweb.service'))->sendheader(true);
        } catch (\Exception $e) {
            $response = new Response();
        }
        
        

        return $this->render($this->getParameter('publicweb.interface') . ':section.html.twig', $this->get($this->getParameter('publicweb.service'))->getParamsSection($request, $id, $page, $category, $province), $response);
    }

    
}
