<?php
// LogoutListener.php - Change the namespace according to the location of this class in your bundle
namespace AppBundle\Listeners;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ResponseListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {   
        $event->getResponse()->headers->set('x-frame-options', 'deny');
    }   
}