<?php

// Change the namespace according to the location of this class in your bundle
namespace AppBundle\Listeners;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;

class LoginListener {
    
    protected $systemusers;
    private $log;
    
    public function __construct($_systemusers, $_log){
        $this->systemusers = $_systemusers;
        $this->log = $_log;
        
    }
    
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        //Acutalizamos el log
        $this->log->insertEntry(LogactionsType::OTHER, 'Login del usuario '.$this->systemusers->getUserConnected()->getUsername());
    }
}