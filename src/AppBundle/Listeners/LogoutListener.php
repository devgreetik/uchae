<?php
// LogoutListener.php - Change the namespace according to the location of this class in your bundle
namespace AppBundle\Listeners;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;

class LogoutListener implements LogoutHandlerInterface {
    protected $systemusers;
    private $log;
    
    public function __construct($_systemusers, $_log){
        $this->systemusers = $_systemusers;
        $this->log = $_log;
        
    }
    
    public function logout(Request $Request, Response $Response, TokenInterface $Token) {
        //Acutalizamos el log
        $this->log->insertEntry(LogactionsType::OTHER, 'Logout del usuario '.$this->systemusers->getUserConnected()->getUsername());
    }
}