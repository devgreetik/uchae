<?php

namespace AppBundle\Services;

use Greetik\CatalogBundle\Entity\Product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product Tools
 *
 * @author Pacolmg
 */
class Catalog {

    private $catalog;
    private $itemlinks;
    private $dataimages;
    private $ytvideos;

    public function __construct($_catalog, $_systemusers, $_itemlinks, $_dataimages, $_ytvideos) {
        $this->catalog = $_catalog;
        $this->systemusers = $_systemusers;
        $this->itemlinks = $_itemlinks;
        $this->dataimages = $_dataimages;
        $this->ytvideos = $_ytvideos;
    }

    /* obtener un producto */

    public function getPublicProduct($product) {
        if (is_numeric($product))
            $product = $this->catalog->getProduct($product);
        $product['itemlinks'] = $this->itemlinks->getItemlinksByItem($product['id'], 'product');
        $product['images'] = $this->dataimages->getImages($product['id'], 'product')['files'];
        $product['documents'] = $this->dataimages->getImages($product['id'], 'product', 1, 'document')['files'];
        $product['videos'] = $this->ytvideos->getVideos($product['id'], 'product');

        return $product;
    }

    public function getProductsByProject($project = '', $page = '', $productperpage = 10) {
        if (!$this->getProductPerm('', 'list'))
            throw new \Exception('No tienes permiso');
        if (empty($page))
            $data = $this->catalog->getProductsByProject($project);
        else
            $data = $this->catalog->getProductsByPage($project, $page, $productperpage);

        foreach ($data as $k => $v) {
            $data[$k] = $this->getPublicProduct($v);
        }

        return $data;
    }

    public function getCatalogForm($project, $onlyid = false) {
        return $this->catalog->getCatalogForm($project, $onlyid);
    }

    public function getCategories($project, $category = '', $onlyid = false) {
        return $this->catalog->getCategories($project, $category, $onlyid);
    }

    public function getCategoriesForSelect($project) {
        return $this->catalog->getCategoriesForSelect($project);
    }

    public function getProduct($id) {
        $product = $this->catalog->getProduct($id);
        if (!$this->getProductPerm($product, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver el producto');

        return $this->getPublicProduct($product);
    }

    public function modifyProduct($productObject, $project, $form) {
        if (!$this->getProductPerm($productObject, 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar el producto');
        return $this->catalog->modifyProduct($productObject, $project, $form);
    }

    public function insertProduct($product, $idmodule, $form) {
        if (!$this->getProductPerm($product, 'insert'))
            throw new AccessDeniedException('No tienes permiso para insertar el producto');
        $product->setNumorder(0);

        if (is_array($form))
            $this->catalog->insertProduct($product, $idmodule, $this->getDataProductFromArray($idmodule, $form));
        else
            $this->catalog->insertProduct($product, $idmodule, $this->getDataProductFromForm($idmodule, $form));
    }

    public function masivedeleteProduct($ids) {
        $data = array('num' => 0, 'warnings' => '');
        foreach ($ids as $id) {
            try {
                $this->deleteProduct($this->catalog->getProductObject($id));
                $data['num'] ++;
            } catch (\Exception $e) {
                $data['warnings'] .= (($data['warnings'] == '') ? '' : ', ') . 'No se pudo eliminar el elemento con id ' . $id;
            }
        }

        return $data;
    }

    public function deleteProduct($product) {
        if (!$this->getProductPerm($product, 'delete'))
            throw new AccessDeniedException('No tienes permiso para eliminar el producto');
        $this->deleteVideosandImages($product->getId());
        $this->catalog->deleteProduct($product);
    }

    public function getProductPerm($product, $type = '', $project = '') {
        if ($type == 'publicview')
            return true;

        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
    }

    protected function deleteVideosandImages($product) {
        if (is_numeric($product))
            $product = $this->getPublicProduct($product);

        foreach ($product['videos'] as $video)
            $this->ytvideos->dropVideo($video['id']);
        
        foreach ($product['itemlinks'] as $link)
            $this->itemlinks->deleteItemlink($link['id']);
        
        $_SERVER['REQUEST_METHOD'] = 'POST';
        foreach (array_merge($product['images'], $product['documents']) as $image) {
            $this->dataimages->dropImage($image['id']);
        }
    }     

    public function getCatalogConfig($project) {
        if (!$this->getProductPerm('', 'insert', $project))
            throw new \Exception('No tiene permiso');
        return $this->catalog->getCatalogConfig($project);
    }

    public function saveConfig($config) {
        if (!$this->getProductPerm('', 'insert', $config->getProject()))
            throw new \Exception('No tiene permiso');
        $this->catalog->saveConfig($config);
    }
    
    public function importcatalog($filename, $idproject) {
        if (!$this->getProductPerm('', 'insert', $idproject))
            throw new \Exception('No tiene permiso para hacer esta operación');
        
        return $this->catalog->imporcatalog($filename, $idproject);
    }    

}
