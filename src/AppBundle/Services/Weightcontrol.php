<?php

namespace AppBundle\Services;

use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;

/**
 * Description of Blog Tools
 *
 * @author Pacolmg
 */
class Weightcontrol
{

    private $weightcontrol;
    private $farms;
    private $log;

    public function __construct($_weightcontrol, $_farms, $_log)
    {
        $this->weightcontrol = $_weightcontrol;
        $this->farms = $_farms;
        $this->log = $_log;
    }

    /*cadena para descripción del log*/
    protected function stringForLog($item)
    {
        $animal = $this->farms->getAnimal($item->getItemId());
        return 'Peso #' . $item->getId() . ' del Animal #' . $animal['id'] . ' (' . $animal['name'] . ' - ' . $animal['tattoo'] . '). El día ' . $item->getWeightdate()->format('d/m/Y') . ' y ' . $item->getWeight() . 'Kgs';
    }

    protected function getWeightcontrolPerm($perm, $item_type, $item_id, $avoidexception = false)
    {
        switch ($item_type) {
            case 'animal':
                if (!$this->farms->getAnimalPerm($perm, $item_id)) {
                    if (!$avoidexception)
                        throw new \Exception('No tienes permiso para operar con el animal');
                    else
                        return false;
                }
                break;
            default:
                if (!$avoidexception)
                    throw new \Exception('No se encuentra el tipo de control de peso');
                else
                    return false;
        }

        return true;
    }

    public function getWeightcontrolItem($item_id, $item_type)
    {
        switch ($item_type) {
            case 'animal':
                if (!$this->farms->getAnimalPerm('view', $item_id))
                    throw new \Exception('No tienes permiso para operar con el animal');
                return $this->farms->getAnimal($item_id);
                break;
            default:
                throw new \Exception('No se encuentra el tipo de control de peso');
        }

        return '';
    }

    public function insertWeightcontrol($weightcontrol)
    {
        $this->getWeightcontrolPerm('modifypartial', $weightcontrol->getItemtype(), $weightcontrol->getItemid());
        if (!$this->getWeightcontrolPerm('modify', $weightcontrol->getItemtype(), $weightcontrol->getItemid(), true)) {
            $weightcontrol->setExtra('provisional');
        } else {
            if ($weightcontrol->getExtra() == 'provisional')
                $weightcontrol->setExtra(null);
        }
        $result = $this->weightcontrol->insertWeightcontrol($weightcontrol);
        $this->log->insertEntry(LogactionsType::INSERT, $this->stringForLog($weightcontrol), $weightcontrol->getId(), 'weight');
        return $result;
    }

    public function getWeightcontrolByDates($item_type, $from, $to, $ids='', $start='', $length='')
    {
        return $this->weightcontrol->getWeightcontrolByDates($item_type, $from, $to, $ids, $start, $length);
    }
    public function getWeightcontrolByDatesNum($item_type, $from, $to, $ids='')
    {
        return $this->weightcontrol->getWeightcontrolByDatesNum($item_type, $from, $to, $ids);
    }

    public function getWeightcontrolByItem($item_id, $item_type)
    {
        $this->getWeightcontrolPerm('view', $item_type, $item_id);
        return $this->weightcontrol->getWeightcontrolByItem($item_id, $item_type);
    }

    public function modifyWeightcontrol($weightcontrol)
    {
        $this->getWeightcontrolPerm('modify', $weightcontrol->getItemtype(), $weightcontrol->getItemid());
        if ($weightcontrol->getExtra() == 'provisional')
            $weightcontrol->setExtra(null);
        $result = $this->weightcontrol->modifyWeightcontrol($weightcontrol);
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringForLog($weightcontrol), $weightcontrol->getId(), 'weight');
        return $result;
    }

    public function deleteWeightcontrol($weightcontrol)
    {
        $stringforlog = $this->stringForLog($weightcontrol);
        $id = $weightcontrol->getId();

        $this->getWeightcontrolPerm('modify', $weightcontrol->getItemtype(), $weightcontrol->getItemid());
        $result = $this->weightcontrol->deleteWeightcontrol($weightcontrol);
        $this->log->insertEntry(LogactionsType::DELETE, $stringforlog, $id, 'weight');
        return $result;
    }

}
