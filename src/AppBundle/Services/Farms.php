<?php

namespace AppBundle\Services;

use Dropbox\Exception;
use Greetik\FarmBundle\DBAL\Types\AnimalgenderType;
use Greetik\FarmBundle\DBAL\Types\AnimalstateType;
use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;

/**
 * Description of Farm Tools
 *
 * @author Pacolmg
 */
class Farms {

    private $farms;
    private $systemusers;
    private $router;
    private $weightcontrol;
    private $visits;
    private $dataimages;
    private $log;

    public function __construct($_farms, $_systemusers, $_router, $_weightcontrol, $_visits, $_dataimages, $_log) {
        $this->farms = $_farms;
        $this->systemusers = $_systemusers;
        $this->router = $_router;
        $this->weightcontrol = $_weightcontrol;
        $this->visits = $_visits;
        $this->dataimages = $_dataimages;
        $this->log = $_log;
    }

    /* cadena para descripción del log */

    protected function stringForLog($item) {
        if (is_numeric($item))
            $item = $this->farms->getFarmObject($item);
        if (!$item)
            return '';
        return 'Ganadería #' . $item->getId() . '. Nombre: ' . $item->getName();
    }

    /* cadena para descripción del log */

    protected function stringAnimalForLog($item) {
        if (is_numeric($item))
            $item = $this->farms->getAnimalObject($item);
        return 'Animal #' . $item->getId() . '. Nombre: ' . $item->getName() . '. (' . $item->getTattoo() . '). ' . $this->stringForLog($item->getFarm());
    }

    /* Comprueba si el usuario tiene permiso para operar sobre la ganadería */

    public function getFarmPerm($type, $farm = '') {
        $roleconnected = $this->systemusers->roleConnected();

        if ($type == 'publicview')
            return true;

        if ($this->systemusers->isGrantedConnected('ROLE_ADMIN'))
            return true;

        if (is_numeric($farm))
            $farm = $this->farms->getFarm($farm);

        if ($type == 'view' || $type == 'modifypartial') {
            if ($this->systemusers->isGrantedConnected('ROLE_EDITOR'))
                return true;

            if (!$farm) {
                return $type == 'view';
            }

            $idfarm = ((is_numeric($farm)) ? $farm : ((is_array($farm)) ? $farm['id'] : $farm->getId()));
            return ($idfarm == $this->systemusers->getUserConnected()->getFarm());
        }

        if ($type == 'insert' || $type == 'delete') {
            return ($roleconnected == 'ROLE_ADMIN');
        }
        /*
          if ($type == 'modifypartial') {
          return $this->systemusers->isGrantedConnected('ROLE_EDITOR');
          } */
        return false;
    }

    /* obtener el listado de ganaderías */

    public function getFarms($forselect = false) {
        if (!$this->getFarmPerm('list'))
            throw new \Exception('No tienes permiso para listar las ganaderías');
        return $this->farms->getFarms($forselect);
    }

    /* Obtener para selector */

    public function getFarmsSelect($search) {
        if (!$this->getFarmPerm('insert'))
            throw new \Exception('No tienes permiso para ver los animales');

        return $this->farms->getFarmsSelect($search);
    }

    /* obtener una ganadería */

    public function getFarm($id) {
        $farm = $this->farms->getFarm($id);
        if (!$this->getFarmPerm('view', $farm))
            throw new \Exception('No tienes permiso para ver la ganadería');
        $farm['admins'] = $this->systemusers->getSystemusersByFarm($farm['id']);
        $farm['visits'] = $this->visits->getVisitsByItem($farm['id'], 'farm');

        return $farm;
    }

    /* eliminar una ganadería */

    public function deleteFarm($farm) {
        if (!$this->getFarmPerm('delete', $farm))
            throw new \Exception('No tienes permiso para eliminar la ganadería');
        if (count($this->systemusers->getSystemusersByFarm($farm)))
            throw new \Exception('Elimine primero los administradores de la ganadería');

        $stringForLog = $this->stringAnimalForLog($farm);
        $id = $farm->getId();
        $result = $this->farms->deleteFarm($farm);
        $this->log->insertEntry(LogactionsType::DELETE, $stringForLog, $id, 'farm');
        return $result;
    }

    /* insertar una ganadería */

    public function insertFarm($farm) {
        if (!$this->getFarmPerm('delete', $farm))
            throw new \Exception('No tienes permiso para insertar la ganadería');
        $result = $this->farms->insertFarm($farm);
        $this->log->insertEntry(LogactionsType::INSERT, $this->stringForLog(), $farm->getId(), 'farm');
        return $result;
    }

    /* modificar una ganadería */

    public function modifyFarm($farm, $oldfarm) {
        if (!$this->getFarmPerm('modifypartial', $farm))
            throw new \Exception('No tienes permiso para modificar la ganadería');

        if (!$oldfarm['enddate'] && $farm->getEnddate()) {
            //damos de baja a todos los animales
            foreach ($farm->getAnimals() as $animal) {
                if (!$animal->getEnddate()) {
                    $animal->setEnddate($farm->getEnddate());
                    $animal->setLaststate($animal->getState());
                    $animal->setState(AnimalstateType::INNACTIVE);
                    $this->farms->modifyAnimalPartial($animal, true);
                }
            }
        }

        if ($oldfarm['enddate'] && !$farm->getEnddate()) {
            //volvemos a dar de alta los animales
            foreach ($farm->getAnimals() as $animal) {
                if ($animal->getEnddate() && $animal->getEnddate()->format('Y-m-d') == $oldfarm['enddate']->format('Y-m-d')) {
                    $animal->setEnddate(null);
                    $animal->setState($animal->getLaststate());
                    $this->farms->modifyAnimalPartial($animal, true);
                }
            }
        }
        $result = $this->farms->modifyFarm($farm);
        $this->log->insertEntry(LogactionsType::INSERT, $this->stringForLog($farm), $farm->getId(), 'farm');
        return $result;
    }

    /* Cambia el avatar de una ganadería */

    public function changeAvatar($file, $farm) {
        if (!$this->getFarmPerm('modifypartial', $farm))
            throw new \Exception('No tienes permiso para modificar la ganadería');
        $result = $this->farms->changeAvatar($file, $farm);
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringForLog($farm) . ' Cambio de Avatar. ', $farm, 'farm');
        return $result;
    }

    /* obtener una ganadería sin pasar permisos */

    public function getPublicFarm($id) {
        $farm = $this->farms->getFarm($id);

        return $farm;
    }

    /* obtener el listado público de ganaderías */

    public function getPublicFarms($page = '', $numporpage = 10, $province = '') {
        if (empty($page))
            return $this->farms->getFarms(false, $province);
        return $this->farms->getFarmsPaged($page, $numporpage, $province);
    }

    /* Devuelve el número de ganaderías que hay */

    public function getNumberOfFarms($province = '') {
        return $this->farms->getNumberOfFarms($province);
    }

    /* permiso para el animal */

    public function getAnimalPerm($type, $animal = '', $farm = '') {
        if ($type == 'publicview')
            return true;

        if ($type == 'delete' || $type == 'insert') {
            return $this->getFarmPerm($type);
        }

        if ($animal) {
            if (is_numeric($animal))
                $animal = $this->farms->getAnimal($animal);

            $farm = ((is_array($animal)) ? $animal['farm'] : (($animal->getFarm()) ? $animal->getFarm()->getId() : '') );
            if (!$farm)
                return $this->getFarmPerm('insert');
            return $this->getFarmPerm($type == 'modify' ? 'modify' : 'modifypartial', $farm);
        }

        if ($farm)
            return $this->getFarmPerm($type == 'modify' ? 'modify' : 'modifypartial', $farm);

        return false;
    }

    /* obtener pesadas para tabla ajax de informes */

    public function getWeightsReportTable($excel, $excelgrouped, $search, $sortcol, $sortdir, $start, $lenght, $idfarm = '', $from = '', $to = '', $sex = '', $onlyActive = true) {

        list($sortcol, $sortdir) = $this->sortcolAnimalsTable($sortcol, $sortdir, $idfarm);
        $numerrors = 0;
        $datatoreturn = array();
        $animals = $this->farms->getAnimalsTableFiltered($search, 'a.id', 'ASC', 0, 999999999, $idfarm, '', '', '', '', '', $sex, '', $onlyActive, '', $excel || $excelgrouped);
        $ids = array();
        foreach ($animals as $animal) {
            $ids[] = $animal['id'];
        }

        $weights = $this->weightcontrol->getWeightControlByDates('animal', $from, $to, $ids, $start, $lenght);


        foreach ($weights as $k => $weight) {
            $found = false;
            foreach ($animals as $ka => $animal) {
                if ($weight['itemid'] == $animal['id']) {
                    $weights[$k]['animal'] = $animal;
                    if (!isset($animal[$ka]['weights']))
                        $animal[$k]['weights'] = array();
                    $animals[$ka]['weights'][] = $weight;
                    $found = true;
                }
            }

            if (!$found)
                unset($weights[$k]);
        }

        if ($excelgrouped) {
            //agrupamos pesadas por animal
            foreach ($animals as $animal) {
                $i = 0;
                $row = array();
                if (!isset($animal['weights']))
                    continue;

                if (count($animal['weights']) == 1 && $animal['weights'][0]['weightdate'] == $animal['birthdate'])
                    continue;

                $animal['weights'] = array_reverse($animal['weights']);

                $row[] = $animal['crotal'];
                $row[] = $animal['tattoo'];
                $row[] = $animal['name'];
                $row[] = $animal['gender'] ? AnimalgenderType::getReadableValue($animal['gender']) : '';
                $row[] = $animal['birthdate'] ? $animal['birthdate']->format('d/m/Y') : '-';
                $row[] = $animal['birthweight'];

                foreach ($animal['weights'] as $weight) {
                    if ($weight['weightdate'] == $animal['birthdate'])
                        continue;

                    if ($i > 2)
                        break;
                    try {
                        $row[] = $weight['weightdate']->format('d/m/Y');
                        $row[] = $weight['weight'];
                    } catch (\Exception $e) {
                        $numerrors++;
                    }
                    $i++;
                }
                for ($i; $i <= 2; $i++) {
                    $row[] = '-';
                    $row[] = '-';
                }
                $row[] = $animal['indexdate'] ? $animal['indexdate']->format('d/m/Y') : '-';
                $row[] = $animal['index_ac'];
                $row[] = $animal['index_ad'];
                $row[] = $animal['index_rn'];
                $row[] = $animal['index_an'];
                $row[] = $animal['index_epl'];
                $row[] = $animal['index_gc'];
                $row[] = $animal['index_ld'];
                $row[] = $animal['index_lp'];
                $row[] = $animal['index_aa'];
                $row[] = $animal['index_tmn'];
                $row[] = $animal['index_m'];
                $row[] = $animal['index_aaa'];
                $row[] = $animal['index_aap'];
                $row[] = $animal['index_rd'];
                $row[] = $animal['index_cc'];
                $row[] = $animal['index_pp'];
                $row[] = $animal['index_ap'];
                $row[] = $animal['index_at'];
                $row[] = $animal['index_ln'];
                $datatoreturn[] = $row;
            }
        } else {
            //Devolvemos una pesada en cada celda
            foreach ($weights as $weight) {
                try {
                    $datatoreturn[] = array(
                        $weight['weightdate']->format('d/m/Y'),
                        $weight['weight'],
                        $weight['animal']['birthdate'] ? $weight['animal']['birthdate']->format('d/m/Y') : '-',
                        $weight['animal']['crotal'],
                        $weight['animal']['tattoo'],
                        $weight['animal']['registerc'] ? $weight['animal']['registerc'] : '-',
                        $weight['animal']['registerd'] ? $weight['animal']['registerd'] : '-',
                        $weight['animal']['farmname'],
                        $weight['animal']['name'],
                        $weight['animal']['gender'] ? AnimalgenderType::getReadableValue($weight['animal']['gender']) : ''
                    );
                } catch (\Exception $e) {
                    //throw $e;
                    //unset($data[$k]);
                    $numerrors++;
                }
            }
        }

        $number = intval($this->weightcontrol->getWeightControlByDatesNum('animal', $from, $to, $ids));

        return array('iTotalRecords' => $number - $numerrors, 'iTotalDisplayRecords' => $number - $numerrors, "aaData" => $datatoreturn);
    }

    /* obtener animales para tabla ajax de informes */

    public function getAnimalsReportTable($excel, $search, $sortcol, $sortdir, $start, $lenght, $idfarm = '', $idfarmbirth = '', $father = '', $mother = '', $birthfrom = '', $birthto = '', $sex = '', $forsale = '', $onlyActive = true) {

        list($sortcol, $sortdir) = $this->sortcolAnimalsTable($sortcol, $sortdir, $idfarm);
        $numerrors = 0;
        foreach ($data = $this->farms->getAnimalsTableFiltered($search, $sortcol, $sortdir, $start, $lenght, $idfarm, $idfarmbirth, $father, $mother, $birthfrom, $birthto, $sex, $forsale, $onlyActive, '', $excel) as $k => $animal) {
            try {
                $data[$k] = $this->setAnimalDataTable($animal, '', $excel);
            } catch (\Exception $e) {
                //throw $e;
                unset($data[$k]);
                $numerrors++;
            }
        }

        $number = intval($this->farms->getAnimalsTableNumberFiltered($search, $idfarm, $idfarmbirth, $father, $mother, $birthfrom, $birthto, $sex, $forsale, $onlyActive));

        return array('iTotalRecords' => $number - $numerrors, 'iTotalDisplayRecords' => $number - $numerrors, "aaData" => $data);
    }

    /* obtener animales para tabla ajax */

    public function getAnimalsTable($search, $sortcol, $sortdir, $start, $lenght, $idfarm = '', $parent = '', $onlyActive = true) {
        if (!empty($idfarm)) {
            if (!$this->getFarmPerm('view', $idfarm))
                throw new \Exception('No tienes permiso para ver los animales');
        }
        elseif (!empty($parent)) {
            if (!$this->getAnimalPerm('publicview', $parent))
                throw new \Exception('No tienes permiso para ver los animales');
        }elseif (!$this->getFarmPerm('insert'))
            throw new \Exception('No tienes permiso ver los animales');

        list($sortcol, $sortdir) = $this->sortcolAnimalsTable($sortcol, $sortdir, $idfarm);

        $numerrors = 0;
        foreach ($data = $this->farms->getAnimalsTable($search, $sortcol, $sortdir, $start, $lenght, $idfarm, $parent, $onlyActive) as $k => $animal) {
            try {
                $data[$k] = $this->setAnimalDataTable($animal, $idfarm);
            } catch (\Exception $e) {
                throw $e;
                unset($data[$k]);
                $numerrors++;
            }
        }

        $number = intval($this->farms->getAnimalsTableNumber($search, $idfarm, $parent, $onlyActive));

        return array('iTotalRecords' => $number - $numerrors, 'iTotalDisplayRecords' => $number - $numerrors, "aaData" => $data);
    }

    /* obtener animales */

    public function getAnimals($search, $sortcol, $sortdir, $start, $lenght, $idfarm = '', $parent = '', $onlyActive = true, $prov = '') {
        if (!empty($idfarm)) {
            if (!$this->getFarmPerm('modifypartial', $idfarm))
                throw new \Exception('No tienes permiso para ver los animales');
        }
        elseif (!empty($parent)) {
            if (!$this->getAnimalPerm('modifypartial', $parent))
                throw new \Exception('No tienes permiso para ver los animales');
        }elseif (!$this->getFarmPerm('insert'))
            throw new \Exception('No tienes permiso ver los animales');

        list($sortcol, $sortdir) = $this->sortcolAnimalsTable($sortcol, $sortdir, $idfarm);
        return $this->farms->getAnimalsTable($search, $sortcol, $sortdir, $start, $lenght, $idfarm, $parent, $onlyActive, $prov);
    }

    protected function sortcolAnimalsTable($sortcol, $sortdir, $idfarm = '') {

        $sortdir = strtoupper($sortdir);

        if ($sortdir != 'ASC' && $sortdir != 'DESC')
            $sortdir = 'ASC';

        if (!empty($idfarm)) {
            //if ($sortcol>1) $sortcol++;
        }

        switch ($sortcol) {
            case 0: $sortcol = "a.birthdate";
                break;
            case 1: $sortcol = "a.tattoo";
                break;
            case 2: $sortcol = (empty($idfarm) ? "a.registerc" : "a.name");
                break;
            case 3: $sortcol = (empty($idfarm) ? "a.registerd" : "a.gender");
                break;
            case 4: $sortcol = (empty($idfarm) ? "f.name" : "a.crotal");
                break;
            case 5: $sortcol = (empty($idfarm) ? "a.name" : "a.register");
                break;
            case 6: $sortcol = (empty($idfarm) ? "a.gender" : "a.mother");
                break;
            case 7: $sortcol = (empty($idfarm) ? "a.crotal" : "a.father");
                break;
            case 8: $sortcol = (empty($idfarm) ? "a.price, a.forsale" : "a.price, a.forsale");
                break;
            case 9: $sortcol = (empty($idfarm) ? "a.enddate" : "a.enddate");
                break;
            case 10: $sortcol = "a.provnew " . $sortdir . ", a.provend";
                break;
            case 11: $sortcol = "a.provnew " . $sortdir . ", a.provend";
                break;
            default: $sortcol = "a.birthdate";
                break;
        }


        return array($sortcol, $sortdir);
    }

    /* poner los datos a un animal para mostrar en una tabla ajax */

    protected function setAnimalDataTable($animal, $idfarm = '', $excel = '') {
        $father = $this->farms->getAnimal($animal['father'], true, true);
        $mother = $this->farms->getAnimal($animal['mother'], true, true);
        $farmbirth = $this->farms->getFarm($animal['farmbirth']);

        if ($excel) {
            return array(
                $animal['tattoo'],
                $animal['farmname'],
                $farmbirth ? $farmbirth['name'] : '-',
                $animal['name'],
                $animal['crotal'],
                $animal['birthdate'] ? $animal['birthdate']->format('d/m/Y') : '',
                $animal['forsale'] ? 'Sí' : 'No',
                $animal['price'] ? $animal['price'] . '€' : '-',
                $animal['enddate'] ? $animal['enddate']->format('d/m/Y') : '',
                $father ? $father['tattoo'] : '-',
                $father ? $father['crotal'] : '-',
                $father ? $father['name'] : '-',
                (($father) ? (($father['gender']) ? \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::getReadableValue($father['gender']) : '-') : '-'),
                $mother ? $mother['tattoo'] : '-',
                $mother ? $mother['crotal'] : '-',
                $mother ? $mother['name'] : '-',
                (($mother) ? (($mother['gender']) ? \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::getReadableValue($mother['gender']) : '-') : '-'),
                $animal['gender'] ? \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::getReadableValue($animal['gender']) : '',
                $animal['registerc'],
                $animal['registerd'],
                $animal['birthweight'],
                $animal['points'],
                $animal['state'] ? \Greetik\FarmBundle\DBAL\Types\AnimalstateType::getReadableValue($animal['state']) : '',
                $animal['birthnum'],
                $animal['index_ac'],
                $animal['index_ad'],
                $animal['index_rn'],
                $animal['index_an'],
                $animal['index_epl'],
                $animal['index_gc'],
                $animal['index_ld'],
                $animal['index_lp'],
                $animal['index_aa'],
                $animal['index_tmn'],
                $animal['index_m'],
                $animal['index_aaa'],
                $animal['index_aap'],
                $animal['index_rd'],
                $animal['index_cc'],
                $animal['index_pp'],
                $animal['index_ap'],
                $animal['index_at'],
                $animal['index_ln']
            );
        }


        return array_merge(array((($animal['birthdate']) ? $animal['birthdate']->format('d/m/Y') : '-'), $animal['tattoo'] ? $animal['tattoo'] : '-'), ((empty($idfarm) || !$idfarm) ? array((($animal['registerc']) ? $animal['registerc'] : ''), (($animal['registerd']) ? $animal['registerd'] : '')) : array()), ((empty($idfarm) || !$idfarm) ? array((($animal['farm']) ? (($this->systemusers->isGrantedConnected('ROLE_EDITOR')) ? '<a href="' . $this->router->generate('farm_viewfarm', array('id' => $animal['farm'])) . '">' : '') . $animal['farmname'] . (($this->systemusers->isGrantedConnected('ROLE_EDITOR')) ? '</a>' : '') : '-')) : array()), array((($excel) ? '' : '<a href="' . $this->router->generate('farm_viewanimal', array('id' => $animal['id'])) . '">') . $animal['name'] . (($excel) ? '' : '</a>'),
            array($animal['gender'] ? \Greetik\FarmBundle\DBAL\Types\AnimalgenderType::getReadableValue($animal['gender']) : '-'),
            $animal['crotal'] ? $animal['crotal'] : '-'), ((empty($idfarm) || !$idfarm) ? array() : array((($animal['registerd']) ? $animal['registerd'] : (($animal['registerc']) ? $animal['registerc'] : '-')), $mother['name'], $father['name'])), array(
            (($animal['forsale']) ? (($animal['price']) ? $animal['price'] . '€' : 'Sí') : 'No'),
            (($animal['enddate']) ? $animal['enddate']->format('d/m/Y') : (($idfarm == $this->systemusers->getUserConnected()->getFarm()) ? array(/* '<a href="' . $this->router->generate('farm_dropanimalform', array('id' => $animal['id'])) . '" data-target="#modal_ajax" data-toggle="modal"><i class="fa fa-arrow-down"></i></a>' */'-') : array())),
                ), array('<a href="' . $this->router->generate('farm_dropanimalform', array('id' => $animal['id'])) . '" data-target="#modal_ajax" data-toggle="modal"><i class="fa fa-arrow-down"></i></a>'), (($animal['provnew'] || $animal['provend']) ? array('<i class="fa fa-exclamation"></i>') : array('-')));
    }

    /* Obtener para selector */

    public function getAnimalsSelect($search, $gender, $idfarm = '', $onlyactive = '') {
        if (!empty($idfarm)) {
            if (!$this->getFarmPerm('modifypartial', $idfarm))
                throw new \Exception('No tienes permiso para ver los animales');
        }/*
          else if (!$this->getFarmPerm('insert'))
          throw new \Exception('No tienes permiso para ver los animales');
         */
        return $this->farms->getAnimalsSelect($search, $gender, $idfarm, $onlyactive);
    }

    /* Insertar animal */

    public function insertAnimal($animal, $mother, $father) {
        if (!$this->getFarmPerm('modify', $animal->getFarm())) {
            if ($animal->getFarm()->getId() == $this->systemusers->getUserConnected()->getFarm())
                $animal->setProvnew(true);
            else
                throw new \Exception('No tienes permiso para insertar el animal');
        }

        $this->farms->insertAnimal($animal, $mother, $father);

        if ($animal->getBirthweight() && $animal->getBirthdate())
            $this->changeSpecialWeight($animal->getId(), $animal->getBirthdate(), $animal->getBirthweight(), 'birth');

        if ($animal->getWeaningweight() || $animal->getWeaningdate())
            $this->changeSpecialWeight($animal->getId(), $animal->getWeaningdate(), $animal->getWeaningweight(), 'weaning');

        return $this->log->insertEntry(LogactionsType::INSERT, $this->stringAnimalForLog($animal), $animal->getId(), 'animal');
    }

    /* Edición Parcial del animal */

    public function modifyAnimalPartial($animal, $oldanimal) {
        if (!$this->getAnimalPerm('modifypartial', $animal))
            throw new \Exception('No tienes permiso para modificar el animal');
        if ($this->systemusers->isGrantedConnected('ROLE_ADMIN')) {
            $animal->setProvnew(false);
            $animal->setProvend(false);
        } else {
            if ($animal->getEnddate())
                $animal->setProvend(true);
        }

        if ($animal->getEnddate() && !empty($oldanimal) && !$oldanimal['enddate']) {
            //Como le damos de baja salvamos el estado anterior
            $animal->setLaststate($animal->getState());
        }

        if (!$animal->getEnddate() && !empty($oldanimal) && $oldanimal['enddate']) {
            //Recuperamos su antiguo estado
            $animal->setState($animal->getLaststate());
        }

        $result = $this->farms->modifyAnimalPartial($animal);
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringAnimalForLog($animal), $animal->getId(), 'animal');
        return $result;
    }

    /* modifica un peso específico: al nacer, al destete... */

    public function changeSpecialWeight($id, $date, $weight, $extra) {
        $weights = $this->weightcontrol->getWeightcontrolByExtra($id, 'animal', $extra);
        if (count($weights) > 0) {
            $weightcontrol = $this->weightcontrol->getWeightcontrolObject($weights[0]['id']);
        } else {
            if ($date && $weight > 0) {
                $weightcontrol = new \Greetik\WeightcontrolBundle\Entity\Weightcontrol();
                $inserting = true;
            }
        }
        if (isset($weightcontrol)) {
            if ($weight > 0) {
                $weightcontrol->setItemid($id);
                $weightcontrol->setItemtype('animal');
                $weightcontrol->setWeight($weight);
                $weightcontrol->setWeightdate($date);
                $weightcontrol->setExtra($extra);

                if (isset($inserting))
                    $this->weightcontrol->insertWeightcontrol($weightcontrol);
                else
                    $this->weightcontrol->modifyWeightcontrol($weightcontrol);
            }else {
                $this->weightcontrol->deleteWeightcontrol($weightcontrol);
            }
        }

        return true;
    }

    /* Editar animal */

    public function modifyAnimal($animal, $mother, $father, $oldanimal) {
        if (!$this->getAnimalPerm('modify', $animal))
            throw new \Exception('No tienes permiso para modificar el animal');

        if ($animal->getBirthweight() != $oldanimal['birthweight'] || $animal->getBirthdate() != $oldanimal['birthdate'])
            $this->changeSpecialWeight($animal->getId(), $animal->getBirthdate(), $animal->getBirthweight(), 'birth');

        if ($animal->getWeaningweight() != $oldanimal['weaningweight'] || $animal->getWeaningdate() != $oldanimal['weaningdate'])
            $this->changeSpecialWeight($animal->getId(), $animal->getWeaningdate(), $animal->getWeaningweight(), 'weaning');

        if ($animal->getEnddate() && !$oldanimal['enddate']) {
            //Como le damos de baja salvamos el estado anterior
            $animal->setLaststate($animal->getState());
        }

        if (!$animal->getEnddate() && $oldanimal['enddate']) {
            //Recuperamos su antiguo estado
            $animal->setState($animal->getLaststate());
        }


        if ($this->systemusers->isGrantedConnected('ROLE_ADMIN')) {
            $animal->setProvnew(false);
            $animal->setProvend(false);
        } else {
            if ($animal->getEnddate())
                $animal->setProvend(true);
        }

        $result = $this->farms->insertAnimal($animal, $mother, $father);
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringAnimalForLog($animal), $animal->getId(), 'animal');
        return $result;
    }

    /* Obtener para baja */

    public function getAnimalsForEndDate($farm) {
        return $this->farms->getAnimalsForEndDate($farm);
    }

    /* Obtener para calificaciones y pesadas (de 4 a 12 meses) */

    public function getAnimalsForWeight($farm, $date) {
        return $this->farms->getAnimalsForWeight($farm, $date);
    }

    /* Obtener para asignar Registro D */

    public function animalsForRegisterD($farm, $date) {
        return $this->farms->animalsForRegisterD($farm, $date);
    }

    /* Obtener para asignar Registro N */

    public function animalsForRegisterC($farm, $date) {
        return $this->farms->animalsForRegisterC($farm, $date);
    }

    /* Obtener para venta */

    public function getAnimalsForSale() {
        $data = $this->farms->getAnimalsForSale();
        foreach ($data as $k => $v) {
            $data[$k] = $this->getPublicAnimal($v['id']);
        }

        return $data;
    }

    /* Obtener un animal */

    public function getAnimal($id, $noparents = false) {
        $animal = $this->farms->getAnimal($id, $noparents);
        if (!$animal)
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

        if (!$this->getFarmPerm('publicview', $animal['farm']))
            throw new \Exception('No tiene permiso para ver el animal');

        $animal['weightcontrol'] = $this->weightcontrol->getWeightcontrolByItem($animal['id'], 'animal');
        foreach ($animal['weightcontrol'] as $k => $v) {
            if ($animal['birthdate']) {
                $diff = $v['weightdate']->diff($animal['birthdate'])->format("%a");
            }
            $animal['weightcontrol'][$k]['weightdate'] = $v['weightdate']->format('Y-m-d') . (isset($diff) ? ' (' . $diff . ' días)' : '' );
        }
        return $animal;
    }

    /* Obtener un animal */

    public function getPublicAnimal($id) {
        $animal = $this->farms->getAnimal($id);
        $animal['images'] = $this->dataimages->getImages($animal['id'], 'animal')['files'];
        $animal['documents'] = $this->dataimages->getImages($animal['id'], 'animal', 1, 'document')['files'];

        return $animal;
    }

    /* Importar ganaderías */

    public function importFarms($filename) {
        if (!$this->getFarmPerm('insert'))
            throw new \Exception('No tienes permiso para importar ganaderías');
        $result = $this->farms->importFarms($filename);
        $this->log->insertEntry(LogactionsType::OTHER, 'Se importó un fichero de ganaderías');
        return $result;
    }

    /* Importar animales */

    public function importAnimals($filename) {
        if (!$this->getFarmPerm('insert'))
            throw new \Exception('No tienes permiso para importar animales');
        $result = $this->farms->importAnimals($filename);
        $this->log->insertEntry(LogactionsType::OTHER, 'Se importó un fichero de animales');
        return $result;
    }

    /* Importar méritos de animales */

    public function importAnimalsindex($filename) {
        if (!$this->getFarmPerm('insert'))
            throw new \Exception('No tienes permiso para importar animales');
        $result = $this->farms->importAnimalsindex($filename);
        $this->log->insertEntry(LogactionsType::OTHER, 'Se importó un fichero de méritos');
        return $result;
    }

    /* Poner fecha de baja a un animal */

    public function setAnimalEnddate($id, $enddate) {
        $animal = $this->farms->getAnimalObject($id);
        if (!$this->getAnimalPerm('modify', $animal))
            throw new \Exception('No tienes permiso para modificar el animal');

        $animal->setEnddate($enddate);
        $result = $this->farms->insertAnimal($animal, $animal->getMother(), $animal->getFather());
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringAnimalForLog($animal) . ' Se le dio de baja con la fecha ' . $enddate->format('d/m/Y') . '', $animal->getId(), 'animal');
        return $result;
    }

    /* Poner calificación a un animal */

    public function setAnimalPoints($id, $points) {
        $animal = $this->farms->getAnimalObject($id);
        if (!$this->getAnimalPerm('modify', $animal))
            throw new \Exception('No tienes permiso para modificar el animal');

        $animal->setPoints($points);
        $result = $this->farms->insertAnimal($animal, $animal->getMother(), $animal->getFather());
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringAnimalForLog($animal) . '. Se le calificó con ' . $points . 'puntos', $animal->getId(), 'animal');
        return $result;
    }

    /* Asignar Registro de Nacimiento */

    public function setAnimalRegisterC($id, $register, $date = '') {
        $animal = $this->farms->getAnimalObject($id);
        if (!$this->getAnimalPerm('modify', $animal))
            throw new \Exception('No tienes permiso para modificar el animal');

        $animal->setRegisterc($register);
        $aux = explode(' ', $animal->getRegisterc());
        $animal->setRegistercNum(isset($aux[1]) && $aux[0] == 'RNU' ? intval($aux[1]) : intval($aux[0]));
        $animal->setRegister($register);
        if ($register)
            $animal->setRnudate($date ? $date : new \Datetime());
        else
            $animal->setRnudate(null);
        $result = $this->farms->insertAnimal($animal, $animal->getMother(), $animal->getFather());
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringAnimalForLog($animal) . '. Se le asignó el Registro de Nacimiento ' . $register . '', $animal->getId(), 'animal');
        return $result;
    }

    /* Asignar Registro Definitivo */

    public function setAnimalRegisterD($id, $register, $date = '') {
        $animal = $this->farms->getAnimalObject($id);
        if (!$this->getAnimalPerm('modify', $animal))
            throw new \Exception('No tienes permiso para modificar el animal');

        $animal->setRegisterd($register);
        $aux = explode(' ', $animal->getRegisterd());
        $animal->setRegisterdNum(isset($aux[1]) && $aux[0] == 'RDU' ? intval($aux[1]) : intval($aux[0]));


        if ($register != '')
            $animal->setRegister($register);
        else
            $animal->setRegister($animal->getRegisterC());

        if ($register)
            $animal->setRdudate($date ? $date : new \Datetime());
        else
            $animal->setRdudate(null);

        $result = $this->farms->insertAnimal($animal, $animal->getMother(), $animal->getFather());
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringAnimalForLog($animal) . '. Se le asignó el Registro Definitivo ' . $register . '', $animal->getId(), 'animal');
        return $result;
    }

    public function getMaxRegisterD() {
        return $this->farms->getMaxRegisterD();
    }

}
