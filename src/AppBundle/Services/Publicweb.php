<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Response;
use Greetik\WebmodulesBundle\DBAL\Types\WebmoduleType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Publicweb {

    private $webmodules;
    private $posts;
    private $sections;
    private $webforms;
    private $catalog;
    private $maps;
    private $farms;
    private $events;
    private $router;
    private $beinterface;
    private $publicweb;

    public function __construct($_webmodules, $_posts, $_sections, $_webforms, $_events, $_catalog, $_maps, $_farms, $_router, $_beinterface, $_publicweb) {
        $this->webmodules = $_webmodules;
        $this->posts = $_posts;
        $this->sections = $_sections;
        $this->webforms = $_webforms;
        $this->events = $_events;
        $this->catalog = $_catalog;
        $this->maps = $_maps;
        $this->farms = $_farms;
        $this->router = $_router;
        $this->beinterface = $_beinterface;
        $this->publicweb = $_publicweb;
    }

    function getFirstImageSection($id, $onlyurl = false) {
        return $this->publicweb->getFirstImageSection($id, $onlyurl);
    }

    public function getRss() {
        return $this->publicweb->getRss();
    }

    public function getBreadcrumb($treesection, $category = '') {
        return $this->publicweb->getBreadcrumb($treesection, $category);
    }

    public function sendheader($nocache = false) {
        return $this->publicweb->sendheader($nocache);
    }

    public function getLasttweets($number = 4) {
        return $this->publicweb->getLasttweets($number);
    }

    public function getSection($treesection, $page = '', $simple = false, $category = '', $province = '') {
        if (is_numeric($treesection))
            $treesection = $this->sections->getTreesection($id);
        if ($simple)
            return $treesection;

        $itsblog = false;
        foreach ($treesection['modules'] as $module) {
            if ($module['moduledata']['moduletype'] == WebmoduleType::BLOG)
                $itsblog = true;
        }

        //SECCIONES ESPECIALES
        switch ($treesection['id']) {
            case '9':
                $numperpage = 200;
                $treesection['farms'] = $this->farms->getPublicFarms(((empty($page)) ? 1 : $page), $numperpage, $province);
                $treesection['numpages'] = $numpages = ceil($this->farms->getNumberOfFarms($province) / $numperpage);
                $itsblog = true;
                $treesection['provinces'] = \AppBundle\DBAL\Types\ProvinceType::getProvinces();
                break;
            case 25 :
                $treesection['animals'] = $this->farms->getAnimalsForSale();
                break;
        }


        if (!$itsblog)
            return $this->publicweb->getSection($treesection, $page, $simple, $category);

        foreach ($treesection['modules'] as $module) {
            switch ($module['moduledata']['moduletype']) {
                case WebmoduleType::BLOG:
                    if (empty($page))
                        $page = 1;
                    $numpages = ceil($this->posts->getNumberOfPostsByProject($module['module']) / 5);
                    $posts = $this->posts->getPublishedPostsByPage($module['module'], $page, 5);
                    foreach ($posts as $k => $post)
                        $posts[$k] = $this->posts->getPublicPost($post);
                    $treesection['insertmodules'][$module['module']] = $posts;
                    break;
                case WebmoduleType::TREESECTION: $treesection['insertmodules'][$module['module']] = $this->sections->getAllTreesectionsByProject($module['module']);
                    break;
                case WebmoduleType::WEBFORMS: $treesection['insertmodules'][$module['module']] = $this->webforms->getPublicFormfieldsByProject($module['module']);
                    break;
                case WebmoduleType::MAP: $treesection['insertmodules'][$module['module']] = $this->maps->getMap($module['module']);
                    break;
                case WebmoduleType::CATALOG: $treesection['insertmodules'][$module['module']] = $this->catalog->getCatalog($module['module'], $category, 'field_7');
                    break;
            }
        }


        return array($treesection, isset($numpages) ? $numpages : 0, $page);
    }

    protected function getMenu() {
        return $this->sections->getPublicTreesections(1);
    }

    public function getParamsCommon($index = false) {
        return array(
            'menuweb' => $this->getMenu(),
            'lastnews' => $this->posts->getPublishedPostsByPage(5, 1, (($index) ? 3 : 5))
        );
    }

    public function getParamsIndex() {
        return array_merge(
                array(
            'linksindex' => $this->sections->getPublicTreesection(13),
            'linksindex2' => $this->sections->getPublicTreesection(14),
            'slider' => $this->sections->getPublicTreesection(12)), $this->getParamsCommon(true));
    }

    public function getParamsSection($request, $id, $page, $category, $province = '') {
        list($section, $numpages, $currentpage) = $this->getSection($this->sections->getPublicTreesection($id), $page, false, $category, $province);
        $breadcrumb = $this->getBreadcrumb($section, $category);

        if (!empty($province) && intval($province) > 0)
            $section['title'] = 'Asociados de ' . \AppBundle\DBAL\Types\ProvinceType::getReadableValue($province);

        return array_merge(
                array(
            'currentpage' => $currentpage,
            'numpages' => $numpages,
            'section' => $section,
            'idfirstparent' => isset($breadcrumb[0]) ? $breadcrumb[0]->getId() : 0,
            'breadcrumb' => $breadcrumb)
                , $this->getParamsCommon());
    }

    public function getParamsPost($request, $id, $page) {
        $post = $this->posts->getPublicPost($id, $page);
        $breadcrumb = $this->getBreadcrumb($this->sections->getPublicTreesection($this->webmodules->getSectionsByModule($post['project'])[0]));

        $tags = $this->posts->getTagsCloud();
        shuffle($tags);

        return array_merge(
                array(
            'post' => $post,
            'idfirstparent' => isset($breadcrumb[0]) ? $breadcrumb[0]->getId() : 0,
            'breadcrumb' => $breadcrumb)
                , $this->getParamsCommon()
        );
    }

    public function getParamsTag($request, $tag, $page) {
        $posts = $this->posts->getPublishedpostsByTag(urldecode($tag), $page, 5);

        $tags = $this->posts->getTagsCloud();
        shuffle($tags);

        return array_merge(
                array(
            'tag' => urldecode($tag),
            'posts' => $posts,
            'idfirstparent' => isset($breadcrumb[0]) ? $breadcrumb[0]->getId() : 0,
            'breadcrumb' => array(),
                )
                , $this->getParamsCommon()
        );
    }

    public function getParamsFarm($request, $id) {
        $section = $this->sections->getPublicTreesection(9);
        $breadcrumb = $this->getBreadcrumb($section);

        return array_merge(
                array(
            'farm' => $this->farms->getPublicFarm($id),
            'idfirstparent' => isset($breadcrumb[0]) ? $breadcrumb[0]->getId() : 0,
            'breadcrumb' => $breadcrumb)
                , $this->getParamsCommon());
    }

    public function getParamsAnimal($request, $id) {
        $section = $this->sections->getPublicTreesection(25);
        $breadcrumb = $this->getBreadcrumb($section);

        $animal = $this->farms->getPublicAnimal($id);
        return array_merge(
                array(
            'animal' => $animal,
            'farm' => $this->farms->getPublicFarm($animal['farm']),
            'idfirstparent' => isset($breadcrumb[0]) ? $breadcrumb[0]->getId() : 0,
            'breadcrumb' => $breadcrumb)
                , $this->getParamsCommon());
    }

}
