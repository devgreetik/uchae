<?php

namespace AppBundle\Services;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dataimage Tools
 *
 * @author Pacolmg
 */
class Dataimages {

    private $dataimages;
    private $blog;
    private $treesections;
    private $events;
    private $catalog;
    private $farms;

    public function __construct($_dataimages, $_blog, $_treesections, $_events='', $_catalog='', $_farms) {
        $this->dataimages = $_dataimages;
        $this->blog = $_blog;
        $this->treesections = $_treesections;
        $this->events = $_events;
        $this->catalog = $_catalog;
        $this->farms = $_farms;
    }

    public function getFilePerm($perm, $item_type, $item_id){
        
        switch($item_type){
            case 'post': 
                if ($perm=='publicview') return true;
                if (!$this->blog->getPostPerm($perm, $item_id)) throw new \Exception('No tienes permiso para operar con el post');
                break;
            case 'treesection': 
                if ($perm=='publicview') return true;
                if (!$this->treesections->getTreesectionPerm($perm, $item_id)) throw new \Exception('No tienes permiso para operar con la sección');
                break;
            case 'event': 
                if ($perm=='publicview') return true;
                if (!$this->events->getEventPerm($perm, $item_id)) throw new \Exception('No tienes permiso para operar con el evento');
                break;
            case 'product': 
                if ($perm=='publicview') return true;
                if (!$this->catalog->getProductPerm($perm, $item_id)) throw new \Exception('No tienes permiso para operar con el producto');
                break;
            case 'animal': 
                if ($perm=='publicview') return true;
                if (!$this->farms->getAnimalPerm('modifypartial', $item_id)) throw new \Exception('No tienes permiso para operar con el animal');
                break;
            default: throw new \Exception('No se encuentra el tipo de imagen');
        }
        
        return true;
    }
    
    public function uploadImage($item_id, $item_type, $filetype = 'image') {
        $this->getFilePerm('modify', $item_type, $item_id);
        return $this->dataimages->uploadImage($item_id, $item_type, $filetype);
    }

    public function getImages($item_id, $item_type, $array_mode = 1, $filetype = "image") {
        $this->getFilePerm('view', $item_type, $item_id);
        return $this->dataimages->getImages($item_id, $item_type, $array_mode, $filetype);
    }

    
    public function modifyImage($image){
        if (is_numeric($image)) $image = $this->dataimages->getDataimageObject($image);
        $this->getFilePerm('modify', $image->getItemtype(), $image->getItemid());
        $this->dataimages->modifyImage($image);
    }
    
    public function getItemImage($image) {
        $item_type = $image->getItemtype();
        $item_id = $image->getItemid();

        switch ($item_type) {
            case 'post': 
                try {
                    $post = $this->blog->getPost($item_id);
                    $post = array('id' => $post->getId(), 'name' => $post->getTitle());
                } catch (\Exception $e) {
                    $post = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $post;
            case 'treesection':
                try {
                    $section = $this->treesections->getTreesection($item_id);
                    $section = array('id' => $section->getId(), 'name' => $section->getTitle());
                } catch (\Exception $e) {
                    $section = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $section;
            case 'product':
                try {
                    $product = $this->catalog->getProductAndData($item_id);
                    $product = array('id' => $product['fields']['id'], 'name' => $product['fields']['mainfield']);
                } catch (\Exception $e) {
                    
                    $product = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $product;
            case 'marker':
                try {
                    $marker = $this->markers->getMarker($item_id);
                    $marker = array('id' => $marker->getId(), 'name' => $marker->getTitle());
                } catch (\Exception $e) {
                    $marker = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $marker;
            default: throw new AccesDeniedException('No se encuentra el tipo de las imágenes');
        }

        return $image;
    }

    public function dropImage($id) {
        $image = $this->dataimages->getDataimageObject($id);
        $this->getFilePerm('modify', $image->getItemtype(), $image->getItemid());
        return $this->dataimages->dropImage($id);
    }

    public function moveImage($id, $newposition, $filetype = '') {
        $image = $this->dataimages->getDataimageObject($id);
        $this->getFilePerm('modify', $image->getItemtype(), $image->getItemid());

        return $this->dataimages->moveImage($id, $newposition, $filetype);
    }
}
