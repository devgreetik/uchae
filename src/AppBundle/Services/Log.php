<?php

namespace AppBundle\Services;

/**
 * Log
 *
 * @author Pacolmg
 */
class Log {

    private $_context;
    private $log;
    
    public function __construct($__context, $_log) {
        $this->_context = $__context;
        $this->log = $_log;
    }

    public function getLogPerm($type){
        return $this->_context->isGranted('ROLE_SUPERADMIN');
    }
    
    /* obtener acciones para tabla ajax */

    public function getLogTable($search, $sortcol, $sortdir, $start, $lenght, $from='', $to='', $user='', $action='', $itemid='', $ityemtype='') {
        if (!$this->getLogPerm('list')) throw new \Exception('No tienes permiso para ver el log');
        return $this->log->getLogTable($search, $sortcol, $sortdir, $start, $lenght, $from, $to, $user, $action, $itemid, $itemtype);
    }
    
    /* Insertar log */
    public function insertEntry($action, $description, $itemid='', $itemtype=''){
        return $this->log->insertEntry($action, $description, $itemid, $itemtype);
    }
}
