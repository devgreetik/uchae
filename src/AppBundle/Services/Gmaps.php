<?php

namespace AppBundle\Services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Marker Tools
 *
 * @author Pacolmg
 */
class Gmaps {

    private $markers;
    private $gpx;
    private $systemusers;

    public function __construct($_markers, $_gpx, $_systemusers) {
        $this->markers = $_markers;
        $this->gpx = $_gpx;
        $this->systemusers = $_systemusers;
    }

    /* Comprueba si el usuario tiene permiso para operar sobre el post */

    public function getMarkerPerm($type, $markert = '', $project = '') {
        if ($type == 'publicview')
            return true;

        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
    }

    public function getMapconfig($project) {
        if (!$this->getMarkerPerm('insert', '', $project))
            throw new \Exception('No tienes permiso para ver la configuración');
        return $this->markers->getMapconfig($project);
    }

    public function saveConfig($mapconfig) {
        if (!$this->getMarkerPerm('insert', '', $mapconfig->getProject()))
            throw new \Exception('No tienes permiso para salvar la configuración');

        return $this->markers->saveConfig($mapconfig);
    }

    public function getMarkersByProject($idproject) {
        if (!$this->getMarkerPerm('list'))
            throw new \Exception('No tienes permiso para listar marcadores');
        return $this->markers->getMarkersByProject($idproject);
    }

    public function getMarker($id) {
        $marker = $this->markers->getMarker($id);
        if (!$this->getMarkerPerm('view', $marker))
            throw new \Exception('No tienes permiso para ver el marcador');

        return $marker;
    }

    public function modifyMarker($marker) {
        if (!$this->getMarkerPerm('modify', $marker))
            throw new \Exception('No tienes permiso para eliminar el marcador');
        $this->markers->modifyMarker($marker);
    }

    public function insertMarker($marker, $user, $project) {
        if (!$this->getMarkerPerm('insert', $marker))
            throw new \Exception('No tienes permiso para insertar el marcador');
        $this->markers->insertMarker($marker, $user, $project);
    }

    public function deleteMarker($marker) {
        if (!$this->getMarkerPerm('delete', $marker))
            throw new \Exception('No tienes permiso para eliminar el marcador');
        $this->markers->deleteMarker($marker);
    }

    public function getGpx($id) {
        $gpx = $this->gpx->getGpx($id);
        if (!$this->getGpxPerm('view', $gpx))
            throw new \Exception('No tienes permiso para ver el gpx');

        return $gpx;
    }

    public function modifyGpx($gpx) {
        if (!$this->getGpxPerm('modify', $gpx))
            throw new \Exception('No tienes permiso para eliminar el gpx');
        $this->gpx->modifyGpx($gpx);
    }

    public function insertGpx($gpx, $uploadedfile, $module) {
        if (!$this->getGpxPerm('insert', $gpx))
            throw new \Exception('No tienes permiso para insertar el gpx');

        $this->gpx->insertGpx($gpx, $uploadedfile, $module);
    }

    public function deleteGpx($gpx) {
        if (!$this->getGpxPerm('delete', $gpx))
            throw new \Exception('No tienes permiso para eliminar el gpx');
        $this->gpx->deleteGpx($gpx);
    }

    public function getGpxPerm($type, $gpx = '', $project = '') {
        if ($type == 'publicview')
            return true;

        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
    }

    public function getGpxsByProject($project) {
        if (!$this->getGpxPerm('list', '', $project))
            throw new \Exception('No tienes permiso para listar los gpx');

        return $this->gpx->getGpxsByProject($project);
    }

}
