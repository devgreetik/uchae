<?php

namespace AppBundle\Services;

/**
 * Description of Events Tools
 *
 * @author Pacolmg
 */
class Events {

    private $events;
    private $systemusers;
    private $itemlinks;
    private $dataimages;
    private $ytvideos;

    public function __construct($_events, $_systemusers, $_itemlinks, $_dataimages, $_ytvideos) {
        $this->events = $_events;
        $this->systemusers = $_systemusers;
        $this->itemlinks = $_itemlinks;
        $this->dataimages = $_dataimages;
        $this->ytvideos = $_ytvideos;
    }

    /* Comprueba si el usuario tiene permiso para operar sobre el event */

    public function getEventPerm($type, $event = '', $project='') {
        if ($type == 'publicview')
            return true;

        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
    }

    /* obtener el listado de events */

    public function getAllEventsByProject($project = '') {
        if (!$this->getEventPerm('list'))
            throw new \Exception('No tienes permiso para listar los eventos');
        return $this->events->getAllEventsByProject($project);
    }

    /* obtener un event */

    public function getEvent($id) {
        if (!$this->getEventPerm('view'))
            throw new \Exception('No tienes permiso para ver el evento');
        return $this->getPublicEvent($id);
    }

    /* eliminar un event */

    public function deleteEvent($event) {
        if (!$this->getEventPerm('delete'))
            throw new \Exception('No tienes permiso para eliminar el evento');

        $this->deleteVideosandImages($event->getId());
        return $this->events->deleteEvent($event);
    }

    /* insertar un event */

    public function insertEvent($event, $project = '', $user = '') {
        if (!$this->getEventPerm('delete'))
            throw new \Exception('No tienes permiso para insertar el evento');
        return $this->events->insertEvent($event, $project, $this->systemusers->getUserConnected()->getId());
    }

    /* modificar un event */

    public function modifyEvent($event) {
        if (!$this->getEventPerm('modify'))
            throw new \Exception('No tienes permiso para modificar el evento');
        return $this->events->modifyEvent($event);
    }

    /* obtener un event */

    public function getPublicEvent($id) {
        $event = $this->events->getEvent($id);
        $event['itemlinks'] = $this->itemlinks->getItemlinksByItem($id, 'event');
        $event['images'] = $this->dataimages->getImages($id, 'event')['files'];
        $event['documents'] = $this->dataimages->getImages($id, 'event', 1, 'document')['files'];
        $event['videos'] = $this->ytvideos->getVideos($id, 'event');

        return $event;
    }

    protected function deleteVideosandImages($event) {
        if (is_numeric($event))
            $event = $this->getPublicEvent($event);

        foreach ($event['videos'] as $video)
            $this->ytvideos->dropVideo($video['id']);

        foreach ($event['itemlinks'] as $link)
            $this->itemlinks->deleteItemlink($link['id']);

        $_SERVER['REQUEST_METHOD'] = 'POST';
        foreach (array_merge($event['images'], $event['documents']) as $image) {
            $this->dataimages->dropImage($image['id']);
        }
    }

    public function getEvents($project = '', $from = '', $to = '') {
        if (!$this->getEventPerm('list', '', $project)) throw new \Exception('No tienes permiso para ver los eventos');
        return $this->events->getEvents($project, $from, $to);
    }

}
