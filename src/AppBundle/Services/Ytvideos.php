<?php

namespace AppBundle\Services;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Toolsitems
 *
 * @author Paco
 */

class Ytvideos {
    
    private $ytvideos;
    private $blog;
    private $treesections;
    private $events;
    private $catalog;
    
    public function __construct($_ytvideos, $_blog, $_treesections, $_events='', $_catalog='') {
        $this->ytvideos = $_ytvideos;
        $this->blog = $_blog;
        $this->treesections = $_treesections;
        $this->events = $_events;
        $this->catalog = $_catalog;
    }

    public function getVideoPerm($perm, $item_type, $item_id){
        
        switch($item_type){
            case 'post': 
                if ($perm=='publicview') return true;
                if (!$this->blog->getPostPerm($perm, $item_id)) throw new AccessDeniedException('No tienes permiso para operar con el post');
                break;
            case 'treesection': 
                if ($perm=='publicview') return true;
                if (!$this->treesections->getTreesectionPerm($perm, $item_id)) throw new AccessDeniedException('No tienes permiso para operar con la sección');
                break;
            case 'event': 
                if ($perm=='publicview') return true;
                if (!$this->events->getEventPerm($perm, $item_id)) throw new AccessDeniedException('No tienes permiso para operar con el evento');
                break;
            case 'product': 
                if ($perm=='publicview') return true;
                if (!$this->catalog->getProductPerm($perm, $item_id)) throw new \Exception('No tienes permiso para operar con el producto');
                break;
            default: throw new \Exception('No se encuentra el tipo de vídeo');
        }
        
        return true;
    }
    
    /**
    * Get the videos
    * 
    * @param int $item_id The videos are associated to an item with this id
    * @param string $item_type It's the type of the item that the videos are associated to
    * @return an array with the videos
    * @author Pacolmg
    */    
    public function getVideos($item_id, $item_type){
        $this->getVideoPerm('view', $item_type, $item_id);
        return $this->ytvideos->getVideos($item_id, $item_type);
    }

  
    /**
    * Get a video
    * 
    * @param int $id The id of the video
    * @return Ytvideo
    * @author Pacolmg
    */    
    public function getVideo($id){
        $video = $this->ytvideos->getVideo($id);
        if (!$video) return '';
        $this->getVideoPerm('view', $video['itemtype'], $video['itemid']);
        return $video;
    }
    
    
    /**
    * Persist the new video in the bd and set its order
    * 
    * @param int $url the url of the video
    * @param Ytvideo $ytvideo the video
    * @return string the id of the youtube video
    * @author Pacolmg
    */       
    public function insertVideo($ytvideo, $url, $item_id, $type){
        $this->getVideoPerm('modify', $type, $item_id);
        return $this->ytvideos->insertVideo($ytvideo, $url, $item_id, $type);
    }    
    /**
    * Persist the new/modified video in the bd and set its order
    * 
    * @param int $url the url of the video
    * @param Ytvideo $ytvideo the video
    * @return string the id of the youtube video
    * @author Pacolmg
    */       
    public function modifyVideo($ytvideo, $url){
        $this->getVideoPerm('modify', $ytvideo->getItemtype(), $ytvideo->getItemid());
        
        return $this->ytvideos->modifyVideo($ytvideo,$url);
    }

    /**
    * Remove the video
    * 
    * @param int id the Id of the Ytvideo
    * @author Pacolmg
    */
    public function dropVideo($id){
        $video = $this->getVideo($id);
        $this->getVideoPerm('modify', $video['itemtype'], $video['itemid']);
        return $this->ytvideos->dropVideo($id);
    }

   /**
    * Move an video from its old position to a new position
    * 
    * @param int $id The video
    * @param int $newposition the new position of the video
    * @author Pacolmg
    */    
    public function moveVideo($id, $newposition){
        $video = $this->getVideo($id);
        $this->getVideoPerm('modify', $video['itemtype'], $video['itemid']);
        return $this->ytvideos->moveVideo($id, $newposition);
    }     
    
    /**
    * Order of the videos of an item, from 1 to n
    * 
    * @param int $item_id The images are associated to an item with this id
    * @param string $item_type It's the type of the item that the images are associated to
    * @return true/false
    * @author Pacolmg
    */    
    public function reorderVideos($item_id, $item_type){
        $this->getVideoPerm('modify', $item_type, $item_id);        
        return $this->ytvideos->reorderVideos($item_id, $item_type);
    }
  }
