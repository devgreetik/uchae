<?php

namespace AppBundle\Services;

use AppBundle\DBAL\Types\WebmoduleType;
use Greetik\WebmodulesBundle\Entity\Webmodule;
/**
 * Description of Webmodules Tools
 *
 * @author Pacolmg
 */
class Webmodules {

    private $webmodules;
    private $sectionmodules;
    private $systemusers;

    public function __construct($_webmodules, $_sectionmodules, $_systemusers) {
        $this->sectionmodules = $_sectionmodules;
        $this->webmodules = $_webmodules;
        $this->systemusers = $_systemusers;
    }

    /* Comprueba si el usuario tiene permiso para operar sobre el webmodule */

    public function getWebmodulePerm($type, $webmodule = '') {
        $roleconnected = $this->systemusers->roleConnected();
        
        if ($type=='insert') return ($roleconnected == 'ROLE_SUPERADMIN');
        
        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
        return false;
    }

    /* obtener el listado de módulos de un tipo*/

    public function getWebmodulesByType($type) {
        if (!$this->getWebmodulePerm('list'))
            throw new \Exception('No tienes permiso para listar los módulos');
        
        return $this->webmodules->getWebmodulesByType($type);
    }


    /* obtener el listado de módulos */

    public function getAllWebmodulesByProject($project = '') {
        if (!$this->getWebmodulePerm('list'))
            throw new \Exception('No tienes permiso para listar los módulos');
        
        return $this->webmodules->getAllWebmodulesByProject($project);
    }

    /* obtener un módulo */

    public function getWebmodule($id) {
        if (!$this->getWebmodulePerm('view'))
            throw new \Exception('No tienes permiso para ver el módulo');
        return $this->getPublicWebmodule($id);
    }

    /* eliminar un módulo */

    public function deleteWebmodule($webmodule) {
        if (!$this->getWebmodulePerm('delete'))
            throw new \Exception('No tienes permiso para eliminar el módulo');
        
        return $this->webmodules->deleteWebmodule($webmodule);
    }

    /* insertar un módulo */

    public function insertWebmodule($webmodule, $project = '') {
        if (!$this->getWebmodulePerm('delete'))
            throw new \Exception('No tienes permiso para insertar el módulo');
        
        $idwebmodule = $this->webmodules->insertWebmodule($webmodule, $project);        
        return $idwebmodule;
    }

    /* modificar un módulo */

    public function modifyWebmodule($webmodule, $oldtype='') {
        if (!$this->getWebmodulePerm('modify'))
            throw new \Exception('No tienes permiso para modificar el módulo');
       
        return $this->webmodules->modifyWebmodule($webmodule, $oldtype);
    }

    /* Obtener datos sin permisos */

    public function getPublicWebmodule($id) {
        return $this->webmodules->getWebmodule($id);
    }

    /*Obtiene en qué secciones está agregado el módulo del parámetro*/
    public function getSectionsByModule($module){
        $data = array();
        $sectionmodules =  $this->sectionmodules->getSectionmodulesByModule($module);
        foreach ($sectionmodules as $sectionmodule) {
            array_push($data, $sectionmodule['section']);
        }
        return $data;
    }
    
}
