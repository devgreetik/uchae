<?php

namespace AppBundle\Services;

/**
 * Description of Blog Tools
 *
 * @author Pacolmg
 */
class Printapp {

    private $pdf;
    private $templating;
    private $farms;
    private $rootdir;

    public function __construct($_pdf, $_templating, $_farms, $_rootdir) {
        $this->pdf = $_pdf;
        $this->templating = $_templating;
        $this->farms = $_farms;
        $this->rootdir = $_rootdir;
    }

    /* Carta genealógica */

    public function heritageCard($id) {
        $filepath = $this->rootdir . '/../var/animalcerts/' . $id . '/cartagenealogica.pdf';
        if (file_exists($filepath)){
            @unlink($filepath);
            //return true;
        }

        try{
            $animal = $this->farms->getAnimal($id);

            if (!$animal['registerd']) throw new \Exception('El animal no tiene asignado registro definitivo');
            $this->pdf->generateFromHtml(
                    $this->templating->render(
                            'AppBundle:Print:heritagecard.html.twig', array(
                        'item' => $animal
                    )), $filepath, array(
                        'orientation' => 'Landscape',
                        'encoding' => 'utf-8',
                        'margin-top'=>5,
                        'margin-right' => 0,
                        'margin-bottom' => 0,
                        'margin-left' => 0
                            )
            );
        }catch(\Exception $e){
            return false;
        }
        return true;
    }

    public function birthRegister($id) {
        $filepath = $this->rootdir . '/../var/animalcerts/' . $id . '/certificadonacimiento.pdf';
        if (file_exists($filepath)){
            @unlink($filepath);
            //return true;
        }

        try{
            $animal = $this->farms->getAnimal($id);
            if (!$animal['registerc']) throw new \Exception('El animal no tiene asignado registro de nacimiento');
            $this->pdf->generateFromHtml(
                    $this->templating->render(
                            'AppBundle:Print:birthregister.html.twig', array(
                        'item' => $animal
                    )), $filepath, array(
                        'orientation' => 'Landscape',
                        'encoding' => 'utf-8',
                        'margin-top'=>0,
                        'margin-right' => 0,
                        'margin-bottom' => 0,
                        'margin-left' => 0
                            )
            );
        }catch(\Exception $e){
            return false;
        }
        return true;
    }
    
    public function visitletter($visit, $num=''){
        $filepath = $this->rootdir . '/../var/visits/' . $visit['id'] . '/carta'.$num.'.pdf';
        @unlink($filepath);
        //if (file_exists($filepath))
          //  return true;

        try{
            $this->pdf->generateFromHtml(
                    $this->templating->render(
                            'AppBundle:Print:visitletter'.$num.'.html.twig', array(
                        'visit' => $visit
                    )), $filepath, array(
                        'encoding' => 'utf-8',
                        'margin-top'=>3,
                        'margin-right' => $num==2 ? 5:5,
                        'margin-bottom' =>$num==2 ? 5:5,
                        'margin-left' => $num==2 ? 10:5
                            )
            );
        }catch(\Exception $e){
            throw $e;
            return false;
        }
        return true;        
    }

    public function cubriciones($farm, $num, $price, $semester){
        $filepath = $this->rootdir . '/../var/cubriciones/cubriciones-'.$farm['abb'].'.pdf';
        @unlink($filepath);
        //if (file_exists($filepath))
          //  return true;

        try{
            $this->pdf->generateFromHtml(
                    $this->templating->render(
                            'AppBundle:Print:cubriciones.html.twig', array(
                        'farm' => $farm,
                        'num'=>$num,
                        'price'=>$price,
                        'semester'=>$semester
                    )), $filepath, array(
                        'encoding' => 'utf-8',
                        'margin-top'=>3,
                        'margin-right' => $num==2 ? 5:5,
                        'margin-bottom' =>$num==2 ? 5:5,
                        'margin-left' => $num==2 ? 10:5
                            )
            );
        }catch(\Exception $e){
            throw $e;
            return false;
        }
        return true;        
    }

}
