<?php

namespace AppBundle\Services;



/**
 * Description of Blog Tools
 *
 * @author Pacolmg
 */
class Blog {

    
    private $blog;
    private $systemusers;
    private $itemlinks;
    private $dataimages;
    private $ytvideos;
    
    public function __construct($_blog, $_systemusers, $_itemlinks, $_dataimages, $_ytvideos) {
        $this->blog = $_blog;
        $this->systemusers = $_systemusers;
        $this->itemlinks = $_itemlinks;
        $this->dataimages = $_dataimages;
        $this->ytvideos = $_ytvideos;
    }

    /*Comprueba si el usuario tiene permiso para operar sobre el post*/
    public function getPostPerm($type, $post=''){
        if ($type=='publicview') return true;
        
        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
    }

    /* obtener el listado de posts */
    public function getAllPostsByProject($project='') {
        if (!$this->getPostPerm('list')) throw new \Exception('No tienes permiso para listar las posts');
        return $this->blog->getAllPostsByProject($project);
    }
    
    
    /* obtener un post */
    public function getPost($id) {
        if (!$this->getPostPerm('view')) throw new \Exception('No tienes permiso para ver el post');
        return $this->getPublicPost($id);
    }
        
    /* eliminar un post */
    public function deletePost($post) {
        if (!$this->getPostPerm('delete')) throw new \Exception('No tienes permiso para eliminar el post');
        
        $this->deleteVideosandImages($post->getId());
        return $this->blog->deletePost($post);
    }

    /* insertar un post */
    public function insertPost($post, $project='', $user='') {
        if (!$this->getPostPerm('delete')) throw new \Exception('No tienes permiso para insertar el post');
        return $this->blog->insertPost($post, $project, $this->systemusers->getUserConnected()->getId());
    }

    /* modificar un post */
    public function modifyPost($post, $oldtags) {
        if (!$this->getPostPerm('modify')) throw new \Exception('No tienes permiso para modificar el post');
        return $this->blog->modifyPost($post, $oldtags);
    }
    
    /*Cambia el avatar de un post*/
    public function getDifferentTags($project = '', $forselect = false) {
        return $this->blog->getDifferentTags($project, $forselect);
    }
    
    /* obtener un post */
    public function getPublicPost($post) {
        if (is_numeric($post)) $post = $this->blog->getPost($post);
        $post['itemlinks'] = $this->itemlinks->getItemlinksByItem($post['id'], 'post');
        $post['images'] = $this->dataimages->getImages($post['id'], 'post')['files'];
        $post['documents'] = $this->dataimages->getImages($post['id'], 'post', 1, 'document')['files'];
        $post['videos'] = $this->ytvideos->getVideos($post['id'], 'post');
        
        return $post;
    }

    /*Obtener los publicados por número de página*/
    public function getPublishedPostsByPage($id, $page, $numposts) {
       foreach($data = $this->blog->getPublishedPostsByPage($id, $page, $numposts) as $k=>$v){
           $data[$k] = $this->getPublicPost($v);
       }
       return $data;
    }   

    /*Obtener la nube de Tags*/
    public function getTagsCloud($project=''){
        return $this->blog->getTagsCloud($project);
    }
    
    /*Obtener número de posts */
    public function getNumberOfPostsByProject($idproject){
        return $this->blog->getNumberOfPostsByProject($idproject);
    }
    
    protected function deleteVideosandImages($post) {
        if (is_numeric($post))
            $post = $this->getPublicPost($post);

        foreach ($post['videos'] as $video)
            $this->ytvideos->dropVideo($video['id']);
        
        foreach ($post['itemlinks'] as $link)
            $this->itemlinks->deleteItemlink($link['id']);
        
        $_SERVER['REQUEST_METHOD'] = 'POST';
        foreach (array_merge($post['images'], $post['documents']) as $image) {
            $this->dataimages->dropImage($image['id']);
        }
    }    
}
