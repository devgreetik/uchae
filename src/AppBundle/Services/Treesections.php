<?php

namespace AppBundle\Services;
use Greetik\TreesectionBundle\Entity\Treesection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Treesection Tools
 *
 * @author Pacolmg
 */
class Treesections {

    private $treesections;
    private $systemusers;
    private $itemlinks;
    private $dataimages;
    private $ytvideos;
    private $sectionmodules;
    
    public function __construct($_treesections, $_systemusers, $_itemlinks, $_dataimages, $_ytvideos, $_sectionmodules) {
        $this->treesections = $_treesections;
        $this->systemusers = $_systemusers;
        $this->itemlinks = $_itemlinks;
        $this->dataimages = $_dataimages;
        $this->ytvideos = $_ytvideos;
        $this->sectionmodules = $_sectionmodules;
    }
    
    /*Comprueba si el usuario tiene permiso para operar sobre la sección*/
    public function getTreesectionPerm($type, $treesection='', $project=''){
        if ($type=='publicview') return true;
        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
    }


    protected function deleteVideosandImagesTreeSection($treesection) {
        if (is_numeric($treesection))
            $treesection = $this->getTreesection($treesection);

        foreach ($treesection['videos'] as $ytvideo)
            $this->ytvideos->dropVideo($ytvideo['id']);
       
        foreach ($treesection['itemlinks'] as $link)
            $this->itemlinks->deleteItemlink($link['id']);

        
        $_SERVER['REQUEST_METHOD'] = 'POST';
        foreach (array_merge($treesection['documents'], $treesection['images']) as $image) {
            $this->dataimages->dropImage($image['id']);
        }
    }

    public function getTreesectionsByProject($project){
        if (!$this->getTreesectionPerm('list')) throw new \Exception('No tienes permiso para listar las secciones');
        return $this->treesections->getTreesectionsByProject($project);
    }

    public function insertTreesection($treesection, $project, $user){
        if (!$this->getTreesectionPerm('insert', $treesection, $project)) throw new \Exception('No tienes permiso para insertar la sección');
        return $this->treesections->insertTreesection($treesection, $project, $user);
    }
    
    public function modifyTreesection($treesection) {
        if (!$this->getTreesectionPerm('modify', $treesection)) throw new \Exception('No tienes permiso para modificar la sección');
        return $this->treesections->modifyTreesection($treesection);
    }

    public function moveTreesections($newtreesections, $idproject, $node = '') {
        if (!$this->getTreesectionPerm('modify', '', $idproject)) throw new \Exception('No tienes permiso para modificar la sección');
        return $this->treesections->moveTreesections($newtreesections, $idproject, $node);
    }

    protected function removeSection($treesection) {
        foreach ($treesection as $v) {
            if (isset($v['__children']) && count($v['__children']) > 0)
                $this->removeSection($v['__children']);

            $this->deleteVideosandImagesTreeSection($v['id']);
        }
    }
    
    public function deleteTreesection($treesection){
        $completetree = $this->treesections->getTreesectionsByProject($treesection->getProject(), $treesection);
       
        $this->deleteVideosandImagesTreeSection($treesection->getId());
        $this->removeSection($completetree);
        $this->treesections->deleteTreesection($treesection);
        
    }
    /* obtener una sección */
    public function getTreesection($id) {
        if (!$this->getTreesectionPerm('view')) throw new \Exception('No tienes permiso para ver el post');
        return $this->getPublicTreesection($id);
    }

    
    /*obtener un árbol sin pasar por permisos*/
    public function getPublicTreesections($project){
       return $this->treesections->getTreesectionsByProject($project); 
    }
    
    /*Obtiene el camino de hijo a padre en un array*/
    public function getPath($treesection){
        return $this->treesections->getPath($treesection);
    }
    
    /* obtener una sección */
    public function getPublicTreesection($id) {
        $treesection = $this->treesections->getTreesection($id);
        $treesection['itemlinks'] = $this->itemlinks->getItemlinksByItem($id, 'treesection');
        $treesection['images'] = $this->dataimages->getImages($id, 'treesection')['files'];
        $treesection['documents'] = $this->dataimages->getImages($id, 'treesection', 1, 'document')['files'];
        $treesection['videos'] = $this->ytvideos->getVideos($id, 'treesection');
        $treesection['modules'] = $this->sectionmodules->getModules($id);
        $treesection['children'] = $this->treesections->getTreesectionsByProject($treesection['project'], $id);
                
        return $treesection;
    }    
    
}
