<?php

namespace AppBundle\Services;

use Greetik\FarmBundle\Entity\Fair;
use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;

/**
 * Description of Fair Tools
 *
 * @author Pacolmg
 */
class Fairs {

    private $__context;
    private $fairs;
    private $systemusers;
    private $log;
    
    public function __construct($_context, $_fairs, $_systemusers, $_log) {
        $this->__context = $_context;
        $this->fairs = $_fairs;
        $this->systemusers = $_systemusers;
        $this->log = $_log;
    }

    /*cadena para descripción del log*/
    protected  function stringForLog($item){
        if (is_numeric($item)){
            $item = $this->fairs->getFairObject($item);
        }
        return 'Feria #'.$item->getId().'. El día '.$item->getFairdate()->format('d/m/Y');
    }

    /* obtener permiso para un feria */
    public function getFairPerm($fair, $type = '') {
        if ($type == 'view' || $type=='get')
            return true;
        
        return $this->__context->isGranted('ROLE_ADMIN');
        /*
        $roletype = $this->systemusers->getRoleTypeUserConnected();
        
        if ($this->__context->isGranted('ROLE_ADMINISTRATIVOSER') && ($roletype=='admin' || $roletype=='ser')) return true;
            return false;

            
        return true;*/
    }

    /* obtener un feria */
    public function getFair($id) {
        $fair = $this->fairs->getFairById($id);
        if (!$this->getFairPerm($fair, 'view'))
            throw new \Exception('No tienes permiso para ver el feria');

        return $fair;
    }
    
    /* eliminar un feria */
    public function deleteFair($fair) {
        $stringForLog = $this->stringForLog(((is_numeric($fair))?$this->fairs->getFairObject($fair):$fair));
        $id = $fair->getId();
        
        if (!$this->getFairPerm($fair, 'delete'))
            throw new \Exception('No tienes permiso para eliminar el feria');
        $this->fairs->deleteFair($fair);
        $this->log->insertEntry(LogactionsType::DELETE, $stringForLog, $id, 'fair');
    }

    /* insertar un feria */
    public function insertFair($fair) {
        if (!$this->getFairPerm($fair, 'insert'))
            throw new \Exception('No tienes permiso para insertar el feria');
        
        try{
            $this->fairs->insertFair($fair);
            $this->log->insertEntry(LogactionsType::INSERT, $this->stringForLog($fair), $fair->getId(), 'fair');
        }catch(\Exception $e){
            $pos = strpos($e->getMessage(), ' 1062 ');
            if ($pos!==false) throw new \Exception('El día '.$fair->getFairdate()->format('d/m/Y').' ya es feria.');
            throw $e;
        }
    }
    
    /* insertar una sección */
    public function insertFairsection($fairsection) {
        if (!$this->getFairPerm($fairsection->getFair(), 'modify'))
            throw new \Exception('No tienes permiso para insertar la sección');
        
        $this->fairs->insertFairsection($fairsection);
        $this->log->insertEntry(LogactionsType::EDIT, 'Se insertó una nueva sección con Id #'.$fairsection->getId().'. '.$this->stringForLog($fairsection->getFair()), $fairsection->getId(),'fairsection');
        
    }
    
    /* eliminar una sección */
    public function deleteFairsection($fairsection) {
        if (!$this->getFairPerm($fairsection->getFair(), 'modify'))
            throw new \Exception('No tienes permiso para eliminar la sección');
        
        $id = $fairsection->getId();
        $stringforlog = $this->stringForLog($fairsection->getFair());
        $this->fairs->deleteFairsection($fairsection);
        $this->log->insertEntry(LogactionsType::EDIT, 'Se eliminó una sección con Id #'.$fairsection->getId().'. '.$stringforlog, $id, 'fairsection');
    }
    
    /* Insertar un animal */
    public function insertAnimal($animal, $fair){
        if (!$fair) return;
        if (!$this->getFairPerm($fair, 'modify'))
            throw new \Exception('No tienes permiso para insertar el animal');
    
        $this->fairs->insertAnimal($animal, $fair);
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringForLog($this->fairs->getFairObject($fair)).' Se eliminó un animal con Id #'.$animal, $fair, 'fair');
    }
    
    /* Eliminar un animal */
    public function deleteAnimal($animal, $fair){
        if (!$fair) return;
        if (!$this->getFairPerm($fair, 'modify'))
            throw new \Exception('No tienes permiso para insertar el animal');
    
        $this->fairs->deleteAnimal($animal, $fair);
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringForLog($this->fairs->getFairObject($fair)).' Se eliminó un animal con Id #'.$animal, $fair, 'fair');
    }
}