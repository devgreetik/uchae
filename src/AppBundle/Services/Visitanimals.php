<?php

namespace AppBundle\Services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Visitanimals Tools
 *
 * @author Pacolmg
 */
class Visitanimals {

    private $em;

    public function __construct($_entityManager) {
        $this->em = $_entityManager;
    }

   
    public function getVisitanimalsByVisit($visit) {
        return $this->em->getRepository('AppBundle:Visitanimal')->getVisitanimalsByVisit($visit);
    }

    public function getVisitanimalObject($id) {
            return $this->em->getRepository('AppBundle:Visitanimal')->findOneById($id);
    }

    public function getVisitanimal($id) {
            return $this->em->getRepository('AppBundle:Visitanimal')->getVisitanimal($id);
    }

    public function modifyVisitanimal($visitanimal) {
        $this->em->persist($visitanimal);
        $this->em->flush();
    }

    public function insertVisitanimal($visitanimal) {
        if ($visitanimal->getEndDate()){
            $visitanimal->setToFair(null);
        }
        $this->em->persist($visitanimal);
        $this->em->flush();
    }

    public function deleteVisitanimal($visitanimal) {
        if (is_numeric($visitanimal)) $visitanimal = $this->getVisitanimalObject ($visitanimal);
        $this->em->remove($visitanimal);
        $this->em->flush();
    }
    
    public function getVisitanimalByAnimalAndVisit($visit, $animal){
        return $this->em->getRepository('AppBundle:Visitanimal')->getVisitanimalByAnimalAndVisit($visit, $animal);
    }
}
