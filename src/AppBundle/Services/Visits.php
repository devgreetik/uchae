<?php

namespace AppBundle\Services;

use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;

/**
 * Description of Blog Tools
 *
 * @author Pacolmg
 */
class Visits {

    private $visits;
    private $farms;
    private $visitanimals;
    private $systemusers;
    private $fairs;
    private $log;

    public function __construct($_visits, $_farms, $_fairs, $_visitanimals, $_systemusers, $_log) {
        $this->visits = $_visits;
        $this->farms = $_farms;
        $this->fairs = $_fairs;
        $this->visitanimals = $_visitanimals;
        $this->systemusers = $_systemusers;
        $this->log = $_log;
    }

    /* cadena para descripción del log */

    protected function stringForLog($item) {
        if (is_numeric($item)) {
            $item = $this->visits->getVisitObject($item);
        }
        return 'Visita #' . $item->getId() . '. El día ' . $item->getVisitdate()->format('d/m/Y') . ' a la ganadería con Id #' . $item->getItemid();
    }

    public function getVisitPerm($perm, $item_type, $item_id = '') {
        switch ($item_type) {
            case 'farm': if (!$this->farms->getFarmPerm($perm, $item_id))
                    throw new \Exception('No tienes permiso para operar con la granja');
                break;
            default: throw new \Exception('No se encuentra el tipo de visita');
        }

        return true;
    }

    public function getVisit($id) {
        $visit = $this->visits->getVisit($id);
        $this->getVisitPerm('view', $visit['itemtype'], $visit['itemid']);

        $visit = array_merge($visit, array('num_index' => 0, 'num_registerc' => 0, 'num_registerd' => 0, 'num_weights' => 0, 'num_fairs' => 0, 'num_end' => 0));
        if ($visit['itemtype'] == 'farm') {
            $visit['animals'] = $this->visitanimals->getVisitanimalsByVisit($id);
            $visit['farm'] = $this->farms->getFarm($visit['itemid']);

            foreach ($visit['animals'] as $k => $v) {

                $visit['animals'][$k]['goto'] = '<i class="fa fa-search"></i>';
                try {
                    $fair = $this->fairs->getFair($v['tofair']);
                    if ($fair) {
                        $visit['animals'][$k]['fair'] = $fair['name'];
                    }
                } catch (\Exception $e) {
                    $visit['animals'][$k]['fair'] = '';
                }

                //determinamos la acción
                $visit['animals'][$k]['action'] = '-';
                if ($visit['animals'][$k]['tofair']) {
                    $visit['num_fairs'] ++;
                    $visit['animals'][$k]['action'] = 'Feria';
                }
                if ($visit['animals'][$k]['register']) {
                    if (strpos($visit['animals'][$k]['register'], 'RNU') !== false) {
                        $visit['num_registerc'] ++;
                    } else {
                        $visit['num_registerd'] ++;
                    }
                    $visit['animals'][$k]['action'] = 'Registro';
                }
                if ($visit['animals'][$k]['weight']>0) {
                    $visit['num_weights'] ++;
                    $visit['animals'][$k]['action'] = 'Peso';
                }
                if ($visit['animals'][$k]['points']) {
                    $visit['num_registerd'] ++;
                    $visit['animals'][$k]['action'] = 'Cal.';
                }
                if ($visit['animals'][$k]['indexanimal']) {
                    $visit['num_index'] ++;
                    $visit['animals'][$k]['action'] = 'Índices';
                }
                if ($visit['animals'][$k]['enddate']) {
                    $visit['num_index'] ++;
                    $visit['animals'][$k]['action'] = 'Baja';
                }
            }
        }
        return $visit;
    }

    public function insertVisit($visit) {
        $visit->setUser($this->systemusers->getUserConnected()->getId());
        $this->getVisitPerm('modify', $visit->getItemtype(), $visit->getItemid());

        $result = $this->visits->insertVisit($visit);
        $this->log->insertEntry(LogactionsType::INSERT, $this->stringForLog($visit), $visit->getId(), 'visit');
        return $result;
    }

    public function getVisitsByItemtype($item_type) {
        $this->getVisitPerm('insert', $item_type);
        $data = array();
        foreach ($visits = $this->visits->getVisitsByItemtype($item_type) as $visit) {
            $visit = $this->getVisit($visit['id']);
            $farm = $this->farms->getFarm($visit['itemid']);
            $visit['statename'] = $visit['state'] ? \Greetik\VisitsBundle\DBAL\Types\VisitstateType::getReadableValue($visit['state']) : '';
            $visit['farmname'] = $farm ? $farm['name'] : '';
            $data[] = $visit;
        }
        return $data;
    }

    public function getVisitsByItem($item_id, $item_type) {
        $this->getVisitPerm('view', $item_type, $item_id);
        return $this->visits->getVisitsByItem($item_id, $item_type);
    }

    public function modifyVisit($visit) {
        $this->getVisitPerm('modify', $visit->getItemtype(), $visit->getItemid());
        $result = $this->visits->modifyVisit($visit);
        $this->log->insertEntry(LogactionsType::EDIT, $this->stringForLog($visit), $visit->getId(), 'visit');
        return $result;
    }

    public function deleteVisit($visit) {
        $this->getVisitPerm('modify', $visit->getItemtype(), $visit->getItemid());
        $stringforlog = $this->stringForLog($visit);
        $id = $visit->getId();
        $result = $this->visits->deleteVisit($visit);
        $this->log->insertEntry(LogactionsType::DELETE, $stringforlog, $id, 'visit');
        return $result;
    }

    public function insertAnimal($visitanimal) {
        $warning = 'Atención';
        $visit = $this->visits->getVisit($visitanimal->getVisit());
        if ($visitanimal->getAnimal()) {
            $animal = $this->farms->getAnimal($visitanimal->getAnimal(), true);
        } else {
            $animal = new \Greetik\FarmBundle\Entity\Animal();
            $animal->setName($visitanimal->getName());
            $animal->setTattoo($visitanimal->getTattoo());
            $animal->setBirthdate($visitanimal->getBirthdate());
            $this->farms->insertAnimal($animal, null, null);

            $visitanimal->setAnimal($animal->getId());
        }

        if (!$animal)
            throw new \Exception('Tiene que seleccionar un animal');

        $this->getVisitPerm('modify', $visit['itemtype'], $visit['itemid']);

        //Si se le asigna calificación, también registro D
        if ($visitanimal->getPoints()) {
            if (!$animal['points'] || intval($animal['points']) <= 0) {
                $this->farms->setAnimalPoints($visitanimal->getAnimal(), $visitanimal->getPoints());
                if (!$animal['registerd']) {
                    $this->farms->setAnimalRegisterD($visitanimal->getAnimal(), 'RDU '.$this->farms->getMaxRegisterD(), $visit['visitdate']);
                }
            } else
                $warning .= '. El animal ya tiene una calificación';
        }


        if ($visitanimal->getTofair())
            $this->fairs->insertAnimal($visitanimal->getAnimal(), $visitanimal->getTofair());


        if ($visitanimal->getWeight())
            $this->farms->changeSpecialWeight($visitanimal->getAnimal(), $visit['visitdate'], $visitanimal->getWeight(), 'visit-' . $visit['id']);

        if ($visitanimal->getEnddate()) {
            if (!$animal['enddate'])
                $this->farms->setAnimalEnddate($visitanimal->getAnimal(), $visitanimal->getEnddate());
            else
                $warning .= '. El animal ya tiene fecha de baja';
        }



        if ($visitanimal->getRegister()) {
            $found = false;
            $found2 = false;
            //Ver si es de nacimiento
            foreach ($animals = $this->animalsForRegisterC($visit['id']) as $k => $v) {
                if ($v['id'] == $visitanimal->getAnimal()) {
                    $saveanimal = $visitanimal;
                    $found = true;
                    break;
                }
            }
            //ver si es definitivo
            foreach ($animals = $this->animalsForRegisterD($visit['id']) as $k => $v) {

                if ($v['id'] == $visitanimal->getAnimal()) {
                    $found2 = true;
                    break;
                }
            }
            if ((!$found && !$found2)/* || ($found && $found2) */) {
                $warning .= '. No se pudo asignar registro al animal ' . $this->farms->getAnimal($visitanimal->getAnimal())['name'];
                return $warning;
            }
            if ($found /* && !$found2 */) {
                $this->farms->setAnimalRegisterC($visitanimal->getAnimal(), $visitanimal->getRegister(), $visit['visitdate']);
            }
            if ($found2 && !$found) {
                $this->farms->setAnimalRegisterD($visitanimal->getAnimal(), $visitanimal->getRegister(), $visit['visitdate']);
            }
        }
        try {
            $this->visitanimals->insertVisitanimal($visitanimal);
        } catch (\Exception $e) {
            $warning .= $e->getMessage();
        }

        $this->log->insertEntry(LogactionsType::EDIT, $this->stringForLog($visitanimal->getVisit()) . '. Nuevo animal visitado con Id #' . $visitanimal->getAnimal(), $visitanimal->getId(), 'visitanimal');
        return $warning;
    }

    public function modifyAnimal($visitanimal, $oldvisitanimal) {
        $warning = 'Atención';
        $visit = $this->visits->getVisit($visitanimal->getVisit());
        $this->getVisitPerm('modify', $visit['itemtype'], $visit['itemid']);
        $animal = $this->farms->getAnimal($visitanimal->getAnimal());
        if (!$animal)
            throw new \Exception('Tiene que seleccionar un animal');

        if ($visitanimal->getTofair() != $oldvisitanimal['tofair']) {
            $this->fairs->deleteAnimal($visitanimal->getAnimal(), $oldvisitanimal['tofair']);
            $this->fairs->insertAnimal($visitanimal->getAnimal(), $visitanimal->getTofair());
        }

        if ($visitanimal->getWeight() != $oldvisitanimal['weight'])
            $this->farms->changeSpecialWeight($visitanimal->getAnimal(), $visit['visitdate'], $visitanimal->getWeight(), 'visit-' . $visit['id']);

        if ($visitanimal->getEnddate() != $oldvisitanimal['enddate']) {
            if ($animal['enddate'] == $oldvisitanimal['enddate'])
                $this->farms->setAnimalEnddate($visitanimal->getAnimal(), $visitanimal->getEnddate());
            else
                $warning .= 'El animal ya tiene fecha de baja';
        }

        if ($visitanimal->getPoints() != $oldvisitanimal['points']) {
            if ($animal['points'] == $oldvisitanimal['points'] || !$animal['points'] || intval($animal['points']) <= 0) {
                $this->farms->setAnimalPoints($visitanimal->getAnimal(), $visitanimal->getPoints());
                if (!$animal['registerd']) {
                    $this->farms->setAnimalRegisterD($visitanimal->getAnimal(), $this->farms->getMaxRegisterD(), $visit['visitdate']);
                }
            } else
                $warning .= '. El animal ya tiene una calificación';
        }

        if ($visitanimal->getRegister() != $oldvisitanimal['register']) {
            if ($animal['registerc'] == $oldvisitanimal['register']) {
                $this->farms->setAnimalRegisterC($visitanimal->getAnimal(), $visitanimal->getRegister(), $visit['visitdate']);
            } elseif ($animal['registerd'] == $oldvisitanimal['register']) {
                $this->farms->setAnimalRegisterD($visitanimal->getAnimal(), $visitanimal->getRegister(), $visit['visitdate']);
            } else
                $warning .= 'El animal ya tenía asignado un registro';
        }

        try {
            $this->visitanimals->modifyVisitanimal($visitanimal);
        } catch (\Exception $e) {
            $warning .= $e->getMessage();
        }

        $this->log->insertEntry(LogactionsType::EDIT, $this->stringForLog($visitanimal->getVisit()) . '. Modificación de animal visitado con Id #' . $visitanimal->getAnimal(), $visitanimal->getId(), 'visitanimal');
        return $warning;
    }

    public function deleteAnimal($visitanimal) {
        $visit = $this->visits->getVisit($visitanimal->getVisit());
        $this->getVisitPerm('modify', $visit['itemtype'], $visit['itemid']);
        $animal = $this->farms->getAnimal($visitanimal->getAnimal());

        $this->farms->changeSpecialWeight($visitanimal->getAnimal(), $visit['visitdate'], 0, 'visit-' . $visit['id']);

        $this->fairs->deleteAnimal($visitanimal->getAnimal(), $visitanimal->getTofair());

        if ($visitanimal->getEnddate() && $animal['enddate'] == $visitanimal->getEnddate())
            $this->farms->setAnimalEnddate($visitanimal->getAnimal(), null);

        if ($visitanimal->getPoints() && $animal['points'] == $visitanimal->getPoints())
            $this->farms->setAnimalPoints($visitanimal->getAnimal(), 0);

        if ($visitanimal->getRegister() && $animal['registerc'] == $visitanimal->getRegister()) {
            $this->farms->setAnimalRegisterC($visitanimal->getAnimal(), '');
        }

        if ($visitanimal->getRegister() && $animal['registerd'] == $visitanimal->getRegister()) {
            $this->farms->setAnimalRegisterD($visitanimal->getAnimal(), '');
        }

        $stringforlog = $this->stringForLog($visitanimal->getVisit());
        $idanimal = $visitanimal->getAnimal();
        $id = $visitanimal->getId();
        $result = $this->visitanimals->deleteVisitanimal($visitanimal);
        $this->log->insertEntry(LogactionsType::EDIT, $stringforlog . '. Modificación de animal visitado con Id #' . $idanimal, $id, 'visitanimal');
        return $result;
    }

    /* Obtener animales para poner fecha de baja (todos los activos) */

    public function animalsForEndDate($id) {
        $visit = $this->getVisit($id);
        if ($visit['itemtype'] != 'farm')
            return array();

        $animals = $this->farms->getAnimalsForEndDate($visit['itemid'], $visit['visitdate']);
        return $animals;
    }

    /* Obtener animales para pesadas y calificaciones (de 4 a 12 meses) */

    public function animalsForWeight($id) {
        $visit = $this->getVisit($id);
        if ($visit['itemtype'] != 'farm')
            return array();

        $animals = $this->farms->getAnimalsForWeight($visit['itemid'], $visit['visitdate']);
        foreach ($animals as $k => $v) {
            foreach ($visit['animals'] as $va) {
                if ($va['animal'] == $v['id']) {
                    $animals[$k]['weight'] = $va['weight'];

                    if (intval($va['weight']) > 0 && $va['indexanimal'])
                        unset($animals[$k]);
                }
            }
        }

        return $animals;
    }

    /* Obtener animales para asignar registro Definitivo */

    public function animalsForRegisterD($id) {
        $visit = $this->getVisit($id);
        if ($visit['itemtype'] != 'farm')
            return array();

        $animals = $this->farms->animalsForRegisterD($visit['itemid'], $visit['visitdate']);
        foreach ($animals as $k => $v) {
            
        }

        return $animals;
    }

    /* Obtener animales para asignar registro de Nacimiento */

    public function animalsForRegisterC($id) {
        $visit = $this->getVisit($id);
        if ($visit['itemtype'] != 'farm')
            return array();

        $animals = $this->farms->animalsForRegisterC($visit['itemid'], $visit['visitdate']);
        foreach ($animals as $k => $v) {
            
        }

        return $animals;
    }

    /* Exportar a excel */

    public function exportVisits($phpExcelObject, $ids = array()) {
        $numrow = 1;
        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A' . $numrow, '#')
                ->setCellValue('B' . $numrow, 'Fecha')
                ->setCellValue('C' . $numrow, 'Ganadería')
                ->setCellValue('D' . $numrow, 'Observaciones')
                ->setCellValue('E' . $numrow, 'Estado')
                ->setCellValue('F' . $numrow, 'Pesadas')
                ->setCellValue('G' . $numrow, 'Índices')
                ->setCellValue('H' . $numrow, 'RNU')
                ->setCellValue('I' . $numrow, 'RDU')
        ;

        $visits = $this->getVisitsByItemtype('farm');

        $numrow = 2;
        foreach ($visits as $visit) {
            if (!$visit['id'])
                continue;


            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A' . $numrow, $visit['id'])
                    ->setCellValue('B' . $numrow, $visit['visitdate']->format('d/m/Y'))
                    ->setCellValue('C' . $numrow, $visit['farmname'])
                    ->setCellValue('D' . $numrow, $visit['description'])
                    ->setCellValue('E' . $numrow, $visit['state'] ? \Greetik\VisitsBundle\DBAL\Types\VisitstateType::getReadableValue($visit['state']) : '')
                    ->setCellValue('F' . $numrow, $visit['num_weights'])
                    ->setCellValue('G' . $numrow, $visit['num_index'])
                    ->setCellValue('H' . $numrow, $visit['num_registerc'])
                    ->setCellValue('I' . $numrow, $visit['num_registerd'])
            ;

            $numrow++;
        }
    }

}
