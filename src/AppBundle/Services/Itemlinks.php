<?php

namespace AppBundle\Services;



/**
 * Description of Blog Tools
 *
 * @author Pacolmg
 */
class Itemlinks {

    
    private $itemlinks;
    private $blog;
    private $treesections;
    private $events;
    private $catalog;
    
    public function __construct($_itemlinks, $_blog, $_treesections, $_events='', $_catalog='') {
        $this->itemlinks = $_itemlinks;
        $this->blog = $_blog;
        $this->treesections = $_treesections;
        $this->events = $_events;
        $this->catalog = $_catalog;
    }

    protected function getItemlinkPerm($perm, $item_type, $item_id){
        switch($item_type){
            case 'post': if (!$this->blog->getPostPerm($perm, $item_id)) throw new AccessDeniedException('No tienes permiso para operar con el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($perm, $item_id)) throw new AccessDeniedException('No tienes permiso para operar con la sección');
                break;
            case 'event': if (!$this->events->getEventPerm($perm, $item_id)) throw new AccessDeniedException('No tienes permiso para operar con el evento');
                break;
            case 'product': 
                if ($perm=='publicview') return true;
                if (!$this->catalog->getProductPerm($perm, $item_id)) throw new \Exception('No tienes permiso para operar con el producto');
                break;
                
            default: throw new \Exception('No se encuentra el tipo de enlace');
        }
        
        return true;
    }
    
    public function insertItemlink($itemlink) {
        $this->getItemlinkPerm('modify', $itemlink->getItemtype(), $itemlink->getItemid());

        return $this->itemlinks->insertItemlink($itemlink);
    }
    
    public function getItemlinksByItem($item_id, $item_type) {
        $this->getItemlinkPerm('view', $item_type, $item_id);
        return $this->itemlinks->getItemlinksByItem($item_id, $item_type);
    }

    public function modifyItemlink($itemlink) {
        $this->getItemlinkPerm('modify', $itemlink->getItemtype(), $itemlink->getItemid());
        return $this->itemlinks->modifyItemlink($itemlink);
    }
    
    public function deleteItemlink($itemlink) {
        $this->getItemlinkPerm('modify', $itemlink->getItemtype(), $itemlink->getItemid());
        return $this->itemlinks->deleteItemlink($itemlink);
    }
    
    public function putupItemlink($itemlink){
        $this->getItemlinkPerm('modify', $itemlink->getItemtype(), $itemlink->getItemid());
        return $this->itemlinks->putupItemlink($itemlink);
    }
    
    public function putdownItemlink($itemlink){
        $this->getItemlinkPerm('modify', $itemlink->getItemtype(), $itemlink->getItemid());
        return $this->itemlinks->putdownItemlink($itemlink);
    }
    
}
