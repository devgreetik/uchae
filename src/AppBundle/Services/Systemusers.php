<?php

namespace AppBundle\Services;

use Greetik\FarmBundle\Entity\Farm;

/**
 * Description of Farm Tools
 *
 * @author Pacolmg
 */
class Systemusers {

    
    private $systemusers;
    private $farms;
    private $visits;
    private $log;
    
    public function __construct($_systemusers, $_farms, $_visits, $_log) {
        $this->systemusers = $_systemusers;
        $this->farms = $_farms;
        $this->visits = $_visits;
        $this->log = $_log;
    }
    
    //Descripción para log
    public function stringForLog($item, $complete=false, $includecompany=false) {
        $item = ((is_numeric($item))?$this->systemusers->getSystemuserObject($item):$item);
        
        $string = 'Usuario #' . $item->getId() . ' - ' . $item->getUsername();
        
        if ($includecompany){
            $string .= '. ' . $this->companies->stringForLog($user->getCompany());
        }

        
        return $string;
    }

    /*Inserta el usuario junto a su ganadería*/
    public function insertSystemuser($systemuser, $enabled, $farmname, $farmabb, $farmrega, $farmdesc){
        if (!$systemuser->getFarm()){
            $farm = new Farm();
            $farm->setName($farmname);
            $farm->setDescription($farmdesc);
            $farm->setAbb($farmabb);
            $farm->setRega($farmrega);
            $this->farms->insertFarm($farm);
            
            $systemuser->setFarm($farm->getId());
        }
        
        return $this->systemusers->insertSystemuser($systemuser, $enabled);
    }

    /*Obtiene el usuario junto a su ganadería*/
    public function getSystemuser($id){
        $user = $this->systemusers->getSystemUser($id);
        $user['farmdata'] = $this->farms->getFarm($user['farm']);
        $user['farmname'] = (($user['farmdata'])?$user['farmdata']['name']:'-');
        $user['farmdata']['visits'] = $this->visits->getVisitsByItem($user['farm'], 'farm');
        
        return $user;
    }

    /* Obtener los farmers, integrándolo con sus ganaderías */
    public function getFarmers() {
        return $this->systemusers->getLowUsers($this->farms->getFarms(true));
    }

    /*Esporta los farmers a Excel */    
    public function exportFarmers($phpExcelObject, $ids=array()){
            $numrow=1;
            $phpExcelObject->setActiveSheetIndex(0)
               ->setCellValue('A'.$numrow, '#')
               ->setCellValue('B'.$numrow, 'Usuario')
               ->setCellValue('C'.$numrow, 'Email')
               ->setCellValue('D'.$numrow, 'Nombre')
               ->setCellValue('E'.$numrow, 'Apellidos')
               ->setCellValue('F'.$numrow, 'Teléfono')
               ->setCellValue('G'.$numrow, 'Dirección')
               ->setCellValue('H'.$numrow, 'Nacimiento')
               ->setCellValue('K'.$numrow, 'Ganadería');
        
        $numrow=2;
        foreach($ids as $id){
            if (!$id) continue;
            
            $user = $this->getSystemuser($id);
            
            $phpExcelObject->setActiveSheetIndex(0)
               ->setCellValue('A'.$numrow, $user['id'])
               ->setCellValue('B'.$numrow, $user['username'])
               ->setCellValue('C'.$numrow, $user['email'])
               ->setCellValue('D'.$numrow, $user['name'])
               ->setCellValue('E'.$numrow, $user['surname'])
               ->setCellValue('F'.$numrow, $user['phone'])
               ->setCellValue('G'.$numrow, $user['address'])
               ->setCellValue('H'.$numrow, $user['birthdate']->format('d/m/Y'))
               ->setCellValue('K'.$numrow, $user['farmname'])
                    ;
            
            $numrow++;
        }    

    }
    
    
}
