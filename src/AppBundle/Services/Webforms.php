<?php

namespace AppBundle\Services;

/**
 * Description of Blog Tools
 *
 * @author Pacolmg
 */
class Webforms {

    private $webforms;
    private $systemusers;

    public function __construct($_webforms, $_systemusers) {
        $this->webforms = $_webforms;
        $this->systemusers = $_systemusers;
    }

    /* Comprueba si el usuario tiene permiso para operar sobre el campo */

    public function getFormfieldPerm($type, $formfield = '', $project = '') {
        if ($type == 'publicview')
            return true;
        return $this->systemusers->isGrantedConnected('ROLE_ADMIN');
    }

    /* obtener el listado de campos */

    public function getFormfieldsByProject($project = '') {
        if (!$this->getFormfieldPerm('list'))
            throw new \Exception('No tienes permiso para listar los campos');
        return $this->getPublicFormfieldsByProject($project);
    }

    /* obtener un formfield */

    public function getFormfield($id) {
        if (!$this->getFormfieldPerm('view'))
            throw new \Exception('No tienes permiso para ver el campo');
        return $this->getPublicFormfield($id);
    }

    /* eliminar un formfield */

    public function deleteFormfield($formfield) {
        if (!$this->getFormfieldPerm('delete'))
            throw new \Exception('No tienes permiso para eliminar el campo');

        return $this->webforms->deleteFormfield($formfield);
    }

    /* insertar un formfield */

    public function insertFormfield($formfield, $project = '', $user = '') {
        if (!$this->getFormfieldPerm('delete'))
            throw new \Exception('No tienes permiso para insertar el campo');
        return $this->webforms->insertFormfield($formfield, $project, $this->systemusers->getUserConnected()->getId());
    }

    /* modificar un formfield */

    public function modifyFormfield($formfield, $oldtags) {
        if (!$this->getFormfieldPerm('modify'))
            throw new \Exception('No tienes permiso para modificar el campo');
        return $this->webforms->modifyFormfield($formfield, $oldtags);
    }

    /* obtener un formfield */

    public function getPublicFormfield($id) {
        $formfield = $this->webforms->getFormfield($id);

        return $formfield;
    }

    /* obtener sin permisos un formulario */

    public function getPublicFormfieldsByProject($project) {
        return $this->webforms->getFormfieldsByProject($project);
    }

    /* guardar configuración */

    public function saveConfig($formconfig) {
        if (!$this->getFormfieldPerm('config', '', $formconfig->getProject()))
            throw new \Exception('No tienes permiso para cambiar la configuración');
        return $this->webforms->saveConfig($formconfig);
    }

    /* opciones */
    public function modifyFormfieldoption($formfieldoption) {
        if (!$this->getFormfieldPerm('modify', $formfieldoption->getFormfield()))
            throw new \Exception('No tienes permiso para modificar el campo');
        
        return $this->webforms->modifyFormfieldoption($formfieldoption);
    }

    public function insertFormfieldoption($formfieldoption) {
        if (!$this->getFormfieldPerm('modify', $formfieldoption->getFormfield()))
            throw new \Exception('No tienes permiso para modificar el campo');
        
        return $this->webforms->insertFormfieldoption($formfieldoption);
    }

    public function deleteFormfieldoption($formfieldoption) {
        if (!$this->getFormfieldPerm('modify', $formfieldoption->getFormfield()))
            throw new \Exception('No tienes permiso para modificar el campo');
        
        return $this->webforms->deleteFormfieldoption($formfieldoption);
    }
    
}
