<?php

namespace Greetik\FarmBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('farm');

        $rootNode
            ->children()
                ->scalarNode('permsservice')->defaultValue('farm.tools')->end()
                ->scalarNode('interface')->defaultValue('FarmBundle:Farm')->end()
                ->scalarNode('interface_animal')->defaultValue('FarmBundle:Animal')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
