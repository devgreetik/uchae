<?php

namespace Greetik\FarmBundle\Services;

/**
 * Description of Farm Tools
 *
 * @author Pacolmg
 */
class Tools {

    private $em;
    private $farms;

    public function __construct($_entityManager, $_farms) {
        $this->em = $_entityManager;
        $this->em = $_farms;
    }

    public function isTwin($animal) {
        if ($animal['mother']) {
            $twins = $this->em->getRepository('FarmBundle:Animal')->findBy(array('mother' => $animal['mother'], 'birthdate' => $animal['birthdate']));

            if (count($twins) > 1)
                return true;
        }
        return false;
    }

}
