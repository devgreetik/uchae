<?php

namespace Greetik\FarmBundle\Services;

use AppBundle\DBAL\Types\ProvinceType;
use Greetik\FarmBundle\DBAL\Types\TypefarmType;
use Greetik\FarmBundle\Entity\Farm;
use Greetik\FarmBundle\Entity\Animal;
use Greetik\FarmBundle\DBAL\Types\AnimalgenderType;
use Greetik\FarmBundle\DBAL\Types\AnimalstateType;

/**
 * Description of Farm Tools
 *
 * @author Pacolmg
 */
class Farms
{

    private $__context;
    private $em;
    private $beinterfacetools;
    private $excel;
    //variables para cachear info
    private $tattoosinserted = array();
    private $crotalsinserted = array();

    public function __construct($_entityManager, $_context, $_beinterfacetools, $_excel)
    {
        $this->em = $_entityManager;
        $this->__context = $_context;
        $this->beinterfacetools = $_beinterfacetools;
        $this->excel = $_excel;
    }

    /* Obtener ganadería en formato objeto */

    public function getFarmObject($id)
    {
        return $this->em->getRepository('FarmBundle:Farm')->findOneById($id);
    }

    public function getFarmObjectByRega($rega)
    {
        if (!$rega)
            return '';
        return $this->em->getRepository('FarmBundle:Farm')->findOneByRega($rega);
    }

    /* obtener el listado de ganaderías */

    public function getFarms($forselect = false, $province = '')
    {
        $data = $this->em->getRepository('FarmBundle:Farm')->getFarms($province);
        $dataforselect = array();
        if ($forselect) {
            foreach ($data as $k => $v) {
                $dataforselect[$v['id']] = $v['name'];
            }
        } else {
            foreach ($data as $k => $v) {
                $data[$k]['province_name'] = (($v['province']) ? ProvinceType::getReadableValue($v['province']) : '');
            }
            return $data;
        }

        return $dataforselect;
    }

    /* Obtiene para selector */

    public function getFarmsSelect($search)
    {
        $data = $this->em->getRepository('FarmBundle:Farm')->getFarmsBySearch($search);

        foreach ($data as $k => $v) {
            $data[$k] = array('id' => $v['id'], 'text' => $v['name'] . ' (' . $v['abb'] . ')');
        }

        return $data;
    }

    /* Obtener ganaderías por página */

    public function getFarmsPaged($page = 1, $numperpage = 20, $province = '')
    {
        return $this->em->getRepository('FarmBundle:Farm')->getFarmsPaged($page, $numperpage, $province);
    }

    /* Obtiene el número de ganaderías */

    public function getNumberOfFarms($province = '')
    {
        return $this->em->getRepository('FarmBundle:Farm')->getNumberOfFarms($province);
    }

    /* obtener una ganadería */

    public function getFarm($id)
    {
        $farm = $this->em->getRepository('FarmBundle:Farm')->getFarm($id);

        return $farm;
    }

    /* eliminar una ganadería */

    public function deleteFarm($farm)
    {
        if (is_numeric($farm))
            $farm = $this->getFarmObject($farm);

        $this->beinterfacetools->deleteAvatar($farm, 'uploads/farm/');

        $this->em->remove($farm);
        $this->em->flush();
    }

    /* Comprueba que los datos son correctos */

    public function checkdataFarm($farm)
    {
        if (!$farm->getName() || $farm->getName() == '')
            throw new \Exception('El nombre de la ganadería es obligatorio');
        if ($this->beinterfacetools->countwords($farm->getDescription()) > 200)
            throw new \Exception('La descripción no puede ser mayor a 200 palabras');
        return true;
    }

    /* insertar una ganadería */

    public function insertFarm($farm)
    {
        $this->checkdataFarm($farm);
        $farm->setEnabled(true);

        $this->em->persist($farm);
        $this->em->flush();
    }

    /* modificar una ganadería */

    public function modifyFarm($farm)
    {
        $this->checkdataFarm($farm);
        $this->em->flush();
    }

    /* Obtener el nombre de una ganadería */

    public function getFarmname($id)
    {
        $farm = $this->em->getRepository('FarmBundle:Farm')->getFarm($id);
        if ($farm)
            return $farm['name'];

        return '';
    }

    /* Cambia el avatar de una ganadería */

    public function changeAvatar($file, $farm)
    {
        if (is_numeric($farm))
            $farm = $this->getFarmObject($farm);

        $this->beinterfacetools->changeAvatar($file, $farm, 'uploads/farm/');
    }

    /* Obtener animales para fechas de baaja (activos) */

    public function getAnimalsForEndDate($farm)
    {
        $animals = $this->getAnimalsTableFiltered('', 'a.birthdate', 'DESC', 0, 9999, $farm);
        foreach ($animals as $k => $v) {
            $animals[$k]['gendername'] = $v['gender'] ? AnimalgenderType::getReadableValue($v['gender']) : '-';
        }

        return $animals;
    }

    /* Obtener animales para pesadas (de 4 a 12 meses) */

    public function getAnimalsForWeight($farm, $date = '')
    {
        if (empty($date))
            $date = new \Datetime();

        //En qué fecha hay que haber nacido para tener entre 4 y 12 meses para la fecha $date
        $birthfrom = new \Datetime($date->format('Y-m-d'));
        $birthto = new \Datetime($date->format('Y-m-d'));
        $birthfrom->modify('-12 months');
        $birthto->modify('-4 months');
        $animals = $this->getAnimalsTableFiltered('', 'a.birthdate', 'DESC', 0, 9999, $farm, '', '', '', $birthfrom, $birthto);
        foreach ($animals as $k => $v) {
            $animals[$k]['gendername'] = $v['gender'] ? AnimalgenderType::getReadableValue($v['gender']):'-';
        }

        return $animals;
    }

    /* Obtener animales para asignar registro D (machos > 14 meses, hembras > 18 meses) */

    public function animalsForRegisterD($farm, $date = '')
    {
        if (empty($date))
            $date = new \Datetime();

        //MACHOS
        $birthfrom = new \Datetime($date->format('Y-m-d'));
        $birthto = new \Datetime($date->format('Y-m-d'));
        $birthfrom->modify('-100 years');
        $birthto->modify('-14 months');

        $animals = $this->getAnimalsTableFiltered('', 'a.birthdate', 'DESC', 0, 9999, $farm, '', '', '', $birthfrom, $birthto, AnimalgenderType::MACHO);


        //HEMBRAS
        $birthto = new \Datetime($date->format('Y-m-d'));
        $birthto->modify('-18 months');

        $animals = array_merge($animals, $this->getAnimalsTableFiltered('', 'a.birthdate', 'DESC', 0, 9999, $farm, '', '', '', $birthfrom, $birthto, AnimalgenderType::HEMBRA));


        //quitar los que no tengan rnu
        foreach ($animals as $k => $v) {
            if (!$v['registerc']) {
                unset($animals[$k]);
                continue;
            }
            if ($v['registerd']) {
                unset($animals[$k]);
                continue;
            }

            $animals[$k]['gendername'] = $v['gender'] ? AnimalgenderType::getReadableValue($v['gender']) : '-';
        }

        return $animals;
    }

    /* Obtener animales para asignar registro de nacimiento (animales sin RNU) */

    public function animalsForRegisterC($farm, $date = '')
    {
        $animals = $this->getAnimalsTableFiltered('', 'a.birthdate', 'DESC', 0, 9999, $farm);
        foreach ($animals as $k => $v) {
            if ($v['registerc'] != '' || $v['registerd'] != '') {
                unset($animals[$k]);
                continue;
            }
            $animals[$k]['gendername'] = $v['gender'] ? AnimalgenderType::getReadableValue($v['gender']) : '-';
        }

        return $animals;
    }

    /* Obtiene los animales para tabla ajax por filtros */

    public function getAnimalsTableFiltered($search, $sortcol, $sortdir, $start, $lenght, $farm = '', $farmbirth = '', $father = '', $mother = '', $birthfrom = '', $birthto = '', $sex = '', $forsale = '', $onlyActive = true, $prov = '', $allparams = false)
    {
        return $this->em->getRepository('FarmBundle:Animal')->getAnimalsTableFiltered($search, $sortcol, $sortdir, $start, $lenght, $farm, $farmbirth, $father, $mother, $birthfrom, $birthto, $sex, $forsale, $onlyActive, $prov, $allparams);
    }

    /* Obtiene el número de animales para tabla ajax  por filtros */

    public function getAnimalsTableNumberFiltered($search, $farm = '', $farmbirth = '', $father = '', $mother = '', $birthfrom = '', $birthto = '', $sex = '', $forsale = '', $onlyActive = true, $prov = '')
    {
        return $this->em->getRepository('FarmBundle:Animal')->getAnimalsTableNumberFiltered($search, $farm, $farmbirth, $father, $mother, $birthfrom, $birthto, $sex, $forsale, $onlyActive, $prov);
    }

    /* Obtiene los animales para tabla ajax */

    public function getAnimalsTable($search, $sortcol, $sortdir, $start, $lenght, $farm = '', $parent = '', $onlyActive = true, $prov = '')
    {
        return $this->em->getRepository('FarmBundle:Animal')->getAnimalsTable($search, $sortcol, $sortdir, $start, $lenght, $farm, $parent, $onlyActive, $prov);
    }

    /* Obtiene el número de animales para tabla ajax */

    public function getAnimalsTableNumber($search, $farm = '', $parent = '', $onlyActive = true, $prov = '')
    {
        return $this->em->getRepository('FarmBundle:Animal')->getAnimalsTableNumber($search, $farm, $parent, $onlyActive, $prov);
    }

    /* Obtiene para selector */

    public function getAnimalsSelect($search, $gender = '', $farm = '', $onlyactive = '')
    {
        if (!empty($gender))
            $data = $this->em->getRepository('FarmBundle:Animal')->getAnimalsByGender($search, $gender, $farm, $onlyactive);
        else
            $data = $this->em->getRepository('FarmBundle:Animal')->getAnimalsBySearch($search, $farm, $onlyactive);

        foreach ($data as $k => $v) {
            $data[$k] = array('id' => $v['id'], 'text' => $v['name'] . ' (' . $v['tattoo'] . ')');
        }

        return $data;
    }

    /* Obtiene animales para la venta */

    public function getAnimalsForSale($page = '', $numperpage = 20)
    {
        return $this->em->getRepository('FarmBundle:Animal')->getAnimalsByForSale($page, $numperpage);
    }

    /* Obtiene un animal en formato objeto */

    public function getAnimalObject($id)
    {
        return $this->em->getRepository('FarmBundle:Animal')->findOneById($id);
    }

    /* Obtiene un animal en formato objeto */

    public function getAnimalObjectByCrotal($crotal)
    {
        if (!$crotal)
            return '';
        return $this->em->getRepository('FarmBundle:Animal')->findOneByCrotal($crotal);
    }

    /* Obtiene un animal en formato objeto */

    public function getAnimalObjectByTattoo($tattoo)
    {
        if (!$tattoo)
            return '';
        return $this->em->getRepository('FarmBundle:Animal')->findOneByTattoo($tattoo);
    }

    /* Obtiene los animales que comparten tatuaje en formato friendly, devuelve objetos */

    public function getAnimalObjecstByFriendlytattoo($tattoo)
    {
        if (!$tattoo)
            return array();
        return $this->em->getRepository('FarmBundle:Animal')->findByFriendlytattoo($tattoo);
    }

    /* Obtiene el índice DM */

    public function getIndexDM($animal)
    {
        if (isset($animal['index_ac']))
            return round(($animal['index_ac'] + $animal['index_ad'] + $animal['index_rn'] + $animal['index_an'] + $animal['index_epl'] + $animal['index_epl']) * 100 / 60);
        return 0;
    }

    /* Obtiene el índice DS  */

    public function getIndexDS($animal)
    {
        if (isset($animal['index_ac']))
            return round(($animal['index_ld'] + $animal['index_lp'] + $animal['index_aa'] + $animal['index_tmn'] + $animal['index_tmn']) * 2);
        return 0;
    }

    /* Obtiene el índice AF  */

    public function getIndexAF($animal)
    {
        if (isset($animal['index_ac']))
            return round(($animal['index_m'] + $animal['index_aaa'] + $animal['index_aap'] + $animal['index_rd']) * 100 / 40);
        return 0;
    }

    /* Obtiene la aptitud cárnica de un animal */

    protected function getMeatAptitude($animal)
    {
        if (isset($animal['easebirth']) && isset($animal['weaningval']) && isset($animal['muscledev']) && isset($animal['bonedev'])) {
            return 0.1 * $animal['easebirth'] + 0.4 * $animal['weaningval'] + 0.3 * $animal['muscledev'] + 0.2 * $animal['bonedev'];
        }
        return 0;
    }

    /* Obtiene un animal */

    public function getAnimal($id, $noparents = false, $nofarm = false, $level = 0)
    {
        if (!$id)
            return '';
        $animal = $this->em->getRepository('FarmBundle:Animal')->getAnimal($id);
        if (!$animal) return '';

        $animal['meatap'] = $this->getMeatAptitude($animal);
        $animal['index_dm'] = $this->getIndexDM($animal);
        $animal['index_ds'] = $this->getIndexDS($animal);
        $animal['index_af'] = $this->getIndexAF($animal);


        if (!$nofarm) {
            $animal['farmbirthdata'] = $this->getFarm($animal['farmbirth']);
            $animal['farmdata'] = $this->getFarm($animal['farm']);
        }

        if (!$noparents) {
            $animal['fatherdata'] = $this->getAnimal($animal['father'], $level < 4 ? false : true, $level < 2 ? false : true, $level + 1);
            $animal['motherdata'] = $this->getAnimal($animal['mother'], $level < 4 ? false : true, $level < 2 ? false : true, $level + 1);
        }

        return $animal;
    }

    /* Le quita a un tatuaje "-" y "/" */

    protected function getFriendlytatoo($tattoo)
    {
        if (!$tattoo)
            return $tattoo;

        return str_replace(array('-', '/', '_', ' '), '', $tattoo);
    }

    /* valida un aimal */

    protected function validAnimal($animal, $mother = '', $father = '')
    {
        if (!empty($father)) {
            $fatherobject = $this->getAnimalObject($father);
            if ($fatherobject)
                $animal->setFather($fatherobject);
        }
        if (!empty($mother)) {
            $motherobject = $this->getAnimalObject($mother);
            if ($motherobject)
                $animal->setMother($motherobject);
        }
        $animal->setFriendlytattoo($this->getFriendlytatoo($animal->getTattoo()));

        if ($animal->getEnddate() && $animal->getForsale())
            throw new \Exception('No puede poner a la venta un animal que está dado de baja');
        if ($animal->getForsale() && (!$animal->getPrice() || $animal->getPrice() < 0))
            throw new \Exception('Asigne un precio al animal');

        //if ($animal->getWeaningweight() && $animal->getWeaningdate() < $animal->getBirthdate() && !$animal->getEnddate()) throw new \Exception('La fecha de destete no puede ser anterior al nacimiento');
        //if ($animal->getEnddate() && $animal->getWeaningdate() && $animal->getWeaningdate() > $animal->getEnddate()) throw new \Exception('La fecha de destete no puede ser posterior a la baja');
        //if ($animal->getWeaningweight() && !$animal->getWeaningdate() && !$animal->getEnddate()) throw new \Exception('Para poner peso al destete debes poner fecha de destete');
        //if ($animal->getBirthweight() && !$animal->getBirthdate()) throw new \Exception('Para poner peso al destete debes poner fecha de destete');


        /* Le ponemos el estado */
        if (!$animal->getEnddate()) {
            if ($animal->getFarm())
                $animal->setState(AnimalstateType::ACTIVE);
            else
                $animal->setState(AnimalstateType::REF);
        } else {
            $animal->setState(AnimalstateType::INNACTIVE);
        }

        $this->registerToNum($animal);

        return true;
    }

    protected function registerToNum($animal)
    {
        if (!$animal->getFarmbirth()) return;

        $registerc_num = 0;
        if ($animal->getRegisterc()) {
            if (is_numeric($animal->getRegisterc()))
                $animal->setRegisterc('RNU ' . intval($animal->getRegisterc()));
            $aux = explode(' ', $animal->getRegisterc());
            $registerc_num = isset($aux[1]) && $aux[0] == 'RNU' ? intval($aux[1]) : intval($aux[0]);
        }
        $animal->setRegistercNum($registerc_num);

        $registerd_num = 0;
        if ($animal->getRegisterd()) {
            if (is_numeric($animal->getRegisterd()))
                $animal->setRegisterd('RDU ' . intval($animal->getRegisterd()));
            $aux = explode(' ', $animal->getRegisterd());
            $registerd_num = isset($aux[1]) && $aux[0] == 'RDU' ? intval($aux[1]) : intval($aux[0]);
        }
        $animal->setRegisterdNum($registerd_num);

        if (!$animal->getIndexdate() && $animal->getIndexAc()) {
            $animal->setIndexdate(new \Datetime());
        }

        if (!$animal->getRnudate() && $animal->getRegisterc()) {
            $animal->setRnudate(new \Datetime());
        } else {
            if (!$animal->getRegisterc()) $animal->setRnudate(null);
        }

        if (!$animal->getRdudate() && $animal->getRegisterd()) {
            $animal->setRdudate(new \Datetime());
        } else {
            if (!$animal->getRegisterd()) $animal->setRdudate(null);
        }

    }

    /* modifica un animal */

    public function modifyAnimal($animal, $mother, $father)
    {
        $this->validAnimal($animal, $mother, $father);

        try {
            $this->em->flush();
        } catch (\Exception $e) {
            if (strpos($e->getMessage(), 'UNIQ_6AAB231FDEE4C6F') !== false)
                throw new \Exception('El tatuaje ya existe en la base de datos');
            else if (strpos($e->getMessage(), 'UNIQ_6AAB231FB4EF3FB4') !== false)
                throw new \Exception('El crotal ya existe en la base de datos');
            else
                throw $e;
        }
        return true;
    }

    /* modifica un animal */

    public function modifyAnimalPartial($animal, $force = false)
    {
        if (!$force) {
            $this->validAnimal($animal);
        } else {
            $this->registerToNum($animal);
        }
        try {
            $this->em->flush();
        } catch (\Exception $e) {
            if (strpos($e->getMessage(), 'UNIQ_6AAB231FDEE4C6F') !== false)
                throw new \Exception('El tatuaje ya existe en la base de datos');
            else if (strpos($e->getMessage(), 'UNIQ_6AAB231FB4EF3FB4') !== false)
                throw new \Exception('El crotal ya existe en la base de datos');
            else
                throw $e;
        }
        return true;
    }

    /* Inserta un animal */

    public function insertAnimal($animal, $mother, $father)
    {
        $this->validAnimal($animal, $mother, $father);
        try {
            $this->em->persist($animal);
            $this->em->flush();
        } catch (\Exception $e) {
            if (strpos($e->getMessage(), 'UNIQ_6AAB231FDEE4C6F') !== false)
                throw new \Exception('El tatuaje ya existe en la base de datos');
            else if (strpos($e->getMessage(), 'UNIQ_6AAB231FB4EF3FB4') !== false)
                throw new \Exception('El crotal ya existe en la base de datos');
            else
                throw $e;
        }
        return true;
    }

    /* Obtener el máximo número de registro en C */

    public function getMaxRegisterC()
    {
        $startfrom = 11385;
        $max = $this->em->getRepository('FarmBundle:Animal')->getMaxRegisterC();
        if ($max < $startfrom)
            return $startfrom;
        return $max;
    }

    /* Obtener el máximo número de registro en D */

    public function getMaxRegisterD()
    {
        $startfrom = 8756;
        $max = $this->em->getRepository('FarmBundle:Animal')->getMaxRegisterD();
        if ($max < $startfrom)
            return $startfrom;
        return $max;
    }

    /* Obtener puntos en formato texto */

    public function getPointstext($points, $gender)
    {
        if ($gender == AnimalgenderType::MACHO) {
            if ($points < 70)
                return 'I';
            if ($points < 75)
                return 'S';
            if ($points < 80)
                return 'B';
            if ($points < 85)
                return 'MB';
            if ($points < 90)
                return 'MBS';
            return 'EXC';
        } else {
            if ($points < 65)
                return 'I';
            if ($points < 70)
                return 'S';
            if ($points < 75)
                return 'B';
            if ($points < 81)
                return 'MB';
            if ($points < 87)
                return 'MBS';
            return 'EXC';
        }

        return 'I';
    }

    /* Importar ganaderías */

    public function importFarms($filename)
    {
        ini_set('max_execution_time', 300);
        $fields = array('0' => 'rega', '1' => 'name', '2' => 'abb', '3' => 'address', '4' => 'zip', '5' => 'town', '6' => 'province', '8' => 'contact', '9' => 'contact2', '10' => 'phone', '11' => 'mobile', '12' => 'fax', '13' => 'phone2', '14' => 'namelabel', '15' => 'owner', '16' => 'email', '17' => 'regaabb', '18' => 'fieldname', '19' => 'fieldtown');

        $data = array('numimported' => 0, 'warnings' => '');

        $inputFileType = \PHPExcel_IOFactory::identify($filename);

        $reader = $this->excel->createReader($inputFileType);
        $phpExcelObject = $reader->load($filename);

        // read some data
        $objWorksheet = $phpExcelObject->getActiveSheet();

        $i = 1;
        foreach ($objWorksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,

            if ($i > 1) {
                $j = 0;
                $farm = $this->getFarmObjectByRega($objWorksheet->getCell('A' . $i));
                if (!$farm) {
                    $inserting = true;
                    $farm = new Farm();
                } else
                    $inserting = false;

                foreach ($cellIterator as $cell) {
                    if (isset($fields[$j])) {
                        $namefunction = 'set' . ucfirst($fields[$j]);
                        $farm->$namefunction($cell->getValue());
                    }
                    $j++;
                }

                if ($inserting)
                    $this->em->persist($farm);
            }
            $data['numimported']++;
            $i++;
        }
        $this->em->flush();
        return $data;
    }

    /* check if the tattoo or the crotal has been imported before */

    protected function importAnimalCheckTattoo($tattoo, $crotal)
    {
        if (isset($tattoo) && $tattoo != '') {
            if (in_array($tattoo, $this->crotalsinserted)) {
                return false;
            }
        }
        if (isset($crotal) && $crotal != '') {
            if (in_array($crotal, $this->tattoosinserted)) {
                return false;
            }
        }
        if (isset($tattoo) && $tattoo != '')
            $this->crotalsinserted[] = $tattoo;
        if (isset($crotal) && $crotal != '')
            $this->tattoosinserted[] = $tattoo;

        return true;
    }

    /* import animals from excell */

    protected function importAnimalExcel($filename, $fields, $inputFileType, $data)
    {
        $reader = $this->excel->createReader($inputFileType);
        $phpExcelObject = $reader->load($filename);

        // read some data
        $objWorksheet = $phpExcelObject->getActiveSheet();

        $i = 1;
        foreach ($objWorksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,

            if ($i > 1) {
                $j = 0;

                if ((!$objWorksheet->getCell('A' . intval($i)) || $objWorksheet->getCell('A' . intval($i)) == '') && (!$objWorksheet->getCell('C' . intval($i)) || $objWorksheet->getCell('C' . intval($i)))) {
                    $i++;
                    continue;
                }
                if (!$this->importAnimalCheckTattoo((($objWorksheet->getCell('A' . intval($i))) ? $objWorksheet->getCell('A' . intval($i)) : ''), (($objWorksheet->getCell('C' . intval($i))) ? $objWorksheet->getCell('C' . intval($i)) : ''))) {
                    $i++;
                    continue;
                }

                $animal = $this->getAnimalObjectByTattoo($objWorksheet->getCell('A' . intval($i)));
                if (!$animal) {
                    $inserting = true;
                    $animal = new Animal();
                } else {
                    $inserting = false;
                }

                foreach ($cellIterator as $cell) {
                    $this->importAnimalCell($cell->getValue(), $animal, $fields, $j, $inserting);
                    $j++;
                }

                $animal->setFriendlytattoo($this->getFriendlytatoo($animal->getTattoo()));
                if ($inserting) {
                    $this->em->persist($animal);
                }

                if ($i % 25 == 0) {
                    $this->em->flush();
                    $this->em->clear();
                }
            }
            $data['numimported']++;
            $i++;

            //free space
            $objWorksheet->removeRow($i, 1);
        }
        $this->em->flush();
        return $data;
    }

    /* import animals from csv */

    protected function importAnimalsCSV($filename, $fields, $data)
    {
        $content = file_get_contents($filename);
        $i = 0;
        $counter = 0;
        $rows = explode("\n", $content);

        //freespace
        $content = null;
        unset($content);

        foreach ($rows as $k => $row) {
            if ($i > 0) {
                $cells = explode(";", $row);
                $j = 0;

                if ((!isset($cells[0]) || $cells[0] == '') && (!isset($cells[2]) || $cells[2])) {
                    $i++;
                    continue;
                }
                if (!$this->importAnimalCheckTattoo(((isset($cells[0])) ? $cells[0] : ''), ((isset($cells[2])) ? $cells[2] : ''))) {
                    $i++;
                    continue;
                }

                $animal = $this->getAnimalObjectByTattoo($cells[0]);
                if (!$animal) {
                    $animal = new Animal();
                    $inserting = true;
                } else {
                    $inserting = false;
                }

                $maxcells = count($cells);
                while ($j < $maxcells) {
                    $this->importAnimalCell($cells[$j], $animal, $fields, $j, $inserting);
                    $j++;
                }

                $animal->setFriendlytattoo($this->getFriendlytatoo($animal->getTattoo()));
                if ($inserting) {
                    $this->em->persist($animal);
                }

                if ($i % 25 == 0) {
                    //$this->tattoosinserted = array();
                    $this->crotalsinserted = array();
                    $this->em->flush();
                    $this->em->clear();
                }
            }
            $data['numimported']++;
            $i++;

            //free space
            $rows[$k] = null;
            unset($rows[$k]);
        }
        $this->em->flush();
        return $data;
    }

    /* Importar celda de un animal */

    protected function importAnimalCell($value, $animal, $fields, $j, $inserting)
    {
        $noinsert = false;

        if (isset($fields[$j])) {

            switch ($fields[$j]) {
                case 'tattoo':
                    if (!$inserting)
                        $noinsert = true;
                    else
                        if ($value == '')
                            $value = null;
                    break;
                case 'crotal':
                    if (!$inserting)
                        $noinsert = true;
                    else {
                        if ($value == '')
                            $value = null;
                        else {
                            $animal2 = $this->getAnimalObjectByCrotal($value);
                            if ($animal2) {
                                if ($animal->getTattoo() != $animal2->getTattoo()) {
                                    $value = null;
                                }
                            }
                        }
                    }
                    break;
                case 'father':
                    $value = $this->getAnimalObjectByCrotal($value);
                    if (!$value)
                        $noinsert = true;
                    break;
                case 'mother':
                    $value = $this->getAnimalObjectByCrotal($value);
                    if (!$value)
                        $noinsert = true;
                    break;
                case 'farm':
                    $value = $this->getFarmObjectByRega($value);
                    if (!$value)
                        $noinsert = true;
                    break;
                case 'farmbirth':
                    $value = $this->getFarmObjectByRega($value);
                    if (!$value)
                        $noinsert = true;
                    break;
                case 'birthdate':
                    try {
                        $value = $this->beinterfacetools->dateFromPickDate($value);
                        if ($value == new \Datetime())
                            $value = null;
                    } catch (\Exception $e) {
                        $value = null;
                        //$noinsert = true;
                    }
                    break;
                case 'enddate':
                    try {
                        $value = $this->beinterfacetools->dateFromPickDate($value);
                        if ($value == new \Datetime())
                            $value = null;
                    } catch (\Exception $e) {
                        $value = null;
                        //$noinsert = true;
                    }
                    break;
                case 'gender':
                    $value = (($value == 'M') ? AnimalgenderType::MACHO : AnimalgenderType::HEMBRA);
                    break;
            }

            if (!$noinsert) {
                $namefunction = 'set' . ucfirst($fields[$j]);
                $animal->$namefunction($value);
            }
        }
    }

    /* Importar animales */

    public function importAnimals($filename)
    {
        ini_set('max_execution_time', 400);

        $fields = array('0' => 'tattoo', '1' => 'name', '2' => 'crotal', '3' => 'registerc', '4' => 'registerd', '6' => 'father', '7' => 'mother', '9' => 'enddate', '10' => 'farm', '11' => 'gender', '12' => 'birthdate', '13' => 'birthweight', '15' => 'register', '16' => 'registerid', '17' => 'country', '18' => 'farmbirth', '20' => 'points');

        $data = array('numimported' => 0, 'warnings' => '');

        $inputFileType = \PHPExcel_IOFactory::identify($filename);

        if ($inputFileType == 'CSV')
            return $this->importAnimalsCSV($filename, $fields, $data);
        return $this->importAnimalExcel($filename, $fields, $inputFileType, $data);
    }

    /* Importar méritos de animales */

    public function importAnimalsindex($filename)
    {
        ini_set('max_execution_time', 300);

        $fields = array('2' => 'birthweight', '3' => 'weanweight', '4' => 'easebirth', '5' => 'weaningval', '6' => 'muscledev', '7' => 'bonedev');

        $data = array('numimported' => 0, 'warnings' => '');

        $inputFileType = \PHPExcel_IOFactory::identify($filename);

        $reader = $this->excel->createReader($inputFileType);
        $phpExcelObject = $reader->load($filename);

        // read some data
        $objWorksheet = $phpExcelObject->getActiveSheet();

        $i = 1;
        foreach ($objWorksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,

            if ($i > 2) {

                if ((!$objWorksheet->getCell('B' . intval($i)) || $objWorksheet->getCell('B' . intval($i)) == '')) {
                    $i++;
                    continue;
                }

                $animals = $this->getAnimalObjecstByFriendlytattoo($objWorksheet->getCell('B' . intval($i)));

                foreach ($animals as $animal) {
                    $j = 0;
                    foreach ($cellIterator as $cell) {
                        if (isset($fields[$j])) {
                            $namefunction = 'set' . ucfirst($fields[$j]);
                            $animal->$namefunction($cell->getValue());
                        }
                        $j++;
                    }
                    $this->em->persist($animal);
                }

                if ($i % 25 == 0) {
                    $this->em->flush();
                    $this->em->clear();
                }
            }
            $data['numimported']++;
            $i++;
        }
        $this->em->flush();
        return $data;
    }

}
