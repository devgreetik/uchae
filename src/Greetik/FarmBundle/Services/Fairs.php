<?php

namespace Greetik\FarmBundle\Services;

use Greetik\FarmBundle\Entity\Fair;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Greetik\FarmBundle\Entity\Fairanimal;
use Greetik\FarmBundle\DBAL\Types\AnimalgenderType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Fairs {

    private $em;
    private $colorcalendar = 'red';
    private $colorcalendarback = '#d1d1d1';
    private $beinterface;

    public function __construct($_entityManager, $_beinterface) {
        $this->em = $_entityManager;
        $this->beinterface = $_beinterface;
    }

    /* pone un titulo, una url de edición, un color... lo que necesita el plugin del calendario */

    protected function renderforcalendarplugin($fairs, $background = false) {
        $data = array();
        foreach ($fairs as $fair) {
            array_push($data, array(
                'id' => $fair['id'],
                'type' => (($background) ? 'background' : 'fair'),
                'title' => $fair['name'],
                'start' => $fair['fairdate']->format('Y-m-d') . (($background) ? ' 23:59' : ''),
                'end' => $fair['enddate']->format('Y-m-d') . (($background) ? ' 23:59' : ''),
                'color' => (($background) ? $this->colorcalendarback : $this->colorcalendar)
            ));
        }

        return $data;
    }

    /* obtener ferias de un mes concreto */

    public function getFairsMonth($month = '', $year = '', $forcalendar = false) {
        if (empty(($year))) {
            $year = date('Y');
        }

        if (empty($month)) {
            $month = date('m');
        }

        $this->getFairs(new \DateTime($year . '-' . $month . '-01'), new \DateTime($year . '-' . $month . '-31'), $forcalendar);
        if (!$forcalendar)
            return $data;

        return $this->renderforcalendarplugin($data);
    }

    /* Obtener ferias desde - hasta. Opcional, los devuelve todos si faltan los parámetros. */

    public function getFairs($from = '', $to = '', $forcalendar = false, $forbackground = false) {
        $data = $this->em->getRepository('FarmBundle:Fair')->getFairs($from, $to);
        if (!$forcalendar)
            return $data;

        /* Obtenemos también los fines de semana */
        if ($forcalendar && $forbackground) {
            while ($from <= $to) { // Loop will work begin to the end date 
                if ($from->format("N") == 6 || $from->format("N") == 7) { //Check that the day is Saturday or Sunday here
                    array_push($data, array('id' => 0, 'name' => '', 'fairdate' => new \Datetime($from->format('Y-m-d'))));
                }

                $from->modify('+1 day');
            }
        }
        return $this->renderforcalendarplugin($data, $forbackground);
    }

    /* Obtiene un array con los ferias */

    public function getFairsDates($from = '', $to = '') {
        $data = array();
        foreach ($this->getFairs($from, $to) as $k => $v) {
            $data[] = $v['fairdate']->format('Y-m-d');
        }
        return $data;
    }

    /* obtener un feria por fecha */

    public function getFairByDate($date) {
        return $this->em->getRepository('FarmBundle:Fair')->getFairByDate($date);
    }

    /* obtener un feria por id */

    public function getFairById($id) {
        $fair = $this->em->getRepository('FarmBundle:Fair')->getFairById($id);
        if ($fair) {
            $fair['sections'] = $this->getFairsections($id);
            $fair['animals'] = $this->getFairanimals($id);
        }
        return $fair;
    }

    /* Comprueba si una fecha es feria */

    public function isFair($date) {
        if ($this->getFairByDate($date))
            return true;
        return false;
    }

    /* Obtiene un feria por id, en formato objeto */

    public function getFairObject($id) {
        try {
            return $this->em->getRepository('FarmBundle:Fair')->findOneById($id);
        } catch (\Exception $e) {
            
        }
        return '';
    }

    /* Inserta un nuevo feria */

    public function insertFair($fair) {
        $this->em->persist($fair);
        $this->em->flush();
    }

    /* Elimina un feria */

    public function deleteFair($fair) {
        if (is_numeric($fair))
            $fair = $this->getFairObject($fair);
        $this->em->remove($fair);
        $this->em->flush();
    }

    /* Obtener secciones */

    public function getFairsections($id) {
        $data = $this->em->getRepository('FarmBundle:Fairsection')->getSections($id);
        foreach ($data as $k => $section) {
            $data[$k]['gender_name'] = $section['gender'] ? AnimalgenderType::getReadableValue($section['gender']) : '';
        }
        return $data;
    }

    /* Obtener animales */

    public function getFairanimals($id) {
        $animals = $this->em->getRepository('FarmBundle:Fairanimal')->getAnimals($id);
        $data = array();

        foreach ($animals as $k => $v) {
            $animal = $this->em->getRepository('FarmBundle:Animal')->getAnimal($v['animal']);
            $animal['gender_name'] = $animal['gender'] ? AnimalgenderType::getReadableValue($animal['gender']) : '';
            $animal['section'] = $this->getFairsectionByMonthAndGender($this->beinterface->getMonthsFromDate($animal['birthdate']), $animal['gender']);
            if ($animal['section']){
                $animal['section_name'] = $animal['section']['name'];
            }
            $animal['id'] = $v['id'];
            $animal['fair'] = $v['fair'];
            $data[] = $animal;
        }
        return $data;
    }

    /* Inserta una nueva sección */

    public function insertFairsection($fairsection) {
        $this->em->persist($fairsection);
        $this->em->flush();
    }

    /* Obtiene una sección por id, en formato objeto */

    public function getFairsectionObject($id) {
        try {
            return $this->em->getRepository('FarmBundle:Fairsection')->findOneById($id);
        } catch (\Exception $e) {
            return '';
        }
        return '';
    }

    /* Pone los datos de una sección (nombre y sexo) */

    protected function setFairsectionData($section) {
        $section['gender_name'] = $section['gender'] ? AnimalgenderType::getReadableValue($section['gender']) : '';
        $section['name'] = $section['gender_name'] . 's de ' . $section['frommonths'] . ' a ' . $section['tomonths'] . ' meses';
        return $section;
    }

    /* Obtiene una sección */

    public function getFairsection($id) {
        try {
            $section = $this->em->getRepository('FarmBundle:Fairsection')->getSection($id);
            if ($section) {
                $section = $this->setFairsectionData($section);
            }
        } catch (\Exception $e) {
            return '';
        }
        return '';
    }

    /* Obtener nombre */

    public function getFairsectionName($id) {
        $section = $this->getFairsection($id);
        if ($section)
            return $section['name'];
        return '';
    }

    /* Obtener sección por mes y sexo */

    public function getFairsectionByMonthAndGender($month, $gender) {
        $section = $this->em->getRepository('FarmBundle:Fairsection')->getSectionByMonthAndGender($month, $gender);
        if ($section) {
            $section = $this->setFairsectionData($section);
        }
        return $section;
    }

    /* Elimina una sección */

    public function deleteFairsection($fairsection) {
        if (is_numeric($fairsection))
            $fairsection = $this->getFairsectionObject($fairsection);

        $this->em->remove($fairsection);
        $this->em->flush();
    }

    /* Obtener animal */

    public function getAnimal($animal, $fair) {
        $fairanimal = $this->em->getRepository('FarmBundle:Fairanimal')->findBy(array('animal' => $this->em->getRepository('FarmBundle:Animal')->findOneById($animal), 'fair' => $this->getFairObject($fair)));

        if (count($fairanimal) > 0){
            return $fairanimal[0];
        }

        return '';
    }

    /* Insertar un animal */

    public function insertAnimal($animal, $fair) {
        $fairanimal = $this->getAnimal($animal, $fair);
        if (!$fairanimal)
            $fairanimal = new Fairanimal();
        $fairanimal->setAnimal($this->em->getRepository('FarmBundle:Animal')->findOneById($animal));
        $fairanimal->setFair($this->getFairObject($fair));
        $this->em->persist($fairanimal);
        $this->em->flush();
    }

    /* Elimina un animal */

    public function deleteAnimal($animal, $fair) {
        $fairanimal = $this->getAnimal($animal, $fair);

        if ($fairanimal) {
            $this->em->remove($fairanimal);
            $this->em->flush();
        }
    }
    
    /* Obtiene un animal por id, en formato objeto */

    public function getFairanimalObject($id) {
        try {
            return $this->em->getRepository('FarmBundle:Fairanimal')->findOneById($id);
        } catch (\Exception $e) {
            return '';
        }
        return '';
    }
}
