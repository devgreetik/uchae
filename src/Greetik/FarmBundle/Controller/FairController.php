<?php

namespace Greetik\FarmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\FarmBundle\Entity\Fair;
use Greetik\FarmBundle\Entity\Fairsection;
use Greetik\FarmBundle\Entity\Fairanimal;
use Greetik\FarmBundle\Form\Type\FairType;
use Greetik\FarmBundle\Form\Type\FairsectionType;
use Greetik\FarmBundle\Form\Type\FairanimalType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class FairController extends Controller {
    /* Mostramos pantalla de ferias */

    public function indexAction() {

        return $this->render('FarmBundle:Fair:index.html.twig', array(
                    'insertAllow' => $this->get('app.fairs')->getFairPerm('', 'insert'),
                    'data' => $data = $this->get('fairs.tools')->getFairs()
        ));
    }

    /* Obtener los ferias (para el calendario) */

    public function getFairsAction(Request $request) {
        if ($request->getMethod() != 'POST')
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación no permitida')), 200, array('Content-Type' => 'application/json'));

        try {
            $data = $this->get('fairs.tools')->getFairs($this->get('beinterface.tools')->getDateFromParams($request->get('start')), $this->get('beinterface.tools')->getDateFromParams($request->get('end')), true);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode($data), 200, array('Content-Type' => 'application/json'));
    }

    /* Mostrar el formulario para insertar */

    public function insertformAction($date = '') {
        $fair = new Fair();

        if (!empty($date)) {
            $date = new \Datetime($date);
        }

        $newForm = $this->createForm(FairType::class, $fair, array('_date' => $date));
        return $this->render('FarmBundle:Modals:insertfair.html.twig', array(
                    'new_form' => $newForm->createView()
        ));
    }

    /* Insertar una feria */

    public function insertAction(Request $request) {
        if ($request->getMethod() == "POST") {
            try {
                $fair = new Fair();
                $newForm = $this->createForm(FairType::class, $fair);
                $newForm->handleRequest($request);

                if ($newForm->isValid()) {
                    $this->get("app.fairs")->insertFair($fair);
                } else {
                    $errors = $editForm->getErrors(true, false);
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
                }
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        } else
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación no permitida')), 200, array('Content-Type' => 'application/json'));

        return new Response(json_encode(array('errorCode' => 0, 'data' => array('html' => $this->render('FarmBundle:Default:tablefairs.html.twig', array(
                            'data' => $this->get('fairs.tools')->getFairs(),
                            'insertAllow' => true
                        ))->getContent()))), 200, array('Content-Type' => 'application/json'));
    }

    /* Mostrar pantalla de una feria */

    public function viewAction($id) {
        $fair = $this->get('app.fairs')->getFair($id);

        $newForm = $this->createForm(FairType::class, $this->get('fairs.tools')->getFairObject($id));

        $modifyAllow = $this->get('app.fairs')->getFairPerm($fair, 'modify');
        return $this->render('FarmBundle:Fair:view.html.twig', array(
                    'item' => $fair,
                    'new_form' => $newForm->createView(),
                    'modifyAllow' => $modifyAllow,
                    'insertAllow' => $modifyAllow
        ));
    }

    /* elimina el feria */

    public function deleteAction(Request $request) {
        if ($request->getMethod() == "POST") {
            try {
                $fair = $this->get('app.fairs')->getFair($request->get('id'));
                if (!$fair)
                    throw new \Exception('No existe el feria que intenta eliminar');

                $this->get("app.fairs")->deleteFair($fair['id']);
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        } else
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación no permitida')), 200, array('Content-Type' => 'application/json'));

        return new Response(json_encode(array('errorCode' => 0, 'data' => array('html' => $this->render('FarmBundle:Default:tablefairs.html.twig', array(
                            'data' => $this->get('fairs.tools')->getFairs()
                        ))->getContent()))), 200, array('Content-Type' => 'application/json'));
    }

    /* Mostrar el formulario para insertar */

    public function insertsectionformAction($id_fair, $id = '') {

        if (!empty($id)) {
            $fairsection = $this->get('fairs.tools')->getFairsectionObject($id);
        } else {
            $fairsection = new Fairsection();
        }

        $newForm = $this->createForm(FairsectionType::class, $fairsection);
        return $this->render('FarmBundle:Modals:insertfairsection.html.twig', array(
                    'new_form' => $newForm->createView(),
                    'id_fair' => $id_fair,
                    'id'=>$id
        ));
    }

    /* Insertar una sección */

    public function insertsectionAction(Request $request, $id_fair, $id = '') {
        if ($request->getMethod() == "POST") {
            try {
                if (!empty($id)) {
                    $fairsection = $this->get('fairs.tools')->getFairsectionObject($id);
                } else {
                    $fairsection = new Fairsection();
                }
                $newForm = $this->createForm(FairsectionType::class, $fairsection);
                $newForm->handleRequest($request);

                if ($newForm->isValid()) {
                    if (empty($id))
                        $fairsection->setFair($this->get('fairs.tools')->getFairObject($id_fair));
                    $this->get("app.fairs")->insertFairsection($fairsection);
                }else {
                    $errors = $editForm->getErrors(true, false);
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
                }
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        } else
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación no permitida')), 200, array('Content-Type' => 'application/json'));

        return new Response(json_encode(array('errorCode' => 0, 'data' => array('html' => $this->render('FarmBundle:Default:tablefairsections.html.twig', array(
                            'id_fair' => $id_fair,
                            'data' => $this->get('fairs.tools')->getFairsections($id_fair),
                            'insertAllow' => true
                        ))->getContent()))), 200, array('Content-Type' => 'application/json'));
    }

    /* elimina una sección de la feria */

    public function deletesectionAction(Request $request) {
        if ($request->getMethod() == "POST") {
            try {
                $fairsection = $this->get('fairs.tools')->getFairsectionObject($request->get('id'));
                if (!$fairsection)
                    throw new \Exception('No existe la sección que intenta eliminar');

                $this->get("app.fairs")->deleteFairsection($fairsection);
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        } else
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación no permitida')), 200, array('Content-Type' => 'application/json'));

        return new Response(json_encode(array('errorCode' => 0, 'data' => array('html' => $this->render('FarmBundle:Default:tablefairsections.html.twig', array(
                            'data' => $this->get('fairs.tools')->getFairsections($fairsection->getFair()->getId()),
                            'id_fair'=>$fairsection->getFair()->getId(),
                            'insertAllow'=>true
                        ))->getContent()))), 200, array('Content-Type' => 'application/json'));
    }

    
        /* Mostrar el formulario para insertar */

    public function insertanimalformAction($id_fair, $id = '') {

        if (!empty($id)) {
            $fairanimal = $this->get('fairs.tools')->getFairanimalObject($id);
        } else {
            $fairanimal = new Fairanimal();
        }

        $newForm = $this->createForm(FairanimalType::class, $fairanimal);
        return $this->render('FarmBundle:Modals:insertfairanimal.html.twig', array(
                    'new_form' => $newForm->createView(),
                    'id_fair' => $id_fair,
                    'id'=>$id, 
                    'item'=>$fairanimal
        ));
    }

    /* Insertar una sección */

    public function insertanimalAction(Request $request, $id_fair, $id = '') {
        if ($request->getMethod() == "POST") {
            try {
                if (!empty($id)) {
                    $fairanimal = $this->get('fairs.tools')->getFairanimalObject($id);
                } else {
                    $fairanimal = new Fairanimal();
                }
                $newForm = $this->createForm(FairanimalType::class, $fairanimal);
                $newForm->handleRequest($request);

                if ($newForm->isValid()) {
                    if (!empty($id)) {
                        $this->get("app.fairs")->deleteAnimal($fairanimal->getAnimal()->getId(), $id_fair);
                    }
                    $this->get("app.fairs")->insertAnimal($newForm->get('animal')->getData(), $id_fair);
                }else {
                    $errors = $editForm->getErrors(true, false);
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
                }
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        } else
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación no permitida')), 200, array('Content-Type' => 'application/json'));

        return new Response(json_encode(array('errorCode' => 0, 'data' => array('html' => $this->render('FarmBundle:Default:tablefairanimals.html.twig', array(
                            'id_fair' => $id_fair,
                            'data' => $this->get('fairs.tools')->getFairanimals($id_fair),
                            'insertAllow' => true
                        ))->getContent()))), 200, array('Content-Type' => 'application/json'));
    }

    /* elimina una sección de la feria */

    public function deleteanimalAction(Request $request) {
        if ($request->getMethod() == "POST") {
            try {
                $fairanimal = $this->get('fairs.tools')->getFairanimalObject($request->get('id'));
                if (!$fairanimal)
                    throw new \Exception('No existe la sección que intenta eliminar');

                $this->get("app.fairs")->deleteAnimal($fairanimal->getAnimal()->getId(), $fairanimal->getFair()->getId());
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        } else
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación no permitida')), 200, array('Content-Type' => 'application/json'));

        return new Response(json_encode(array('errorCode' => 0, 'data' => array('html' => $this->render('FarmBundle:Default:tablefairanimals.html.twig', array(
                            'data' => $this->get('fairs.tools')->getFairanimals($fairanimal->getFair()->getId()),
                            'id_fair'=>$fairanimal->getFair()->getId(),
                            'insertAllow'=>true
                        ))->getContent()))), 200, array('Content-Type' => 'application/json'));
    }

}
