<?php

namespace Greetik\FarmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\FarmBundle\FarmBundle;
use Symfony\Component\HttpFoundation\Request;
use Greetik\FarmBundle\Entity\Farm;
use Greetik\FarmBundle\Form\Type\FarmType;
use Greetik\FarmBundle\Form\LoadfarmsForm;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {
    /* listar ganaderías */

    public function indexAction() {
        $data = $this->get($this->getParameter('farm.permsservice'))->getFarms();
        foreach($data as $k=>$v){
            if ($v['enddate']) $data[$k]['active']=false;
            else $data[$k]['active']=true;
        }
        return $this->render($this->getParameter('farm.interface').':index.html.twig', array(
            'data' => $data,
            'insertAllow' => $this->get($this->getParameter('farm.permsservice'))->getFarmPerm('insert')
        ));
    }

    /* insertar una ganadería */

    public function insertAction(Request $request) {
        $farm = new Farm();

        $newForm = $this->createForm(FarmType::class, $farm);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);

            if ($newForm->isValid()) {
                try {
                    $this->get($this->getParameter('farm.permsservice'))->insertFarm($farm);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage().'->'.$e->getLine().'->'.$e->getFile());
                    return $this->render($this->getParameter('farm.interface').':insert.html.twig', array('new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('farm_listfarms', array()));
            } else {
                $this->addFlash('error', (string) $newForm->getErrors(true, false));
                return $this->render($this->getParameter('farm.interface').':insert.html.twig', array('new_form' => $newForm->createView()));
            }
        }

        return $this->render($this->getParameter('farm.interface').':insert.html.twig', array('new_form' => $newForm->createView()));
    }

    /* obtener para selector */

    public function selectAction(Request $request) {
        if ($request->getMethod() != 'POST')
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación No Permitida')), 200, array('Content-Type' => 'application/json'));

        try {
            $data = $this->get($this->getParameter('farm.permsservice'))->getFarmsSelect($request->get('q'));
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() )), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0, 'data' => $data)), 200, array('Content-Type' => 'application/json'));
    }
    
    /* ver una ganadería */

    public function viewAction($id) {
        $editForm = $this->createForm(FarmType::class, $this->get('farms.tools')->getFarmObject($id));
        $farm = $this->get($this->getParameter('farm.permsservice'))->getFarm($id);

        return $this->render('AppBundle:Farm:view.html.twig', array(
                    'item' => $farm,
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get($this->getParameter('farm.permsservice'))->getFarmPerm('modify', $farm)
        ));
    }

    /* editar una ganadería */

    public function modifyAction(Request $request, $id) {
        $oldfarm = $this->get('farms.tools')->getFarm($id);
        $farmobject = $this->get('farms.tools')->getFarmObject($id);
        $editForm = $this->createForm(FarmType::class, $farmobject);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('farm.permsservice'))->modifyFarm($farmobject, $oldfarm);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
                return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
            }
        }

        $this->addFlash('error', $editForm->$editForm->getErrors(true, false));
        return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
    }

    /* Eliminar una ganadería */

    public function deleteAction(Request $request) {
        $id = $request->get('id');
        if ($request->getMethod() != 'POST') {
            $this->addFlash('error', 'No tiene permiso para hacer esta operación');
            return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
        }

        try {
            $this->get($this->getParameter('farm.permsservice'))->deleteFarm($id);
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
        }

        return $this->redirect($this->generateUrl('farm_listfarms'));
    }

    /*change avatar*/
    public function changeavatarAction(Request $request, $id) {
        try {
            $this->get($this->getParameter('farm.permsservice'))->changeAvatar($request->files->all()['avatar'], $id);
        } catch (\Exception $e) {
            if ($e->getCode() == 11)
                $this->addFlash('warning', $e->getMessage());
            else
                $this->addFlash('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
    }

    /* Importar Ganaderías */
    public function importformAction() {
        $formfile = $this->createForm(LoadfarmsForm::class);

        return $this->render('FarmBundle:Import:farms.html.twig', array(
                    'new_form' => $formfile->createView()
        ));
    }

    /* importar desde un fichero */

    public function importAction(Request $request) {
        try {
            if (count($request->files) <= 0 || !$request->files->get('loadfarms_form')) {
                $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error cargando fichero, cárguelo de nuevo, por favor')));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $data = $this->get($this->getParameter('farm.permsservice'))->importFarms($request->files->get('loadfarms_form')['xls']);
            $response = new Response(json_encode(array('errorCode' => 0, 'data' => $data)));
        } catch (\Exception $e) {
            $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() . ' (' . $e->getFile().' - '. $e->getLine() . ')')));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /*   Obtener poisición  */
    public function getpositionAction(Request $request, $id){
        if ($request->getMethod()!='POST') return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Operación no permitida')), 200, array('Content-Type'=>'application/json'));
        try{
            $farm = $this->get($this->getParameter('farm.permsservice'))->getFarm($id);
        }catch(\Exception $e){
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
        }
        
        return new Response(json_encode(array('errorCode'=>0, 'data'=>array('color'=>'000000', 'lat'=>$farm['lat'], 'lon'=>$farm['lng']))), 200, array('Content-Type'=>'application/json'));
    }

    public function savepositionAction(Request $request, $id){
        if ($request->getMethod()!='POST') return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Operación no permitida')), 200, array('Content-Type'=>'application/json'));
        try{
            $farm = $this->get('farms.tools')->getFarmObject($id);
            $farm->setLat($request->get('lat'));
            $farm->setLng($request->get('lon'));
            $this->get($this->getParameter('farm.permsservice'))->modifyFarm($farm, $this->get('farms.tools')->getFarm($id));
        }catch(\Exception $e){
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
        }
        
        return new Response(json_encode(array('errorCode'=>0)), 200, array('Content-Type'=>'application/json'));
    }
    
}
