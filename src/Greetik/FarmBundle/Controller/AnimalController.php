<?php

namespace Greetik\FarmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\FarmBundle\FarmBundle;
use Symfony\Component\HttpFoundation\Request;
use Greetik\FarmBundle\Entity\Animal;
use Greetik\FarmBundle\Form\Type\AnimalType;
use Greetik\FarmBundle\Form\Type\AnimalPartialType;
use Greetik\FarmBundle\Form\Type\AnimalindexType;
use Greetik\FarmBundle\Form\Type\AnimalmeritType;
use Symfony\Component\HttpFoundation\Response;
use Greetik\FarmBundle\Form\LoadanimalsForm;
use Greetik\FarmBundle\Form\LoadanimalsindexForm;
use Greetik\FarmBundle\Form\DropanimalForm;
use Greetik\FarmBundle\Form\Type\AnimalmasiveType;

class AnimalController extends Controller {

    /* obtener índice af, dt, ds */

    public function calculateindexAction(Request $request, $type) {
        if ($request->getMethod() != 'POST')
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación No Permitida')), 200, array('Content-Type' => 'application/json'));

        try {
            //throw new \Exception(json_encode($this->get('jms_serializer')->serialize($request->get('animalindex'), 'json')));
            $data=0;
            switch($type){
                case 'AF': $data = $this->get('farms.tools')->getIndexAF($request->get('animalindex')); break;
                case 'DS': $data = $this->get('farms.tools')->getIndexDS($request->get('animalindex')); break;
                case 'DM': $data = $this->get('farms.tools')->getIndexDM($request->get('animalindex')); break;
            }
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() . ' . ' . $e->getLine() . ' . ' . $e->getFile())), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0, 'data' => $data)), 200, array('Content-Type' => 'application/json'));
    }

    
    /* listar animales */

    public function indexAction(Request $request, $farm = '', $parent = '') {
        if ($request->getMethod() == 'POST') {
            $data = $this->get($this->getParameter('farm.permsservice'))->getAnimalsTable($request->get('sSearch'), $request->get('iSortCol_0'), $request->get('sSortDir_0'), $request->get('iDisplayStart'), $request->get('iDisplayLength'), $farm, $parent, $request->get('onlyActive'));
            $serializedEntity = $this->container->get('jms_serializer')->serialize(array_merge($data, array('sEcho' => intval($request->get('sEcho')))), 'json');
            $response = new Response($serializedEntity);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        return $this->render($this->getParameter('farm.interface_animal') . ':index.html.twig', array(
                    'insertAllow' => $this->get($this->getParameter('farm.permsservice'))->getFarmPerm('insert')
        ));
    }

    /* obtener para selector */

    public function selectAction(Request $request) {
        if ($request->getMethod() != 'POST')
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Operación No Permitida')), 200, array('Content-Type' => 'application/json'));

        try {
            $farm='';
            /*if ($this->get('systemuser.tools')->hasAccessToAdminPanel()){
                $farm='';
            }else $farm=$this->getUser()->getFarm();*/
            $data = $this->get($this->getParameter('farm.permsservice'))->getAnimalsSelect($request->get('q'), $request->get('gender'), $farm, false);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() . ' . ' . $e->getLine() . ' . ' . $e->getFile())), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0, 'data' => $data)), 200, array('Content-Type' => 'application/json'));
    }

    /* insertar un animal */

    public function insertAction(Request $request, $farm = '') {
        $animal = new Animal();

        $newForm = $this->createForm(AnimalType::class, $animal, array('_farm' => (($request->getMethod() != 'POST' && !empty($farm)) ? $this->get('farms.tools')->getFarmObject($farm) : '')));

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);

            if ($newForm->isValid()) {
                try {
                    $this->get($this->getParameter('farm.permsservice'))->insertAnimal($animal, $newForm->get('mother')->getData(), $newForm->get('father')->getData());
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                    return $this->render($this->getParameter('farm.interface_animal') . ':insert.html.twig', array(
                        'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                        'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()),
                        'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('farm_listanimals', array()));
            } else {
                $this->addFlash('error', (string) $newForm->getErrors(true, false));
                return $this->render($this->getParameter('farm.interface_animal') . ':insert.html.twig', array(
                    'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                    'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()),
                    'new_form' => $newForm->createView()));
            }
        }

        return $this->render($this->getParameter('farm.interface_animal') . ':insert.html.twig', array(
            'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
            'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()),
            'new_form' => $newForm->createView()));
    }

    /* insertar un animal */

    public function insertmasiveAction(Request $request, $farm) {
        $animal = new Animal();

        $newForm = $this->createForm(AnimalmasiveType::class, $animal, array('_farm' => (($request->getMethod() != 'POST' && !empty($farm)) ? $this->get('farms.tools')->getFarmObject($farm) : '')));

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);

            if ($newForm->isValid()) {
                try {
                    $animal->setFarm($this->get('farms.tools')->getFarmObject($farm));
                    if (!$animal->getName()) throw new \Exception('El nombre es obligatorio');
                    if (!$animal->getTattoo()) throw new \Exception('El tatuaje es obligatorio');
                    
                    $animal->setFarmbirth($animal->getFarm());
                    
                    $this->get($this->getParameter('farm.permsservice'))->insertAnimal($animal, $newForm->get('mother')->getData(), $newForm->get('father')->getData());
                    $this->addFlash('success', 'Animal Insertado Correctamente - Nombre: '.$animal->getName().' - Tatuaje: '.$animal->getTattoo().' - Fecha de Nacimiento: '.$animal->getBirthdate()->format('d/m/Y'));
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                    return $this->render($this->getParameter('farm.interface_animal') . ':insertmasive.html.twig', array(
                        'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
                        'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()),
                        'farm' => $farm, 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('farm_insertanimalmasive', array('farm' => $farm)));
            } else {
                $this->addFlash('error', (string) $newForm->getErrors(true, false));
            }
        }

        return $this->render($this->getParameter('farm.interface_animal') . ':insertmasive.html.twig', array(
            'next_registerd' => intval($this->get('farms.tools')->getMaxRegisterD()),
            'next_registerc' => intval($this->get('farms.tools')->getMaxRegisterC()),
            'farm' => $farm,
            'new_form' => $newForm->createView()));
    }

    /* ver un animal */

    public function viewAction($id) {
        $editForm = $this->createForm(AnimalType::class, $this->get('farms.tools')->getAnimalObject($id));
        $editFormpartial = $this->createForm(AnimalPartialType::class, $this->get('farms.tools')->getAnimalObject($id));
        $editindexForm = $this->createForm(AnimalmeritType::class, $this->get('farms.tools')->getAnimalObject($id));
        $animal = $this->get($this->getParameter('farm.permsservice'))->getAnimal($id);

        return $this->render($this->getParameter('farm.interface_animal') . ':view.html.twig', array(
                    'item' => $animal,
                    'new_form' => $editForm->createView(),
                    'new_form_partial' => $editFormpartial->createView(),
                    'new_form_index' => $editindexForm->createView(),
                    'modifyAllow' => $this->get($this->getParameter('farm.permsservice'))->getFarmPerm('modify', $this->get('farms.tools')->getFarmObject($animal['farm'])),
                    'modifypartialAllow' => $this->get($this->getParameter('farm.permsservice'))->getFarmPerm('modifypartial', $this->get('farms.tools')->getFarmObject($animal['farm'])),
                    'deleteAllow' => $this->get($this->getParameter('farm.permsservice'))->getAnimalPerm('delete', $this->get('farms.tools')->getAnimalObject($animal['id']))
        ));
    }

    /* editar un animal */

    public function modifyAction(Request $request, $id) {
        $animalobject = $this->get('farms.tools')->getAnimalObject($id);
        $oldanimal = $this->get('farms.tools')->getAnimal($id);
        $editForm = $this->createForm(AnimalType::class, $animalobject);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('farm.permsservice'))->modifyAnimal($animalobject, $editForm->get('mother')->getData(), $editForm->get('father')->getData(), $oldanimal);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            } else {
                $this->addFlash('error', (string) $editForm->getErrors(true, false));
            }
        }

        return $this->redirect($this->generateUrl('farm_viewanimal', array('id' => $id)));
    }

    /* editar índices de un animal */

    public function modifyindexAction(Request $request, $id, $fromvisit = '') {
        $animalobject = $this->get('farms.tools')->getAnimalObject($id);
        $oldanimal = $this->get('farms.tools')->getAnimal($id);
        $editForm = $this->createForm(AnimalindexType::class, $animalobject);

        if ($fromvisit) {
            $visitanimal = $this->get('visitanimals.tools')->getVisitanimalByAnimalAndVisit($fromvisit, $animalobject->getId());
            $visit = $this->get('app.visits')->getVisit($fromvisit);
            if (!$visitanimal) {
                $visitanimal = new \AppBundle\Entity\Visitanimal();
                $visitanimal->setAnimal($animalobject->getId());
                $visitanimal->setVisit($fromvisit);
            } else {
                $visitanimal = $this->get('visitanimals.tools')->getVisitanimalObject($visitanimal['id']);
            }
            try {
                $visitanimal->setIndexanimal(true);
                $warning = $this->get('app.visits')->insertAnimal($visitanimal);
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        }

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('farm.permsservice'))->modifyAnimal($animalobject, $animalobject->getMother() ? $animalobject->getMother()->getId() : '', $animalobject->getFather() ? $animalobject->getFather()->getId() : '', $oldanimal);
                } catch (\Exception $e) {
                    //$this->addFlash('error', $e->getLine() . '- ' . $e->getFile() . ' -' . $e->getMessage());
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
                }
            } else {
                //$this->addFlash('error', (string) $editForm->getErrors(true, false));
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $editForm->getErrors(true, false))), 200, array('Content-Type' => 'application/json'));
            }
        }

        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
        return $this->redirect($this->generateUrl('farm_viewanimal', array('id' => $id)));
    }

    /* editar méritos de un animal */

    public function modifymeritAction(Request $request, $id) {
        $animalobject = $this->get('farms.tools')->getAnimalObject($id);
        $oldanimal = $this->get('farms.tools')->getAnimal($id);
        $editForm = $this->createForm(AnimalmeritType::class, $animalobject);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('farm.permsservice'))->modifyAnimal($animalobject, $animalobject->getMother() ? $animalobject->getMother()->getId() : '', $animalobject->getFather() ? $animalobject->getFather()->getId() : '', $oldanimal);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getLine() . '- ' . $e->getFile() . ' -' . $e->getMessage());
                }
            } else {
                $this->addFlash('error', (string) $editForm->getErrors(true, false));
            }
        }

        return $this->redirect($this->generateUrl('farm_viewanimal', array('id' => $id)));
    }

    /* editar índices de un animal (formulario) */

    public function modifyindexformAction(Request $request, $id, $fromvisit = '') {
        $animalobject = $this->get('farms.tools')->getAnimalObject($id);
        $animal = $this->get('farms.tools')->getAnimal($id, true, true);
        $editForm = $this->createForm(AnimalindexType::class, $animalobject);

        return $this->render('FarmBundle:Modals:insertindex.html.twig', array('new_form' => $editForm->createView(), 'fromvisit' => $fromvisit, 'item' => $animalobject, 'animal'=>$animal));
    }

    /* Importar Animales */

    public function importformAction() {
        $formfile = $this->createForm(LoadanimalsForm::class);

        return $this->render('FarmBundle:Import:animals.html.twig', array(
                    'new_form' => $formfile->createView()
        ));
    }

    /* importar desde un fichero */

    public function importAction(Request $request) {
        try {
            if (count($request->files) <= 0 || !$request->files->get('loadanimals_form')) {
                $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error cargando fichero, cárguelo de nuevo, por favor')));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $data = $this->get($this->getParameter('farm.permsservice'))->importAnimals($request->files->get('loadanimals_form')['xls']);
            $response = new Response(json_encode(array('errorCode' => 0, 'data' => $data)));
        } catch (\Exception $e) {
            $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() . ' (' . $e->getFile() . ' - ' . $e->getLine() . ')')));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /* Importar méritos de los animales */

    public function importindexformAction() {
        $formfile = $this->createForm(LoadanimalsindexForm::class);

        return $this->render('FarmBundle:Import:animalsindex.html.twig', array(
                    'new_form' => $formfile->createView()
        ));
    }

    /* importar desde un fichero los méritos de los animales */

    public function importindexAction(Request $request) {
        try {
            if (count($request->files) <= 0 || !$request->files->get('loadanimalsindex_form')) {
                $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error cargando fichero, cárguelo de nuevo, por favor')));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $data = $this->get($this->getParameter('farm.permsservice'))->importAnimalsindex($request->files->get('loadanimalsindex_form')['xls']);
            $response = new Response(json_encode(array('errorCode' => 0, 'data' => $data)));
        } catch (\Exception $e) {
            $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() . ' (' . $e->getFile() . ' - ' . $e->getLine() . ')')));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /* Formulario para dar de baja un animal */

    public function dropformAction(Request $request, $id) {
        $dropform = $this->createForm(DropanimalForm::class, $this->get('farms.tools')->getAnimalObject($id));

        return $this->render('FarmBundle:Default:dropanimal.html.twig', array(
                    'item' => $this->get('farms.tools')->getAnimal($id),
                    'new_form' => $dropform->createView(),
                    'id' => $id
        ));
    }

    /* Formulario para dar de baja un animal */

    public function dropAction(Request $request, $id) {
        $dropform = $this->createForm(DropanimalForm::class, $this->get('farms.tools')->getAnimalObject($id));

        $animalobject = $this->get('farms.tools')->getAnimalObject($id);
        $oldanimal = $this->get('farms.tools')->getAnimal($id);
        $editForm = $this->createForm(DropanimalForm::class, $animalobject);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('farm.permsservice'))->modifyAnimalPartial($animalobject, $oldanimal);
                    $response = new Response(json_encode(array('errorCode' => 0)));
                } catch (\Exception $e) {
                    $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())));
                }
            } else {
                $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => (string) $editForm->getErrors(true, true))));
            }
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getPointsTextAction(Request $request, $id) {
        $animal = $this->get($this->getParameter('farm.permsservice'))->getAnimal($id);
        $points = $request->get('points');
        try {
            $response = new Response(json_encode(array('errorCode' => 0, 'data'=>$this->get('farms.tools')->getPointstext($points, $animal['gender']))));
        } catch (\Exception $e) {
            $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /* Eliminar un animal *

      public function deleteAction(Request $request) {
      $id = $request->get('id');
      if ($request->getMethod() != 'POST') {
      $this->addFlash('error', 'No tiene permiso para hacer esta operación');
      return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
      }

      try {
      $this->get($this->getParameter('farm.permsservice'))->deleteFarm($id);
      } catch (\Exception $e) {
      $this->addFlash('error', $e->getMessage());
      return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
      }

      return $this->redirect($this->generateUrl('farm_listfarms'));
      }

      /*change avatar*
      public function changeavatarAction(Request $request, $id) {
      try {
      $this->get($this->getParameter('farm.permsservice'))->changeAvatar($request->files->all()['avatar'], $id);
      } catch (\Exception $e) {
      if ($e->getCode() == 11)
      $this->addFlash('warning', $e->getMessage());
      else
      $this->addFlash('error', $e->getMessage());
      }
      return $this->redirect($this->generateUrl('farm_viewfarm', array('id' => $id)));
      }

      /* */
}
