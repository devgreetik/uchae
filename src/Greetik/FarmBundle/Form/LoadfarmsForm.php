<?php

namespace Greetik\FarmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostType
 *
 * @author Paco
 */
class LoadfarmsForm extends AbstractType {
    public function __construct() {
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('xls', FileType::class);
    }

    public function getName() {
        return 'Loadfarms';
    }

    public function getDefaultOptions(array $options) {
        return array();
    }

}
