<?php

namespace Greetik\FarmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Farm
 *
 * @author Paco
 */
class FairType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (empty($options['_date'])) $datadate = array();
        else{
            $datadate = array('data'=>$options['_date']);
        }

        $builder
                ->add('name')
                ->add('fairdate', DateType::class, array_merge(array('widget'=>'single_text', 'format'=>'dd/MM/yyyy', 'empty_data'=>new \Datetime()),$datadate))
                ->add('enddate', DateType::class, array('required'=>false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Hasta', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
                ;
    }

    public function getName() {
        return 'Fair';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\FarmBundle\Entity\Fair',
            '_date'=>''
        ));
    }

}
