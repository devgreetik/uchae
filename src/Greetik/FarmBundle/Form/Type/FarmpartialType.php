<?php

namespace Greetik\FarmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Farm
 *
 * @author Paco
 */
class FarmpartialType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name')
            ->add('address')
            ->add('zip')
            ->add('town')
            ->add('province')
            ->add('phone')
            ->add('phone2')
            ->add('contact')
            ->add('contact2')
            ->add('mobile')
            ->add('fax')
            ->add('namelabel')
            ->add('email');
    }

    public function getName()
    {
        return 'Farm';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\FarmBundle\Entity\Farm'
        ));
    }

}
