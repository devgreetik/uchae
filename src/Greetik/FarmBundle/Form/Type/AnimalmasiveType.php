<?php

namespace Greetik\FarmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Farm
 *
 * @author Paco
 */
class AnimalmasiveType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        if (!empty($options['_farm'])){
            $farmdata = array('data'=>$options['_farm']);
        }else{
            $farmdata=array();
        }
        
        $builder
                ->add('birthdate', DateType::class, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha de Nacimiento', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
                ->add('name')
                ->add('tattoo')
                ->add('crotal')
                ->add('gender')
                ->add('birthweight')
                ->add('mother', TextType::class, array('mapped'=>false, 'required'=>false))
                ->add('father', TextType::class, array('mapped'=>false, 'required'=>false))
                ->add('birthnum')
                ->add('calvingscore')
                ->add('twin')
                ;
    }

    public function getName() {
        return 'Animal';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\FarmBundle\Entity\Animal',
            '_farm'=>''
        ));
    }

}
