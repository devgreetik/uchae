<?php

namespace Greetik\FarmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Farm
 *
 * @author Paco
 */
class AnimalmeritType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('birthweight')
                ->add('weaningweight')
                ->add('easebirth')
                ->add('muscledev')
                ->add('weaningval')
                ->add('bonedev');
    }

    public function getName() {
        return 'Animal';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\FarmBundle\Entity\Animal'
        ));
    }

}
