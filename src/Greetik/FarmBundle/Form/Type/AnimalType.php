<?php

namespace Greetik\FarmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Farm
 *
 * @author Paco
 */
class AnimalType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if (!empty($options['_farm'])) {
            $farmdata = array('data' => $options['_farm']);
        } else {
            $farmdata = array();
        }

        $builder
            ->add('country')
            ->add('name')
            ->add('farm', EntityType::class, array_merge($farmdata, array('required' => false, 'placeholder' => '-', 'class' => 'FarmBundle:Farm',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.name', 'ASC');
                },
                'choice_label' => 'name')))
            ->add('farmbirth', EntityType::class, array_merge($farmdata, array('required' => false, 'placeholder' => '-', 'class' => 'FarmBundle:Farm',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.name', 'ASC');
                },
                'choice_label' => 'name')))
            ->add('tattoo')
            ->add('registerc')
            ->add('registerd')
            ->add('points')
            ->add('gender')
            ->add('forsale')
            ->add('price')
            ->add('mother', TextType::class, array('mapped' => false, 'required' => false))
            ->add('father', TextType::class, array('mapped' => false, 'required' => false))
            ->add('birthdate', DateType::class, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha de Nacimiento', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
            ->add('enddate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha de Nacimiento', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
            ->add('crotal')
            ->add('weaningcondition')
            ->add('weaningdate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha de Nacimiento', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
            ->add('indexdate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha Índices', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
            ->add('rnudate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha RNU', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
            ->add('rdudate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha RDU', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
            ->add('birthnum')
            ->add('calvingscore')
            ->add('twin');
    }

    public function getName()
    {
        return 'Animal';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\FarmBundle\Entity\Animal',
            '_farm' => ''
        ));
    }

}
