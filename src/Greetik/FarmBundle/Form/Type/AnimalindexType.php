<?php

namespace Greetik\FarmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Farm
 *
 * @author Paco
 */
class AnimalindexType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder/*->add('birthweight')
                ->add('weaningweight')
                ->add('easebirth')
                ->add('muscledev')
                ->add('weaningval')
                ->add('bonedev');*/
                ->add('index_ac')
                ->add('index_ad')
                ->add('index_rn')
                ->add('index_an')
                ->add('index_epl')
                ->add('index_gc')
                ->add('index_ld')
                ->add('index_lp')
                ->add('index_aa')
                ->add('index_tmn')
                ->add('index_m')
                ->add('index_aaa')
                ->add('index_aap')
                ->add('index_rd')
                ->add('index_cc')
                ->add('index_pp')
                ->add('index_ap')
                ->add('index_at')
                ->add('index_ln');
    }

    public function getName() {
        return 'Animal';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\FarmBundle\Entity\Animal'
        ));
    }

}
