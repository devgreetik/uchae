<?php

namespace Greetik\FarmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Farm
 *
 * @author Paco
 */
class FarmType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('rega')
            ->add('name')
            ->add('abb')
            ->add('cif')
            ->add('bankaccount')
            ->add('address')
            ->add('zip')
            ->add('town')
            ->add('province')
            ->add('contact')
            ->add('contact2')
            ->add('phone')
            ->add('phone2')
            ->add('mobile')
            ->add('fax')
            ->add('namelabel')
            ->add('owner')
            ->add('email')
            ->add('regaabb')
            ->add('fieldname')
            ->add('fieldtown')
            ->add('fieldprovince')
            ->add('description')
            ->add('enddate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha de Nacimiento', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')));
    }

    public function getName()
    {
        return 'Farm';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\FarmBundle\Entity\Farm'
        ));
    }

}
