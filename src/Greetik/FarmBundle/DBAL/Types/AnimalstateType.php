<?php
namespace Greetik\FarmBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class AnimalstateType extends AbstractEnumType
{
    const ACTIVE = 1;
    const REF = 2;
    const INNACTIVE = 3;
    
    protected static $choices = [
        self::ACTIVE => 'Activo',
        self::REF => 'Referencia',
        self::INNACTIVE => 'Inactivo'
    ];
    
    /*
     * UPDATE `animal` SET state=3 WHERE enddate IS NOT NULL
     * UPDATE `animal` SET state=2 WHERE farm IS NULL AND enddate IS NULL
     * UPDATE `animal` SET state=1 WHERE farm IS NOT NULL AND enddate IS NULL
     */
    
}