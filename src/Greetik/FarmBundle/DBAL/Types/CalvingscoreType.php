<?php
namespace Greetik\FarmBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class CalvingscoreType extends AbstractEnumType
{
    const NO = 1; //no difficulty, no assistance
    const MINOR = 2; //minor difficulty, some assistance
    const MAJOR = 3; //major difficulty, usually mechanical assistance
    const CAESAREAN = 4; //caesarean;
    const EMBRYOTOMY = 5; //embryotomy
    
    protected static $choices = [
        self::NO => '1 - Sin Dificultad',
        self::MINOR => '2 - Pequeña Dificultad',
        self::MAJOR => '3 - Mayor Dificultad',
        self::CAESAREAN => '4 - Cesárea',
        self::EMBRYOTOMY => '5 - Muerte'
    ];

}