<?php
namespace Greetik\FarmBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class AnimalcountryType extends AbstractEnumType
{
    const ES = 1;
    const UK = 2;
    const PT = 3;
    const ND = 4;
    const FR = 5;
    const DK = 6;
    const DE = 7;
    const BE = 8;
    
    protected static $choices = [
        self::ES => 'España',
        self::UK => 'Reino Unido',
        self::PT => 'Portugal',
        self::ND => 'Países Bajos',
        self::FR => 'Francia',
        self::DK => 'Dinamarca',
        self::DE => 'Alemania',
        self::BE => 'Bélgica'
    ];
    
    
    protected static $abbs = [
        self::ES => 'ES',
        self::UK => 'UK',
        self::PT => 'PT',
        self::ND => 'ND',
        self::FR => 'FR',
        self::DK => 'DK',
        self::DE => 'DE',
        self::BE => 'BE'
    ];
    
    //obtiene la abreviatura asociado a un tipo
    public function getAbb($type){
        return self::$abbs[$type];
    }

}