<?php
namespace Greetik\FarmBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class AnimalgenderType extends AbstractEnumType
{
    const MACHO = 1;
    const HEMBRA = 2;
    
    protected static $choices = [
        self::MACHO => 'Macho',
        self::HEMBRA => 'Hembra'
    ];
    
    
    protected static $colors = [
        self::MACHO => '#90C3D4',
        self::HEMBRA => '#DB5397'
    ];
    
    protected static $abbs = [
        self::MACHO => 'M',
        self::HEMBRA => 'H'
    ];
    
    protected static $icons = [
        self::MACHO => 'mars',
        self::HEMBRA => 'venus'
    ];

    //obtiene el icono asociado a un tipo
    public function getIcon($type){
        return self::$icons[$type];
    }

    //obtiene el color asociado a un tipo
    public function getColor($type){
        return self::$colors[$type];
    }

    //obtiene la abreviatura asociado a un tipo
    public function getAbb($type){
        return self::$abbs[$type];
    }

}