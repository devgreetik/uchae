<?php
namespace Greetik\FarmBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class WeaningconditionType extends AbstractEnumType
{
    const NORMAL = 0;
    const PENALIZED = 1; //penalized Calf (disease or accident)
    const TREATMENT = 2; //Calf with a specific individual positive treatment (preparation for a show for example)
    
    protected static $choices = [
        self::NORMAL => '0 -Normal',
        self::PENALIZED => '1 - Penalizado',
        self::TREATMENT => '2 -Tratamiento Individual Positivo'
    ];

}