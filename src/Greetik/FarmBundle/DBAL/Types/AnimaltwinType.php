<?php
namespace Greetik\FarmBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class AnimaltwinType extends AbstractEnumType
{
    const INDIVIDUAL = 1;
    const DOUBLE = 2; //mellizos
    const TRIPLE = 3; //Triple Parto
    
    protected static $choices = [
        self::INDIVIDUAL => 'Simple',
        self::DOUBLE => 'Dolbe',
        self::TRIPLE => 'Triple'
    ];

}