<?php

namespace Greetik\FarmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fairsection
 *
 * @ORM\Table(name="fairsection")
 * @ORM\Entity(repositoryClass="Greetik\FarmBundle\Repository\FairsectionRepository")
 */
class Fairsection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var AnimalgenderType
     *
     * @ORM\Column(name="gender", type="AnimalgenderType")
     */
    private $gender;

    /**
     * @var int
     *
     * @ORM\Column(name="frommonths", type="integer", nullable=true)
     */
    private $frommonths;

    /**
     * @var int
     *
     * @ORM\Column(name="tomonths", type="integer", nullable=true)
     */
    private $tomonths;

    /**
    * @ORM\ManyToOne(targetEntity="Fair")
    * @ORM\JoinColumn(name="fair", referencedColumnName="id", onDelete="CASCADE")
    */
    private $fair;     
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param AnimalgenderType $gender
     *
     * @return Fairsection
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return AnimalgenderType
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set frommonths
     *
     * @param integer $frommonths
     *
     * @return Fairsection
     */
    public function setFrommonths($frommonths)
    {
        $this->frommonths = $frommonths;

        return $this;
    }

    /**
     * Get frommonths
     *
     * @return int
     */
    public function getFrommonths()
    {
        return $this->frommonths;
    }

    /**
     * Set tomonths
     *
     * @param integer $tomonths
     *
     * @return Fairsection
     */
    public function setTomonths($tomonths)
    {
        $this->tomonths = $tomonths;

        return $this;
    }

    /**
     * Get tomonths
     *
     * @return int
     */
    public function getTomonths()
    {
        return $this->tomonths;
    }

    /**
     * Set fair
     *
     * @param \Greetik\FarmBundle\Entity\Fair $fair
     *
     * @return Fairsection
     */
    public function setFair(\Greetik\FarmBundle\Entity\Fair $fair = null)
    {
        $this->fair = $fair;

        return $this;
    }

    /**
     * Get fair
     *
     * @return \Greetik\FarmBundle\Entity\Fair
     */
    public function getFair()
    {
        return $this->fair;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Fairsection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
