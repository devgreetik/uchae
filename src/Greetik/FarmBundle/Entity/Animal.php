<?php

namespace Greetik\FarmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Greetik\FarmBundle\DBAL\Types\CalvingscoreType;
use Greetik\FarmBundle\DBAL\Types\WeaningconditionType;
use Greetik\FarmBundle\DBAL\Types\AnimalcountryType;

/**
 * Animal
 *
 * @ORM\Table(name="animal", indexes={
 *      @ORM\Index(name="gender", columns={"gender"}), @ORM\Index(name="country", columns={"country"}), @ORM\Index(name="forsale", columns={"forsale"}), @ORM\Index(name="provenew", columns={"provnew"}), @ORM\Index(name="provend", columns={"provend"}), @ORM\Index(name="state", columns={"state"}), @ORM\Index(name="registerc_num", columns={"registerc_num"}), @ORM\Index(name="registerd_num", columns={"registerd_num"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\FarmBundle\Repository\AnimalRepository")
 */
class Animal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tattoo", type="string", length=15, unique=true, nullable=true)
     */
    private $tattoo;

    /**
     * @var string
     *
     * @ORM\Column(name="friendlytattoo", type="string", length=15, nullable=true)
     */
    private $friendlytattoo;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="crotal", type="string", length=31,  nullable=true, unique=true)
     */
    private $crotal;

    /**
     * @var string
     *
     * @ORM\Column(name="registerc", type="string", length=15, nullable=true)
     */
    private $registerc;

    /**
     * @var string
     *
     * @ORM\Column(name="registerd", type="string", length=15, nullable=true)
     */
    private $registerd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enddate", type="date", nullable=true)
     */
    private $enddate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="indexdate", type="date", nullable=true)
     */
    private $indexdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rnudate", type="date", nullable=true)
     */
    private $rnudate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rdudate", type="date", nullable=true)
     */
    private $rdudate;

    /**
     * @var AnimalstateType
     *
     * @ORM\Column(name="gender", type="AnimalgenderType", nullable=true)
     */
    private $gender;

    /**
     * @var AnimalgenderType
     *
     * @ORM\Column(name="state", type="AnimalstateType", nullable=true)
     */
    private $state;

    /**
     * @var AnimalstateType
     *
     * @ORM\Column(name="laststate", type="AnimalstateType", nullable=true)
     */
    private $laststate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="birthweight", type="integer", nullable=true)
     */
    private $birthweight;

    /**
     * @var integer
     *
     * @ORM\Column(name="weaningweight", type="integer", nullable=true)
     */
    private $weaningweight;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="weaningdate", type="date", nullable=true)
     */
    private $weaningdate;

    /**
     * @var WeaningconditionType
     *
     * @ORM\Column(name="weaningcondition", type="WeaningconditionType", nullable=true)
     */
    private $weaningcondition;    
    
    /**
     * @var AnimaltwinType
     *
     * @ORM\Column(name="twin", type="AnimaltwinType", nullable=true)
     */
    private $twin;    
    
    /**
     * @var CalvingscoreType
     *
     * @ORM\Column(name="calvingscore", type="CalvingscoreType", nullable=true)
     */
    private $calvingscore;      
    
    /**
     * @var string
     *
     * @ORM\Column(name="register", type="string", length=31, nullable=true)
     */
    private $register;

    /**
     * @var string
     *
     * @ORM\Column(name="registerid", type="string", length=31, nullable=true)
     */
    private $registerid;

    /**
     * @var integer
     *
     * @ORM\Column(name="registerc_num", type="integer", nullable=true)
     */
    private $registerc_num;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="registerd_num", type="integer", nullable=true)
     */
    private $registerd_num;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="AnimalcountryType", nullable=true)
     */
    private $country;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true)
     */
    private $points;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="meatap", type="float", nullable=true)
     */
    private $meatap;

    /**
     * @var float
     *
     * @ORM\Column(name="easebirth", type="float", nullable=true)
     */
    private $easebirth;

    /**
     * @var float
     *
     * @ORM\Column(name="muscledev", type="float", nullable=true)
     */
    private $muscledev;

    /**
     * @var float
     *
     * @ORM\Column(name="weaningval", type="float", nullable=true)
     */
    private $weaningval;

    /**
     * @var float
     *
     * @ORM\Column(name="bonedev", type="float", nullable=true)
     */
    private $bonedev;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_ac", type="integer", nullable=true)
     */
    private $index_ac;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_ad", type="integer", nullable=true)
     */
    private $index_ad;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_rn", type="integer", nullable=true)
     */
    private $index_rn;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="index_an", type="integer", nullable=true)
     */
    private $index_an;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_epl", type="integer", nullable=true)
     */
    private $index_epl;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_gc", type="integer", nullable=true)
     */
    private $index_gc;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_ld", type="integer", nullable=true)
     */
    private $index_ld;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_lp", type="integer", nullable=true)
     */
    private $index_lp;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_aa", type="integer", nullable=true)
     */
    private $index_aa;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_tmn", type="integer", nullable=true)
     */
    private $index_tmn;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_m", type="integer", nullable=true)
     */
    private $index_m;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_aaa", type="integer", nullable=true)
     */
    private $index_aaa;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_aap", type="integer", nullable=true)
     */
    private $index_aap;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_rd", type="integer", nullable=true)
     */
    private $index_rd;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_cc", type="integer", nullable=true)
     */
    private $index_cc;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_pp", type="integer", nullable=true)
     */
    private $index_pp;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_ap", type="integer", nullable=true)
     */
    private $index_ap;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_at", type="integer", nullable=true)
     */
    private $index_at;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_ln", type="integer", nullable=true)
     */
    private $index_ln;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="birthnum", type="integer", nullable=true)
     */
    private $birthnum;


    /**
     * @var boolean
     *
     * @ORM\Column(name="provnew", type="boolean", nullable=true)
     */
    private $provnew=false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="provend", type="boolean", nullable=true)
     */
    private $provend=false;

    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="forsale", type="boolean", nullable=true)
     */
    private $forsale=false;

    
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="salefrom", type="date", nullable=true)
     */
    private $salefrom;

    
    /**
    * @ORM\ManyToOne(targetEntity="Farm", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="farm", referencedColumnName="id", onDelete="CASCADE")
    */
    private $farm;     

    /**
    * @ORM\ManyToOne(targetEntity="Farm", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="farmbirth", referencedColumnName="id", onDelete="CASCADE")
    */
    private $farmbirth;     

    /**
    * @ORM\ManyToOne(targetEntity="Animal", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="father", referencedColumnName="id", onDelete="CASCADE")
    */
    private $father;     
    
    /**
     * @ORM\OneToMany(targetEntity="Animal", mappedBy="father")
     */
    private $fatherof;     

    /**
    * @ORM\ManyToOne(targetEntity="Animal", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="mother", referencedColumnName="id", onDelete="CASCADE")
    */
    private $mother;     

    /**
     * @ORM\OneToMany(targetEntity="Animal", mappedBy="mother")
     */
    private $motherof;     

    /**
     * @ORM\OneToMany(targetEntity="Fairanimal", mappedBy="fair")
     */
    private $fairs;     
    
    public function __construct() {
        $this->motherof = new ArrayCollection();
        $this->fatherof = new ArrayCollection();
        $this->fairs = new ArrayCollection();
        $this->country = AnimalcountryType::ES;
    }
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tattoo
     *
     * @param string $tattoo
     *
     * @return Animal
     */
    public function setTattoo($tattoo)
    {
        $this->tattoo = $tattoo;

        return $this;
    }

    /**
     * Get tattoo
     *
     * @return string
     */
    public function getTattoo()
    {
        return $this->tattoo;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Animal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set crotal
     *
     * @param string $crotal
     *
     * @return Animal
     */
    public function setCrotal($crotal)
    {
        $this->crotal = $crotal;

        return $this;
    }

    /**
     * Get crotal
     *
     * @return string
     */
    public function getCrotal()
    {
        return $this->crotal;
    }

    /**
     * Set registerc
     *
     * @param string $registerc
     *
     * @return Animal
     */
    public function setRegisterc($registerc)
    {
        $this->registerc = $registerc;

        return $this;
    }

    /**
     * Get registerc
     *
     * @return string
     */
    public function getRegisterc()
    {
        return $this->registerc;
    }

    /**
     * Set registerd
     *
     * @param string $registerd
     *
     * @return Animal
     */
    public function setRegisterd($registerd)
    {
        $this->registerd = $registerd;

        return $this;
    }

    /**
     * Get registerd
     *
     * @return string
     */
    public function getRegisterd()
    {
        return $this->registerd;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Animal
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set gender
     *
     * @param AnimalgenderType $gender
     *
     * @return Animal
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return AnimalgenderType
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return Animal
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set birthweight
     *
     * @param integer $birthweight
     *
     * @return Animal
     */
    public function setBirthweight($birthweight)
    {
        $this->birthweight = $birthweight;

        return $this;
    }

    /**
     * Get birthweight
     *
     * @return int
     */
    public function getBirthweight()
    {
        return $this->birthweight;
    }

    /**
     * Set register
     *
     * @param string $register
     *
     * @return Animal
     */
    public function setRegister($register)
    {
        $this->register = $register;

        return $this;
    }

    /**
     * Get register
     *
     * @return string
     */
    public function getRegister()
    {
        return $this->register;
    }

    /**
     * Set registerid
     *
     * @param string $registerid
     *
     * @return Animal
     */
    public function setRegisterid($registerid)
    {
        $this->registerid = $registerid;

        return $this;
    }

    /**
     * Get registerid
     *
     * @return string
     */
    public function getRegisterid()
    {
        return $this->registerid;
    }


    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Animal
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set farm
     *
     * @param \Greetik\FarmBundle\Entity\Farm $farm
     *
     * @return Animal
     */
    public function setFarm(\Greetik\FarmBundle\Entity\Farm $farm = null)
    {
        $this->farm = $farm;

        return $this;
    }

    /**
     * Get farm
     *
     * @return \Greetik\FarmBundle\Entity\Farm
     */
    public function getFarm()
    {
        return $this->farm;
    }

    /**
     * Set farmbirth
     *
     * @param \Greetik\FarmBundle\Entity\Farm $farmbirth
     *
     * @return Animal
     */
    public function setFarmbirth(\Greetik\FarmBundle\Entity\Farm $farmbirth = null)
    {
        $this->farmbirth = $farmbirth;

        return $this;
    }

    /**
     * Get farmbirth
     *
     * @return \Greetik\FarmBundle\Entity\Farm
     */
    public function getFarmbirth()
    {
        return $this->farmbirth;
    }

    /**
     * Set father
     *
     * @param \Greetik\FarmBundle\Entity\Animal $father
     *
     * @return Animal
     */
    public function setFather(\Greetik\FarmBundle\Entity\Animal $father = null)
    {
        $this->father = $father;

        return $this;
    }

    /**
     * Get father
     *
     * @return \Greetik\FarmBundle\Entity\Animal
     */
    public function getFather()
    {
        return $this->father;
    }

    /**
     * Add fatherof
     *
     * @param \Greetik\FarmBundle\Entity\Animal $fatherof
     *
     * @return Animal
     */
    public function addFatherof(\Greetik\FarmBundle\Entity\Animal $fatherof)
    {
        $this->fatherof[] = $fatherof;

        return $this;
    }

    /**
     * Remove fatherof
     *
     * @param \Greetik\FarmBundle\Entity\Animal $fatherof
     */
    public function removeFatherof(\Greetik\FarmBundle\Entity\Animal $fatherof)
    {
        $this->fatherof->removeElement($fatherof);
    }

    /**
     * Get fatherof
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFatherof()
    {
        return $this->fatherof;
    }

    /**
     * Set mother
     *
     * @param \Greetik\FarmBundle\Entity\Animal $mother
     *
     * @return Animal
     */
    public function setMother(\Greetik\FarmBundle\Entity\Animal $mother = null)
    {
        $this->mother = $mother;

        return $this;
    }

    /**
     * Get mother
     *
     * @return \Greetik\FarmBundle\Entity\Animal
     */
    public function getMother()
    {
        return $this->mother;
    }

    /**
     * Add motherof
     *
     * @param \Greetik\FarmBundle\Entity\Animal $motherof
     *
     * @return Animal
     */
    public function addMotherof(\Greetik\FarmBundle\Entity\Animal $motherof)
    {
        $this->motherof[] = $motherof;

        return $this;
    }

    /**
     * Remove motherof
     *
     * @param \Greetik\FarmBundle\Entity\Animal $motherof
     */
    public function removeMotherof(\Greetik\FarmBundle\Entity\Animal $motherof)
    {
        $this->motherof->removeElement($motherof);
    }

    /**
     * Get motherof
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMotherof()
    {
        return $this->motherof;
    }

    /**
     * Set easebirth
     *
     * @param integer $easebirth
     *
     * @return Animal
     */
    public function setEasebirth($easebirth)
    {
        $this->easebirth = $easebirth;

        return $this;
    }

    /**
     * Get easebirth
     *
     * @return integer
     */
    public function getEasebirth()
    {
        return $this->easebirth;
    }

    /**
     * Set muscledev
     *
     * @param integer $muscledev
     *
     * @return Animal
     */
    public function setMuscledev($muscledev)
    {
        $this->muscledev = $muscledev;

        return $this;
    }

    /**
     * Get muscledev
     *
     * @return integer
     */
    public function getMuscledev()
    {
        return $this->muscledev;
    }

    /**
     * Set weaningval
     *
     * @param integer $weaningval
     *
     * @return Animal
     */
    public function setWeaningval($weaningval)
    {
        $this->weaningval = $weaningval;

        return $this;
    }

    /**
     * Get weaningval
     *
     * @return integer
     */
    public function getWeaningval()
    {
        return $this->weaningval;
    }

    /**
     * Set bonedev
     *
     * @param integer $bonedev
     *
     * @return Animal
     */
    public function setBonedev($bonedev)
    {
        $this->bonedev = $bonedev;

        return $this;
    }

    /**
     * Get bonedev
     *
     * @return integer
     */
    public function getBonedev()
    {
        return $this->bonedev;
    }

    /**
     * Set friendlytattoo
     *
     * @param string $friendlytattoo
     *
     * @return Animal
     */
    public function setFriendlytattoo($friendlytattoo)
    {
        $this->friendlytattoo = $friendlytattoo;

        return $this;
    }

    /**
     * Get friendlytattoo
     *
     * @return string
     */
    public function getFriendlytattoo()
    {
        return $this->friendlytattoo;
    }

    /**
     * Set forsale
     *
     * @param boolean $forsale
     *
     * @return Animal
     */
    public function setForsale($forsale)
    {
        $this->forsale = $forsale;

        return $this;
    }

    /**
     * Get forsale
     *
     * @return boolean
     */
    public function getForsale()
    {
        return $this->forsale;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Animal
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set weaningweight
     *
     * @param integer $weaningweight
     *
     * @return Animal
     */
    public function setWeaningweight($weaningweight)
    {
        $this->weaningweight = $weaningweight;

        return $this;
    }

    /**
     * Get weaningweight
     *
     * @return integer
     */
    public function getWeaningweight()
    {
        return $this->weaningweight;
    }

    /**
     * Set weaningdate
     *
     * @param \DateTime $weaningdate
     *
     * @return Animal
     */
    public function setWeaningdate($weaningdate)
    {
        $this->weaningdate = $weaningdate;

        return $this;
    }

    /**
     * Get weaningdate
     *
     * @return \DateTime
     */
    public function getWeaningdate()
    {
        return $this->weaningdate;
    }

    /**
     * Set weaningcondition
     *
     * @param WeaningconditionType $weaningcondition
     *
     * @return Animal
     */
    public function setWeaningcondition($weaningcondition)
    {
        $this->weaningcondition = $weaningcondition;

        return $this;
    }

    /**
     * Get weaningcondition
     *
     * @return WeaningconditionType
     */
    public function getWeaningcondition()
    {
        return $this->weaningcondition;
    }

    /**
     * Set calvingscore
     *
     * @param CalvingscoreType $calvingscore
     *
     * @return Animal
     */
    public function setCalvingscore($calvingscore)
    {
        $this->calvingscore = $calvingscore;

        return $this;
    }

    /**
     * Get calvingscore
     *
     * @return CalvingscoreType
     */
    public function getCalvingscore()
    {
        return $this->calvingscore;
    }

    /**
     * Set state
     *
     * @param AnimalstateType $state
     *
     * @return Animal
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return AnimalstateType
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Add fair
     *
     * @param \Greetik\FarmBundle\Entity\Fairanimal $fair
     *
     * @return Animal
     */
    public function addFair(\Greetik\FarmBundle\Entity\Fairanimal $fair)
    {
        $this->fairs[] = $fair;

        return $this;
    }

    /**
     * Remove fair
     *
     * @param \Greetik\FarmBundle\Entity\Fairanimal $fair
     */
    public function removeFair(\Greetik\FarmBundle\Entity\Fairanimal $fair)
    {
        $this->fairs->removeElement($fair);
    }

    /**
     * Get fairs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFairs()
    {
        return $this->fairs;
    }

    /**
     * Set laststate
     *
     * @param AnimalstateType $laststate
     *
     * @return Animal
     */
    public function setLaststate($laststate)
    {
        $this->laststate = $laststate;

        return $this;
    }

    /**
     * Get laststate
     *
     * @return AnimalstateType
     */
    public function getLaststate()
    {
        return $this->laststate;
    }

    /**
     * Set indexAc
     *
     * @param \integer $indexAc
     *
     * @return Animal
     */
    public function setIndexAc($indexAc)
    {
        $this->index_ac = $indexAc;

        return $this;
    }

    /**
     * Get indexAc
     *
     * @return \int
     */
    public function getIndexAc()
    {
        return $this->index_ac;
    }

    /**
     * Set indexAd
     *
     * @param \integer $indexAd
     *
     * @return Animal
     */
    public function setIndexAd( $indexAd)
    {
        $this->index_ad = $indexAd;

        return $this;
    }

    /**
     * Get indexAd
     *
     * @return \int
     */
    public function getIndexAd()
    {
        return $this->index_ad;
    }

    /**
     * Set indexRn
     *
     * @param \integer $indexRn
     *
     * @return Animal
     */
    public function setIndexRn($indexRn)
    {
        $this->index_rn = $indexRn;

        return $this;
    }

    /**
     * Get indexRn
     *
     * @return \int
     */
    public function getIndexRn()
    {
        return $this->index_rn;
    }

    /**
     * Set indexAn
     *
     * @param \integer $indexAn
     *
     * @return Animal
     */
    public function setIndexAn($indexAn)
    {
        $this->index_an = $indexAn;

        return $this;
    }

    /**
     * Get indexAn
     *
     * @return \int
     */
    public function getIndexAn()
    {
        return $this->index_an;
    }

    /**
     * Set indexEpl
     *
     * @param \integer $indexEpl
     *
     * @return Animal
     */
    public function setIndexEpl($indexEpl)
    {
        $this->index_epl = $indexEpl;

        return $this;
    }

    /**
     * Get indexEpl
     *
     * @return \int
     */
    public function getIndexEpl()
    {
        return $this->index_epl;
    }

    /**
     * Set indexGc
     *
     * @param \integer $indexGc
     *
     * @return Animal
     */
    public function setIndexGc($indexGc)
    {
        $this->index_gc = $indexGc;

        return $this;
    }

    /**
     * Get indexGc
     *
     * @return \int
     */
    public function getIndexGc()
    {
        return $this->index_gc;
    }

    /**
     * Set indexLd
     *
     * @param \integer $indexLd
     *
     * @return Animal
     */
    public function setIndexLd($indexLd)
    {
        $this->index_ld = $indexLd;

        return $this;
    }

    /**
     * Get indexLd
     *
     * @return \int
     */
    public function getIndexLd()
    {
        return $this->index_ld;
    }

    /**
     * Set indexLp
     *
     * @param \integer $indexLp
     *
     * @return Animal
     */
    public function setIndexLp($indexLp)
    {
        $this->index_lp = $indexLp;

        return $this;
    }

    /**
     * Get indexLp
     *
     * @return \int
     */
    public function getIndexLp()
    {
        return $this->index_lp;
    }

    /**
     * Set indexAa
     *
     * @param \integer $indexAa
     *
     * @return Animal
     */
    public function setIndexAa($indexAa)
    {
        $this->index_aa = $indexAa;

        return $this;
    }

    /**
     * Get indexAa
     *
     * @return \int
     */
    public function getIndexAa()
    {
        return $this->index_aa;
    }

    /**
     * Set indexTmn
     *
     * @param \integer $indexTmn
     *
     * @return Animal
     */
    public function setIndexTmn($indexTmn)
    {
        $this->index_tmn = $indexTmn;

        return $this;
    }

    /**
     * Get indexTmn
     *
     * @return \int
     */
    public function getIndexTmn()
    {
        return $this->index_tmn;
    }

    /**
     * Set indexM
     *
     * @param \integer $indexM
     *
     * @return Animal
     */
    public function setIndexM($indexM)
    {
        $this->index_m = $indexM;

        return $this;
    }

    /**
     * Get indexM
     *
     * @return \int
     */
    public function getIndexM()
    {
        return $this->index_m;
    }

    /**
     * Set indexAaa
     *
     * @param \integer $indexAaa
     *
     * @return Animal
     */
    public function setIndexAaa($indexAaa)
    {
        $this->index_aaa = $indexAaa;

        return $this;
    }

    /**
     * Get indexAaa
     *
     * @return \int
     */
    public function getIndexAaa()
    {
        return $this->index_aaa;
    }

    /**
     * Set indexAap
     *
     * @param \integer $indexAap
     *
     * @return Animal
     */
    public function setIndexAap($indexAap)
    {
        $this->index_aap = $indexAap;

        return $this;
    }

    /**
     * Get indexAap
     *
     * @return \int
     */
    public function getIndexAap()
    {
        return $this->index_aap;
    }

    /**
     * Set indexRd
     *
     * @param \integer $indexRd
     *
     * @return Animal
     */
    public function setIndexRd($indexRd)
    {
        $this->index_rd = $indexRd;

        return $this;
    }

    /**
     * Get indexRd
     *
     * @return \int
     */
    public function getIndexRd()
    {
        return $this->index_rd;
    }

    /**
     * Set indexCc
     *
     * @param \integer $indexCc
     *
     * @return Animal
     */
    public function setIndexCc($indexCc)
    {
        $this->index_cc = $indexCc;

        return $this;
    }

    /**
     * Get indexCc
     *
     * @return \int
     */
    public function getIndexCc()
    {
        return $this->index_cc;
    }

    /**
     * Set indexPp
     *
     * @param \integer $indexPp
     *
     * @return Animal
     */
    public function setIndexPp($indexPp)
    {
        $this->index_pp = $indexPp;

        return $this;
    }

    /**
     * Get indexPp
     *
     * @return \int
     */
    public function getIndexPp()
    {
        return $this->index_pp;
    }

    /**
     * Set indexAp
     *
     * @param \integer $indexAp
     *
     * @return Animal
     */
    public function setIndexAp($indexAp)
    {
        $this->index_ap = $indexAp;

        return $this;
    }

    /**
     * Get indexAp
     *
     * @return \int
     */
    public function getIndexAp()
    {
        return $this->index_ap;
    }

    /**
     * Set indexAt
     *
     * @param \integer $indexAt
     *
     * @return Animal
     */
    public function setIndexAt($indexAt)
    {
        $this->index_at = $indexAt;

        return $this;
    }

    /**
     * Get indexAt
     *
     * @return \int
     */
    public function getIndexAt()
    {
        return $this->index_at;
    }

    /**
     * Set indexLn
     *
     * @param \integer $indexLn
     *
     * @return Animal
     */
    public function setIndexLn($indexLn)
    {
        $this->index_ln = $indexLn;

        return $this;
    }

    /**
     * Get indexLn
     *
     * @return \int
     */
    public function getIndexLn()
    {
        return $this->index_ln;
    }

    /**
     * Set registercNum
     *
     * @param integer $registercNum
     *
     * @return Animal
     */
    public function setRegistercNum($registercNum)
    {
        $this->registerc_num = $registercNum;

        return $this;
    }

    /**
     * Get registercNum
     *
     * @return integer
     */
    public function getRegistercNum()
    {
        return $this->registerc_num;
    }

    /**
     * Set registerdNum
     *
     * @param integer $registerdNum
     *
     * @return Animal
     */
    public function setRegisterdNum($registerdNum)
    {
        $this->registerd_num = $registerdNum;

        return $this;
    }

    /**
     * Get registerdNum
     *
     * @return integer
     */
    public function getRegisterdNum()
    {
        return $this->registerd_num;
    }

    /**
     * Set meatap
     *
     * @param float $meatap
     *
     * @return Animal
     */
    public function setMeatap($meatap)
    {
        $this->meatap = $meatap;

        return $this;
    }

    /**
     * Get meatap
     *
     * @return float
     */
    public function getMeatap()
    {
        return $this->meatap;
    }

    /**
     * Set birthnum
     *
     * @param integer $birthnum
     *
     * @return Animal
     */
    public function setBirthnum($birthnum)
    {
        $this->birthnum = $birthnum;

        return $this;
    }

    /**
     * Get birthnum
     *
     * @return integer
     */
    public function getBirthnum()
    {
        return $this->birthnum;
    }

    /**
     * Set provnew
     *
     * @param boolean $provnew
     *
     * @return Animal
     */
    public function setProvnew($provnew)
    {
        $this->provnew = $provnew;

        return $this;
    }

    /**
     * Get provnew
     *
     * @return boolean
     */
    public function getProvnew()
    {
        return $this->provnew;
    }

    /**
     * Set provend
     *
     * @param boolean $provend
     *
     * @return Animal
     */
    public function setProvend($provend)
    {
        $this->provend = $provend;

        return $this;
    }

    /**
     * Get provend
     *
     * @return boolean
     */
    public function getProvend()
    {
        return $this->provend;
    }

    /**
     * Set salefrom
     *
     * @param \DateTime $salefrom
     *
     * @return Animal
     */
    public function setSalefrom($salefrom)
    {
        $this->salefrom = $salefrom;

        return $this;
    }

    /**
     * Get salefrom
     *
     * @return \DateTime
     */
    public function getSalefrom()
    {
        return $this->salefrom;
    }

    /**
     * Set twin
     *
     * @param AnimaltwinType $twin
     *
     * @return Animal
     */
    public function setTwin($twin)
    {
        $this->twin = $twin;

        return $this;
    }

    /**
     * Get twin
     *
     * @return AnimaltwinType
     */
    public function getTwin()
    {
        return $this->twin;
    }

    /**
     * Set country
     *
     * @param AnimalcountryType $country
     *
     * @return Animal
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return AnimalcountryType
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set indexdate
     *
     * @param \DateTime $indexdate
     *
     * @return Animal
     */
    public function setIndexdate($indexdate)
    {
        $this->indexdate = $indexdate;

        return $this;
    }

    /**
     * Get indexdate
     *
     * @return \DateTime
     */
    public function getIndexdate()
    {
        return $this->indexdate;
    }

    /**
     * Set rnudate
     *
     * @param \DateTime $rnudate
     *
     * @return Animal
     */
    public function setRnudate($rnudate)
    {
        $this->rnudate = $rnudate;

        return $this;
    }

    /**
     * Get rnudate
     *
     * @return \DateTime
     */
    public function getRnudate()
    {
        return $this->rnudate;
    }

    /**
     * Set rdudate
     *
     * @param \DateTime $rdudate
     *
     * @return Animal
     */
    public function setRdudate($rdudate)
    {
        $this->rdudate = $rdudate;

        return $this;
    }

    /**
     * Get rdudate
     *
     * @return \DateTime
     */
    public function getRdudate()
    {
        return $this->rdudate;
    }
}
