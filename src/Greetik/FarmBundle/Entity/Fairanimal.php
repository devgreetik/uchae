<?php

namespace Greetik\FarmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fairanimal
 *
 * @ORM\Table(name="fairanimal")
 * @ORM\Entity(repositoryClass="Greetik\FarmBundle\Repository\FairanimalRepository")
 */
class Fairanimal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="Fair")
    * @ORM\JoinColumn(name="fair", referencedColumnName="id", onDelete="CASCADE")
    */
    private $fair;     
    
    /**
    * @ORM\ManyToOne(targetEntity="Animal")
    * @ORM\JoinColumn(name="animal", referencedColumnName="id", onDelete="CASCADE")
    */
    private $animal;     
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fair
     *
     * @param \Greetik\FarmBundle\Entity\Fair $fair
     *
     * @return Fairanimal
     */
    public function setFair(\Greetik\FarmBundle\Entity\Fair $fair = null)
    {
        $this->fair = $fair;

        return $this;
    }

    /**
     * Get fair
     *
     * @return \Greetik\FarmBundle\Entity\Fair
     */
    public function getFair()
    {
        return $this->fair;
    }

    /**
     * Set animal
     *
     * @param \Greetik\FarmBundle\Entity\Animal $animal
     *
     * @return Fairanimal
     */
    public function setAnimal(\Greetik\FarmBundle\Entity\Animal $animal = null)
    {
        $this->animal = $animal;

        return $this;
    }

    /**
     * Get animal
     *
     * @return \Greetik\FarmBundle\Entity\Animal
     */
    public function getAnimal()
    {
        return $this->animal;
    }
}
