<?php

namespace Greetik\FarmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Farm
 *
 * @ORM\Table(name="farm")
 * @ORM\Entity(repositoryClass="Greetik\FarmBundle\Repository\FarmRepository")
 */
class Farm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rega", type="string", length=15, unique=true)
     */
    private $rega;

    /**
     * @var string
     *
     * @ORM\Column(name="cif", type="string", length=15, unique=true, nullable=true)
     */
    private $cif;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="abb", type="string", length=3, unique=true)
     */
    private $abb;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="bankaccount", type="string", length=255, nullable=true)
     */
    private $bankaccount;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=5, nullable=true)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="town", type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="province", type="ProvinceType", length=255, nullable=true)
     */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2", type="string", length=255, nullable=true)
     */
    private $contact2;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=12, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=12, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=12, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=12, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="namelabel", type="string", length=255, nullable=true)
     */
    private $namelabel;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255, nullable=true)
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="regaabb", type="string", length=10, nullable=true)
     */
    private $regaabb;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldname", type="string", length=255, nullable=true)
     */
    private $fieldname;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldtown", type="string", length=255, nullable=true)
     */
    private $fieldtown;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldprovince", type="ProvinceType", length=255, nullable=true)
     */
    private $fieldprovince;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=255, nullable=true)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", length=255, nullable=true)
     */
    private $lng;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;
    
    /**
     * @ORM\OneToMany(targetEntity="Animal", mappedBy="farm")
     */
    private $animals;      

    /**
     * @ORM\OneToMany(targetEntity="Animal", mappedBy="farmbirth")
     */
    private $animalsbirth;      

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enddate", type="date", nullable=true)
     */
    private $enddate;

    public function __construct() {
        $this->animals = new ArrayCollection();
        $this->animalsbirth = new ArrayCollection();
    }    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rega
     *
     * @param string $rega
     *
     * @return Farm
     */
    public function setRega($rega)
    {
        $this->rega = $rega;

        return $this;
    }

    /**
     * Get rega
     *
     * @return string
     */
    public function getRega()
    {
        return $this->rega;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Farm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set abb
     *
     * @param string $abb
     *
     * @return Farm
     */
    public function setAbb($abb)
    {
        $this->abb = $abb;

        return $this;
    }

    /**
     * Get abb
     *
     * @return string
     */
    public function getAbb()
    {
        return $this->abb;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Farm
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return Farm
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set town
     *
     * @param string $town
     *
     * @return Farm
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set province
     *
     * @param string $province
     *
     * @return Farm
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return Farm
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact2
     *
     * @param string $contact2
     *
     * @return Farm
     */
    public function setContact2($contact2)
    {
        $this->contact2 = $contact2;

        return $this;
    }

    /**
     * Get contact2
     *
     * @return string
     */
    public function getContact2()
    {
        return $this->contact2;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Farm
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Farm
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Farm
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     *
     * @return Farm
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set namelabel
     *
     * @param string $namelabel
     *
     * @return Farm
     */
    public function setNamelabel($namelabel)
    {
        $this->namelabel = $namelabel;

        return $this;
    }

    /**
     * Get namelabel
     *
     * @return string
     */
    public function getNamelabel()
    {
        return $this->namelabel;
    }

    /**
     * Set owner
     *
     * @param string $owner
     *
     * @return Farm
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Farm
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set regaabb
     *
     * @param string $regaabb
     *
     * @return Farm
     */
    public function setRegaabb($regaabb)
    {
        $this->regaabb = $regaabb;

        return $this;
    }

    /**
     * Get regaabb
     *
     * @return string
     */
    public function getRegaabb()
    {
        return $this->regaabb;
    }

    /**
     * Set fieldname
     *
     * @param string $fieldname
     *
     * @return Farm
     */
    public function setFieldname($fieldname)
    {
        $this->fieldname = $fieldname;

        return $this;
    }

    /**
     * Get fieldname
     *
     * @return string
     */
    public function getFieldname()
    {
        return $this->fieldname;
    }

    /**
     * Set fieldtown
     *
     * @param string $fieldtown
     *
     * @return Farm
     */
    public function setFieldtown($fieldtown)
    {
        $this->fieldtown = $fieldtown;

        return $this;
    }

    /**
     * Get fieldtown
     *
     * @return string
     */
    public function getFieldtown()
    {
        return $this->fieldtown;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Farm
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return Farm
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     *
     * @return Farm
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Farm
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return Farm
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Add animal
     *
     * @param \Greetik\FarmBundle\Entity\Animal $animal
     *
     * @return Farm
     */
    public function addAnimal(\Greetik\FarmBundle\Entity\Animal $animal)
    {
        $this->animals[] = $animal;

        return $this;
    }

    /**
     * Remove animal
     *
     * @param \Greetik\FarmBundle\Entity\Animal $animal
     */
    public function removeAnimal(\Greetik\FarmBundle\Entity\Animal $animal)
    {
        $this->animals->removeElement($animal);
    }

    /**
     * Get animals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnimals()
    {
        return $this->animals;
    }

    /**
     * Add animalsbirth
     *
     * @param \Greetik\FarmBundle\Entity\Animal $animalsbirth
     *
     * @return Farm
     */
    public function addAnimalsbirth(\Greetik\FarmBundle\Entity\Animal $animalsbirth)
    {
        $this->animalsbirth[] = $animalsbirth;

        return $this;
    }

    /**
     * Remove animalsbirth
     *
     * @param \Greetik\FarmBundle\Entity\Animal $animalsbirth
     */
    public function removeAnimalsbirth(\Greetik\FarmBundle\Entity\Animal $animalsbirth)
    {
        $this->animalsbirth->removeElement($animalsbirth);
    }

    /**
     * Get animalsbirth
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnimalsbirth()
    {
        return $this->animalsbirth;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Farm
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set bankaccount
     *
     * @param string $bankaccount
     *
     * @return Farm
     */
    public function setBankaccount($bankaccount)
    {
        $this->bankaccount = $bankaccount;

        return $this;
    }

    /**
     * Get bankaccount
     *
     * @return string
     */
    public function getBankaccount()
    {
        return $this->bankaccount;
    }

    /**
     * Set cif
     *
     * @param string $cif
     *
     * @return Farm
     */
    public function setCif($cif)
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * Get cif
     *
     * @return string
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set fieldprovince
     *
     * @param ProvinceType $fieldprovince
     *
     * @return Farm
     */
    public function setFieldprovince($fieldprovince)
    {
        $this->fieldprovince = $fieldprovince;

        return $this;
    }

    /**
     * Get fieldprovince
     *
     * @return ProvinceType
     */
    public function getFieldprovince()
    {
        return $this->fieldprovince;
    }
}
