<?php

namespace Greetik\FarmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fair
 *
 * @ORM\Table(name="fair")
 * @ORM\Entity(repositoryClass="Greetik\FarmBundle\Repository\FairRepository")
 */
class Fair
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fairdate", type="date")
     */
    private $fairdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enddate", type="date")
     */
    private $enddate;

    /**
     * @ORM\OneToMany(targetEntity="Fairsection", mappedBy="fair")
     */
    private $sections;     
    
    public function __construct() {
        $this->sections = new \Doctrine\Common\Collections\ArrayCollection;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Fair
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fairdate
     *
     * @param \DateTime $fairdate
     *
     * @return Fair
     */
    public function setFairdate($fairdate)
    {
        $this->fairdate = $fairdate;

        return $this;
    }

    /**
     * Get fairdate
     *
     * @return \DateTime
     */
    public function getFairdate()
    {
        return $this->fairdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Fair
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Add section
     *
     * @param \Greetik\FarmBundle\Entity\Fairsection $section
     *
     * @return Fair
     */
    public function addSection(\Greetik\FarmBundle\Entity\Fairsection $section)
    {
        $this->sections[] = $section;

        return $this;
    }

    /**
     * Remove section
     *
     * @param \Greetik\FarmBundle\Entity\Fairsection $section
     */
    public function removeSection(\Greetik\FarmBundle\Entity\Fairsection $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }
}
