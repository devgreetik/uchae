<?php

namespace Greetik\FarmBundle\Twig;


class AppExtension extends \Twig_Extension {

    private $farms;

    public function __construct($_farms) {
        $this->farms = $_farms;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('getFarmName', array($this, 'getFarmName')), //Obtener nombre de una ganadería
            new \Twig_SimpleFilter('getPointstext', array($this, 'getPointstext')) //Obtener nombre de una ganadería
        );
    }


    /*Devuelve el nombre de la ganadería*/
    public function getFarmName($id){
        return $this->farms->getFarmname($id);
    }
    
    /*Devuelve el texto asociado a un número de puntos*/
    public function getPointstext($id, $gender){
        return $this->farms->getPointstext($id, $gender);
    }
    
    
    public function getName() {
        return 'farms_twig_extension';
    }

}
