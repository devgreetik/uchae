<?php
namespace Greetik\VisitsBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class VisitstateType extends AbstractEnumType
{
    const NOTSENDED = 1;
    const PARTSENDED = 2;
    const SENDED = 3;
    
    protected static $choices = [
        self::NOTSENDED =>"No Enviado",
        self::PARTSENDED =>"Parcialmente Enviado",
        self::SENDED =>"Enviado"
    ];    
    
}