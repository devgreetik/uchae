<?php

namespace Greetik\VisitsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VisitsType
 *
 * @author Paco
 */
class VisitType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('description')
                ->add('visitdate', DateType::class, array('required'=>false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy'))
                ->add('privatedescription')
                ->add('state');

    }

    public function getName() {
        return 'Visit';
    }

    public function getDefaultOptions(array $options) {
        return array('data_class' => 'Greetik\VisitsBundle\Entity\Visit');
    }

}
