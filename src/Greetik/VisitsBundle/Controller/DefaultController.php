<?php

namespace Greetik\VisitsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Greetik\VisitsBundle\Entity\Visit;
use Greetik\VisitsBundle\Form\Type\VisitType;

class DefaultController extends Controller
{

    
    /**
    * Show the new visit insert form
    * 
    * @author Pacolmg
    */
     public function insertformAction($item_id, $item_type, $id='')
     {
        
         if (!empty($id)){$visit=$this->get('visits.tools')->getVisitObject($id);}
         else{
            $visit=new Visit();
         }
         
         $newForm = $this->createForm(VisitType::class, $visit);

        return $this->render('VisitsBundle:Default:insert.html.twig',array('new_form' => $newForm->createView(), 'item_id' => $item_id, 'item_type'=>$item_type,'id' =>$id));
    }

    /**
    * Edit the data of an visits or insert a new one
    * 
    * @param int $id is received by Get Request
    * @param Visit $item is received by Post Request
    * @author Pacolmg
    */
     public function insertmodifyAction(Request $request, $item_id, $item_type, $id=''){
        $item = $request->get('visit');
      
        if (!empty($id)){
            $visit=$this->get('visits.tools')->getVisitObject($id);
            $editing = true;
        }else{ $visit = new Visit(); $editing=false;}

         $editForm = $this->createForm(VisitType::class, $visit);
         $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            if ($editing){
                try{
                    $this->get($this->getParameter('visits.permsservice'))->modifyVisit($visit);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }
            }else{
                try{
                    $visit->setItemtype($item_type);
                    $visit->setItemid($item_id);
                    //$visit->setNumorder(0);
                    $this->get($this->getParameter('visits.permsservice'))->insertVisit($visit);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }                
            }
            
            return $this->render('AppBundle:Visit:index.html.twig', array('data' => $this->get($this->getParameter('visits.permsservice'))->getVisitsByItem($visit->getItemid(), $visit->getItemtype()), '_itemid'=>$visit->getItemid(), '_itemtype'=>$visit->getItemtype(), 'modifyAllow'=>true));
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }

     /**
    * Delete a visits
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function dropAction($id)
     {
        $visit = $this->get('visits.tools')->getVisitObject($id);
        
       try{
        $this->get($this->getParameter('visits.permsservice'))->deleteVisit($visit);
       }catch(\Exception $e){
           return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
       }
       
        return $this->render('VisitsBundle:Default:index.html.twig', array('data' => $this->get($this->getParameter('visits.permsservice'))->getVisitsByItem($visit->getItemid(), $visit->getItemtype()), '_itemid'=>$visit->getItemid(), '_itemtype'=>$visit->getItemtype(), 'modifyAllow'=>true));
     }
}
