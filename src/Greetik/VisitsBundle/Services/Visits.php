<?php

namespace Greetik\VisitsBundle\Services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Visits Tools
 *
 * @author Pacolmg
 */
class Visits {

    private $em;

    public function __construct($_entityManager) {
        $this->em = $_entityManager;
    }

   
    public function getVisitsByItemtype($item_type) {
        return $this->em->getRepository('VisitsBundle:Visit')->getVisitsByItemtype($item_type);
    }

    public function getVisitsByItem($item_id, $item_type) {
        $data = array();
        foreach ($this->em->getRepository('VisitsBundle:Visit')->getVisitsByItem($item_id, $item_type) as $visit){
            $data[] = array_merge($visit, array('statename'=> \Greetik\VisitsBundle\DBAL\Types\VisitstateType::getReadableValue($visit['state'])));
        }
        return $data;
    }

    public function getVisitObject($id) {
            return $this->em->getRepository('VisitsBundle:Visit')->findOneById($id);
    }

    public function getVisit($id) {
            return $this->em->getRepository('VisitsBundle:Visit')->getVisit($id);
    }

    public function modifyVisit($visit) {
        $this->em->persist($visit);
        $this->em->flush();
    }

    public function insertVisit($visit) {
        $this->em->persist($visit);
        $this->em->flush();
    }

    public function deleteVisit($visit) {
        if (is_numeric($visit)) $visit = $this->getVisitObject ($visit);
        $this->em->remove($visit);
        $this->em->flush();
    }
}
