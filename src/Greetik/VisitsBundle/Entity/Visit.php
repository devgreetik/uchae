<?php

namespace Greetik\VisitsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Visit
 *
 * @ORM\Table(name="visit", indexes={
 *      @ORM\Index(name="itemtype", columns={"itemtype"}),  @ORM\Index(name="itemid", columns={"itemid"}),  @ORM\Index(name="item", columns={"itemid", "itemtype"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\VisitsBundle\Repository\VisitRepository")
 */
class Visit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="visitdate", type="datetime")
     */
    private $visitdate;

    /**
     * @var int
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255)
     */
    private $itemtype;

    /**
     * @var int
     *
     * @ORM\Column(name="itemid", type="integer")
     */
    private $itemid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime")
     */
    private $createdat;

    /**
     * @var bool
     *
     * @ORM\Column(name="done", type="boolean")
     */
    private $done=false;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=1023, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="privatedescription", type="text", length=1023, nullable=true)
     */
    private $privatedescription;


    /**
     * @var VisitstateType
     *
     * @ORM\Column(name="state", type="VisitstateType")
     */
    private $state;


    public function __construct() {
        $this->createdat = new \Datetime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visitdate
     *
     * @param \DateTime $visitdate
     *
     * @return Visit
     */
    public function setVisitdate($visitdate)
    {
        $this->visitdate = $visitdate;

        return $this;
    }

    /**
     * Get visitdate
     *
     * @return \DateTime
     */
    public function getVisitdate()
    {
        return $this->visitdate;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Visit
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     *
     * @return Visit
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set itemid
     *
     * @param integer $itemid
     *
     * @return Visit
     */
    public function setItemid($itemid)
    {
        $this->itemid = $itemid;

        return $this;
    }

    /**
     * Get itemid
     *
     * @return int
     */
    public function getItemid()
    {
        return $this->itemid;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Visit
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set done
     *
     * @param boolean $done
     *
     * @return Visit
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return bool
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Visit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set privatedescription
     *
     * @param string $privatedescription
     *
     * @return Visit
     */
    public function setPrivatedescription($privatedescription)
    {
        $this->privatedescription = $privatedescription;

        return $this;
    }

    /**
     * Get privatedescription
     *
     * @return string
     */
    public function getPrivatedescription()
    {
        return $this->privatedescription;
    }

    /**
     * Set state
     *
     * @param VisitstateType $state
     *
     * @return Visit
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return VisitstateType
     */
    public function getState()
    {
        return $this->state;
    }
}
