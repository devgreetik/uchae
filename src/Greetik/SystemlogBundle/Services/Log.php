<?php

namespace Greetik\SystemlogBundle\Services;

use Greetik\SystemlogBundle\Entity\Logentry;
use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;


/**
 * Description of Subspecialty Tools
 *
 * @author Pacolmg
 */
class Log {

    private $em;
    private $token;
    private $entityuser;
    private $request;
    
    public function __construct($_entityManager, $_token, $_entityuser, $_request) {
        $this->em = $_entityManager;
        $this->token = $_token;
        $this->entityuser = $_entityuser;
        $this->request = $_request;
    }

    /* Inserta una entrada en el log*/
    public function insertEntry($action, $description, $itemid='', $itemtype=''){
        $entry = new Logentry();
        $entry->setUser($this->token->getToken()->getUser()->getId());
        $entry->setLogtime(new \Datetime());
        $entry->setLogaction($action);
        $entry->setLogdescription($description);
        $entry->setIp($this->request->getCurrentRequest()->server->get('REMOTE_ADDR'));
        if (!empty($itemid) && !empty($itemtype)){
            $entry->setItemid($itemid);
            $entry->setItemtype($itemtype);
        }
        //$entry->setIp($this->request->getCurrentRequest()->getClientIp());
        $this->em->persist($entry);
        $this->em->flush();
    }
    
    /* obtener acciones para tabla datatable ajax */

    public function getLogTable($search, $sortcol, $sortdir, $start, $lenght, $from='', $to='', $user='', $action='', $itemid='', $itemtype='') {
        switch ($sortcol) {
            case 0: $sortcol = "l.id";
                break;
            case 1: $sortcol = "l.ip";
                break;
            case 2: $sortcol = "l.logtime";
                break;
            case 3: $sortcol = "u.username";
                break;
            case 4: $sortcol = "u.logaction";
                break;
            case 5: $sortcol = "l.logdescription";
                break;
        }

        $sortdir = strtoupper($sortdir);

        if ($sortdir != 'ASC' && $sortdir != 'DESC')
            $sortdir = 'ASC';

        $numerrors = 0;
        foreach($data = $this->getLogentriesTable($search, $sortcol, $sortdir, $start, $lenght, $from, $to, $user, $action, $itemid, $itemtype) as $k=>$v){
         $data[$k] = array(
             $v['id'],
             $v['ip'],
             $v['logtime']->format('d/m/Y H:i:s'),
             $v['username'],
             (($v['logaction'])?LogactionsType::getReadableValue($v['logaction']):'-'),
             $v['logdescription']
         );   
        }

        $number = intval($this->getLogentriesNumber($search, $from, $to, $user, $action, $itemid, $itemtype));

        return array('iTotalRecords' => $number - $numerrors, 'iTotalDisplayRecords' => $number, "aaData" => $data);
    }

    
    /*Obtiene el log de una petición de datatable*/
    public function getLogentriesTable($search, $sortcol, $sortdir, $start, $lenght, $from='', $to='', $user='', $action=''){
        return $this->em->getRepository('SystemlogBundle:Logentry')->getLogTable($search, $sortcol, $sortdir, $start, $lenght, $this->entityuser, $from, $to, $user, $action);
    }
    
    /*Obtiene el número de entradas de una petición de datatable*/
    public function getLogentriesNumber($search, $from='', $to='', $user='', $action=''){
        return $this->em->getRepository('SystemlogBundle:Logentry')->getLogTableNumber($search, $this->entityuser, $from, $to, $user, $action);
    }
}
