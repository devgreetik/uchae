<?php
namespace Greetik\SystemlogBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class LogactionsType extends AbstractEnumType
{
    const INSERT = 1;
    const EDIT = 2;
    const DELETE = 3;
    const OTHER = 4;
    
    protected static $choices = [
        self::INSERT => 'Inserción',
        self::EDIT => 'Modificación',
        self::DELETE => 'Borrado',
        self::OTHER => 'Otra'
    ];

    protected static $colors = [
        self::INSERT => '#1ea500',
        self::EDIT => '#ba0303',
        self::DELETE => '#666',
        self::OTHER => '#0d723b'
    ];

    //obtiene el color asociado a un tipo
    public function getColor($type){
        return self::$colors[$type];
    }
    
}