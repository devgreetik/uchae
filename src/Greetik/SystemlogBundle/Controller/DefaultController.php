<?php

namespace Greetik\SystemlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;

class DefaultController extends Controller
{
    /* listar log */

    public function indexAction(Request $request) {
        if ($request->getMethod() == 'POST') {
            $data = $this->get($this->getParameter('systemlog.appservice'))->getLogTable($request->get('sSearch'), $request->get('iSortCol_0'), $request->get('sSortDir_0'), $request->get('iDisplayStart'), $request->get('iDisplayLength'), $request->get('fromdate') ? $this->get('beinterface.tools')->dateFromPickDate($request->get('fromdate')):'', $request->get('todate') ? $this->get('beinterface.tools')->dateFromPickDate($request->get('todate')):'', $request->get('user'), $request->get('action'));
            $serializedEntity = $this->container->get('jms_serializer')->serialize(array_merge($data, array('sEcho' => intval($request->get('sEcho')))), 'json');
            $response = new Response($serializedEntity);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        return $this->render($this->getParameter('systemlog.interface').':index.html.twig', array('actions'=> LogactionsType::getChoices()));
    }
}
