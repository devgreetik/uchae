<?php

namespace Greetik\SystemlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Greetik\SystemlogBundle\DBAL\Types\LogactionsType;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Logentry
 *
 * @ORM\Table(name="logentry", indexes={
 *      @ORM\Index(name="item_type", columns={"item_type"}),  @ORM\Index(name="item_id", columns={"item_id"}),@ORM\Index(name="item", columns={"item_id", "item_type"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\SystemlogBundle\Repository\LogentryRepository")
 */
class Logentry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="logtime", type="datetime")
     */
    private $logtime;

    /**
     * @var string
     *
     * @ORM\Column(name="logdescription", type="string", length=1024)
     */
    private $logdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=15)
     */
    private $ip;

    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=true)
     */
    private $itemid;
  
    /**
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", length=255, nullable=true)
     */
    private $itemtype;    

    /**
     * @var int
     *
     * @ORM\Column(name="logaction", type="LogactionsType", nullable=true)
     * @DoctrineAssert\Enum(entity="Greetik\SystemlogBundle\DBAL\Types\LogactionsType")   
     */
    private $logaction;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Logentry
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set logtime
     *
     * @param \DateTime $logtime
     *
     * @return Logentry
     */
    public function setLogtime($logtime)
    {
        $this->logtime = $logtime;

        return $this;
    }

    /**
     * Get logtime
     *
     * @return \DateTime
     */
    public function getLogtime()
    {
        return $this->logtime;
    }

    /**
     * Set logaction
     *
     * @param string $logaction
     *
     * @return Logentry
     */
    public function setLogaction($logaction)
    {
        $this->logaction = $logaction;

        return $this;
    }

    /**
     * Get logaction
     *
     * @return string
     */
    public function getLogaction()
    {
        return $this->logaction;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Logentry
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set logdescription
     *
     * @param string $logdescription
     *
     * @return Logentry
     */
    public function setLogdescription($logdescription)
    {
        $this->logdescription = $logdescription;

        return $this;
    }

    /**
     * Get logdescription
     *
     * @return string
     */
    public function getLogdescription()
    {
        return $this->logdescription;
    }

    /**
     * Set itemid
     *
     * @param integer $itemid
     *
     * @return Logentry
     */
    public function setItemid($itemid)
    {
        $this->itemid = $itemid;

        return $this;
    }

    /**
     * Get itemid
     *
     * @return integer
     */
    public function getItemid()
    {
        return $this->itemid;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     *
     * @return Logentry
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }
}
