<?php

namespace Greetik\SystemlogBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('systemlog');

        $rootNode
            ->children()
                ->scalarNode('appservice')->defaultValue('systemlog.tools')->end()
                ->scalarNode('interface')->defaultValue('SystemlogBundle:Log')->end()
                ->scalarNode('entityuser')->defaultValue('SystemuserBundle:User')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
