<?php

namespace Greetik\TasksBundle\Service;

use Greetik\TasksBundle\Entity\Task;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Taskstools {

    private $em;

    public function __construct($_entityManager) {
        $this->em = $_entityManager;
    }

    public function getTasksByTouser($touser) {
        return $this->em->getRepository('TasksBundle:Task')->findByTouser($touser);
    }

    public function getTasksByFromuser($fromuser) {
        return $this->em->getRepository('TasksBundle:Task')->findByFromuser($fromuser);
    }

    public function getTasksByUser($user, $from, $to) {
        return $this->em->getRepository('TasksBundle:Task')->findTasksByUser($user, $from, $to);
    }

    public function getTask($id) {
        $task = $this->em->getRepository('TasksBundle:Task')->findOneById($id);

        if (!$task)
            throw new \Exception('No se encuentra la tarea');
       
        return $task;
    }

    public function modifyTask($task) {
        $this->em->flush();
    }


    public function insertTask($task) {
        $this->em->persist($task);
        $this->em->flush();
    }

    public function deleteTask($task) {
        $this->em->remove($task);
        $this->em->flush();
    }

   
}
