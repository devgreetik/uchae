<?php

namespace Greetik\TasksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="Greetik\TasksBundle\Entity\TaskRepository")
 */
class Task
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=511)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="taskdate", type="date")
     */
    private $taskdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="viewed", type="boolean", nullable=true)
     */
    private $viewed;    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="done", type="boolean", nullable=true)
     */
    private $done;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="fromuser", type="integer")
     */
    private $fromuser;    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="touser", type="integer")
     */
    private $touser;    
    
    private $comments;
    
    public function setComments($comments){
        $this->comments = $comments;
    }

    public function getComments(){
        return $this->comments;
    }    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Task
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set taskdate
     *
     * @param \DateTime $taskdate
     *
     * @return Task
     */
    public function setTaskdate($taskdate)
    {
        $this->taskdate = $taskdate;

        return $this;
    }

    /**
     * Get taskdate
     *
     * @return \DateTime
     */
    public function getTaskdate()
    {
        return $this->taskdate;
    }

    /**
     * Set viewed
     *
     * @param boolean $viewed
     *
     * @return Task
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * Get viewed
     *
     * @return boolean
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set done
     *
     * @param boolean $done
     *
     * @return Task
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set fromuser
     *
     * @param integer $fromuser
     *
     * @return Task
     */
    public function setFromuser($fromuser)
    {
        $this->fromuser = $fromuser;

        return $this;
    }

    /**
     * Get fromuser
     *
     * @return integer
     */
    public function getFromuser()
    {
        return $this->fromuser;
    }

    /**
     * Set touser
     *
     * @param integer $touser
     *
     * @return Task
     */
    public function setTouser($touser)
    {
        $this->touser = $touser;

        return $this;
    }

    /**
     * Get touser
     *
     * @return integer
     */
    public function getTouser()
    {
        return $this->touser;
    }
}
