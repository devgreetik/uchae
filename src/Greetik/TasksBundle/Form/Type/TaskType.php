<?php

namespace Greetik\TasksBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaskType
 *
 * @author Paco
 */
class TaskType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        if (!empty($options['_date'])) $datadate = array('data'=>$options['_date']);
        else $datadate = array();    
        
        $builder
                ->add('name', TextType::class, array('required' => false))
                ->add('description', TextareaType::class, array('required' => false))
                ->add('taskdate', DatetimeType::class, array_merge($datadate, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy - HH:mm')))
                ->add('touser', HiddenType::class, array('data'=> empty($options['_touser']) ? '' : $options['_touser']));
    }

    public function getName() {
        return 'Task';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\TasksBundle\Entity\Task',
            '_date'=>null,
            '_touser'=>null
        ));
    }
}
