<?php

namespace Greetik\TasksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\TasksBundle\Entity\Task;
use Greetik\TasksBundle\Form\Type\TaskType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * Render all the alertuser of the user
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {
        /*
          return $this->render('TasksBundle:Task:index.html.twig', array(
          'data' => $this->get($this->getParameter('tasks.permsservice'))->getTasksByProject($idproject),
          'idproject' => $idproject,
          'insertAllow' => true
          )); */
    }

    public function insertformAction($touser = '', $id = 0, $date = '') {

        if (intval($id) != 0) {
            try {
                $task = $this->get($this->getParameter('tasks.permsservice'))->getTask($id);
                $date = $task->getTaskdate()->format('Y-m-d');
                $touser = $task->getTouser();
            } catch (\Exception $e) {
                throw $e;
                $task = new Task();
            }
        } else
            $task = new Task();

        if (!empty($date)) {
            $date = new \Datetime($date);
        }

        $newForm = $this->createForm(TaskType::class, $task, array('_touser' => $touser, '_date' => $date));
        return $this->render('TasksBundle:Task:modalinsert.html.twig', array(
                    'new_form' => $newForm->createView(),
                    'item' => $task,
                    'touser' => $touser
        ));
    }

    /* insert or edit a alertuser */

    public function insertAction(Request $request, $id = 0) {
        if (intval($id) != 0) {
            try {
                $task = $this->get($this->getParameter('tasks.permsservice'))->getTask($id);
            } catch (\Exception $e) {
                $task = new Task();
            }
        } else
            $task = new Task();

        $editForm = $this->createForm(TaskType::class, $task, array());
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            try {
                if ($task->getId()) {
                    $this->get($this->getParameter('tasks.permsservice'))->editTask($task);
                } else {
                    $task->setFromuser($this->getUser()->getId());
                    //$task->setProject($this->getUser()->getProjectadmin());
                    $this->get($this->getParameter('tasks.permsservice'))->insertTask($task);
                }
                return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
                /*
                  return new Response(json_encode(array('errorCode' => 0, "data" => $this->render('TasksBundle:Task:index.html.twig', array(
                  'alerts' => $this->get('gwadmin.alertusers')->getTaskssByTouser($task->getTouser(), $this->getUser()->getProjectadmin()),
                  'touser' => $task->getTouser()
                  ))->getContent())), 200, array('Content-Type' => 'application/json')); */
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, "errorDescription" => $e->getMessage() . ' -> ' . $e->getLine() . ' (' . $e->getFile() . ')')), 200, array('Content-Type' => 'application/json'));
            }
        } else {
            $errors = (string) $editForm->getErrors(true, false);
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
    }

    public function deleteAction(Request $request, $id) {

        $task = $this->get($this->getParameter('tasks.permsservice'))->getTask($id);
        if ($request->getMethod() == "POST") {
            try {
                $this->get("gwadmin.tasks")->deleteTask($task);
                return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
                /*
                  return new Response(json_encode(array('errorCode' => 0, "data" => $this->render('TasksBundle:Tasks:index.html.twig', array(
                  'alerts' => $this->get('gwadmin.alertusers')->getTaskssByTouser($task->getTouser(), $this->getUser()->getProjectadmin()),
                  'touser' => $task->getTouser()
                  ))->getContent())), 200, array('Content-Type' => 'application/json')); */
            } catch (\Exception $e) {
                return new Response(json_encode(array('errorCode' => 1, "errorDescription" => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
    }

    public function viewAction($id) {
        $task = $this->get($this->getParameter('tasks.permsservice'))->getTask($id);

        return $this->render('TasksBundle:Task:modalview.html.twig', array(
                    'item' => $task
        ));
    }

    public function markasdoneAction($id) {
        try {
            $this->get($this->getParameter('tasks.permsservice'))->setTaskdone($id);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, "errorDescription" => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
    }

}
