<?php

namespace Greetik\SystemuserBundle\Services;

use Greetik\SystemuserBundle\Entity\Systemuser;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Systemusertools {

    private $em;
    private $usermanager;
    private $_context;
    private $token;
    private $beinterfacetools;

    /*Los ROLES son: Admin, editor y user (Que será el ganadero)*/
    public function __construct($_entityManager, $_usermanager, $__context, $_token, $_beinterfacetools) {
        $this->roles = array('ROLE_SUPERADMIN'=>array('role'=>'ROLE_SUPERADMIN', 'name'=>'Superadmin', 'num'=>101), 'ROLE_ADMIN'=>array('role'=>'ROLE_ADMIN', 'name'=>'Administrador', 'num'=>100),    'ROLE_USER'=>array('role'=>'ROLE_USER', 'name'=>'Ganadero', 'num'=>98));

        $this->em = $_entityManager;
        $this->usermanager = $_usermanager;
        $this->_context = $__context;
        $this->token = $_token;
        $this->beinterfacetools = $_beinterfacetools;
    }

    /* Devuelve si el usuario tiene acceso al panel de administrador (Ser EDITOR al menos) */
    public function hasAccessToAdminPanel() {
        if (!$this->_context->isGranted('ROLE_ADMIN'))
            return false;

        return true;
    }

    /*Devuele si un usuario es ganadero*/
    public function isLowUser($user){
        $role = $this->grantrole($user['roles']);
        return $role == 'ROLE_USER';
    }
    
    /* Devuelve el nombre del rol */

    public function roletoname($role) {
        if (isset($this->roles[$role])) return $this->roles[$role]['name'];
        
        return '';
    }

    /* Devuelve el número del rol (cuanto más bajo menos permisos)*/

    protected function roletonumber($role) {
        if (isset($this->roles[$role])) return $this->roles[$role]['num'];
        
        return 0;
    }

    /* Devuelve el rol más alto de una lista */

    public function grantrole($roles) {
        foreach ($this->roles as $role)
        {
            if (in_array($role['role'], $roles))
            {
                return $role['role'];
            }
        }

        return false; // Unknown role?
    }

    //Devuelve el Rol del usuario conectado
    public function roleConnected(){
        return $this->grantrole($this->token->getToken()->getUser()->getRoles());
    }
    
    /*Devuelve si el usuario tiene ese rol*/
    public function isGrantedConnected($role){
        return ($this->_context->isGranted($role));
    }
    
    /*Devuelve el usuario conectado*/
    public function getUserConnected(){
        return $this->token->getToken()->getUser();
    }

    /* Devuelve true si el rol del usuario conectado es mayor o igual a los del parámetro */

    protected function isroleconnectedgrant($roles) {
        if ($this->_context->isGranted('ROLE_ADMIN'))
            return true;

        $connectedroles = $this->token->getToken()->getUser()->getRoles();
        $roleconnected = $this->grantrole($connectedroles);

        if ($this->roletonumber($roleconnected) >= $this->roletonumber($this->grantrole($roles)))
            return true;
        
        return false;
    }

    /* Devuelve los roles que están a la misma altura o por debajo de los pasados por parámetro */

    protected function getRolesEqualBelow($role, $withnames = false) {
        $rolenumber = $this->roletonumber($role);
        //if ($withnames) {
            $data = array();
            foreach ($this->roles as $role=>$roledata) {
                if ($rolenumber>=$this->roletonumber($role))
                    $data[$role] = $this->roletoname($role);
            }
            return $data;
        //}

        return $roles;
    }

    /* Devuelve los roles que están por debajo de los pasados por parámetro */

    protected function getRolesbelow($role, $withnames = false) {
        $roles = $this->getRolesEqualBelow($role, $withnames);
        if ($role == 'ROLE_ADMIN')
            return $roles;
        return array_slice($roles, 1);
    }

    /* Devuelve los roles a los que tiene acceso otro rol */

    public function getRolesAccess($roles, $withnames = false) {
        $role = $this->grantrole($roles);

        return $this->getRolesbelow($role, $withnames);
    }


    /* Dado el nombre del Rol devuelve su ID (ROLE_USER, ROLE_ADMIN...) */

    public function getRoleId($role) {
        foreach($this->roles as $r){
            if ($r['name']==$role) return $r['role'];
        }
        
        return '';
    }
    
    /* Permisos para operar con los usuarios */

    public function getSystemuserPerm($type, $systemuser = '') {
        //El admin tiene todos los accesos
        if ($this->_context->isGranted('ROLE_ADMIN'))
            return true;
        
        //Solo el usuario ROLE_USER no puede listar usuarios
        if ($type == 'list') {
            if (!$this->_context->isGranted('ROLE_ADMIN'))
                return false;
            return true;
        }

        //solo a partir de editor puedo ver los usuarios
        if ($type == 'view' || $type == 'viewinlist') {
            if ($type == 'view') {
                //El propio usuario puede verse (perfil)
                if (isset($systemuser['id']) && $systemuser['id'] == $this->token->getToken()->getUser()->getId())
                    return true;
            }
            if (!$this->_context->isGranted('ROLE_ADMIN'))
                return false;

            if ($this->isroleconnectedgrant($systemuser['roles']))
                return true;
            
            return false;
        }

        if ($type == 'insert') {
            //Admin, Editores
            if (!$this->_context->isGranted('ROLE_ADMIN'))
                return false;

            //Si tratamos de insertar sin objeto es que hacemos una comprobación previa
            if (empty($systemuser)) {
                return true;
            } else {
                return in_array($this->grantrole($systemuser->getRoles()), $this->getRolesAccess($this->token->getToken()->getUser()->getRoles()));
            }
        }

        //Con un rol mayor que el otro usario es suficiente
        if ($type == 'modify') {
            if (is_numeric($systemuser)) $systemuser = $this->getSystemuserObject ($systemuser);
            if (!$systemuser) return false;
            if ($systemuser->getId()==$this->token->getToken()->getUser()->getId()) return true;
            return in_array($this->grantrole($systemuser->getRoles()), $this->getRolesAccess($this->token->getToken()->getUser()->getRoles()));
        }

        //Con un rol mayor que el otro usario es suficiente
        if ($type == 'delete') {
            return in_array($this->grantrole($systemuser->getRoles()), $this->getRolesAccess($this->token->getToken()->getUser()->getRoles()));
        }

        return false;
    }

    /* Comprueba si ya existe el email en el sistema */

    protected function checkEmail($email, $id = '') {
        $systemuser = $this->usermanager->findUserByEmail($email);
        if ($systemuser) {
            if (!empty($id)) {
                if ($id == $systemuser->getId())
                    return true;
            }
            return false;
        }
        return true;
    }

    /* Comprueba si el nombre de usuario ya existe en el sistema */

    public function checkUsername($username, $id = '') {
        $systemuser = $this->usermanager->findUserByUsername($username);
        if ($systemuser) {
            if (!empty($id)) {
                if ($id == $systemuser->getId())
                    return true;
            }
            return false;
        }
        return true;
    }

    /* Devuelve el tipo de usuario en función de sus roles, si se le pone el parámetro $name a true lo devuelve en texto: "Ganadero" en vez de "ROLE_USER" */

    protected function getUserType($user, $name = false) {
        $role = $this->grantrole($user['roles']);
        if ($name)
            return $this->roletoname($role);

        return $role;
    }

    /* Recorre los usuarios haciendo las comprobaciones previas a listarlos */

    protected function getListusers($users, $farms=array()) {
        $data = array();
        foreach ($users as $user) {
            if (!$this->getSystemuserPerm('viewinlist', $user))
                continue;

            $data[$user['id']] = array_merge($user, 
                    array('farmname'=>((isset($farms[$user['farm']]))?$farms[$user['farm']]:$user['farm'])),
                    array('typename' => $this->getUserType($user, true)));
        }
        return $data;
    }

    /* obtener los usuarios filtrados, solo los que puede ver el usuario conectado *

    public function getSystemusersFiltered($hospitalServices = array(), $specialties = array(), $subspecialties = array(), $years = array(), $subspecialtieslist=array()) {
        return $this->getListusers($this->em->getRepository('SystemuserBundle:User')->getSystemusersFiltered($hospitalServices, $specialties, $subspecialties, $years), $subspecialtieslist);
    }
    
    /* obtener los usuarios, solo los que puede ver el usuario conectado */

    public function getSystemusers($farms=array()) {
        return $this->getListusers($this->em->getRepository('SystemuserBundle:User')->getSystemusers(), $farms);
    }

    /* obtener los ganaderos, solo los que puede ver el usuario conectado */

    public function getLowUsers($farms=array()) {
        return $this->getListusers($this->em->getRepository('SystemuserBundle:User')->getSystemusersByRole('ROLE_USER'), $farms);
    }
    
    /*Obtener los usuarios de una ganadería*/
    public function getSystemusersByFarm($farm){
        return $this->getListusers($this->em->getRepository('SystemuserBundle:User')->getSystemusersByFarm($farm));
    }
    
   
    /* Comprueba si los datos del usuario son correctos */

    protected function checkdataSystemuser($systemuser) {
        if ($systemuser->getId())
            $id = $systemuser->getId();
        else
            $id = '';

        if (count($systemuser->getRoles()) == 0)
            throw new \Exception('El tipo es obligatorio');

        if (!$this->checkEmail($systemuser->getEmail(), $id))
            throw new Exception('El email ya está en uso', 11);
        if (!$this->checkUsername($systemuser->getUsername(), $id))
            throw new Exception('El nombre de usuario ya está en uso');

        if ($this->grantrole($systemuser->getRoles()) == 'ROLE_USER') {
            if ($systemuser->getFarm() == NULL || $systemuser->getFarm() == 0)
                throw new \Exception('La ganadería es obligatoria para los ganaderos');
        }

        return true;
    }

    /* Inserta el objeto usuario */

    public function insertSystemuser($systemuser, $enable = false) {
        $this->checkdataSystemuser($systemuser);

        $systemuser->setCreatedat(new \DateTime());
        $systemuser->setEnabled($enable);
        
        
        if (!$this->getSystemuserPerm('insert', $systemuser))
            throw new Exception('No tiene permiso para insertar el usuario');

        $this->em->persist($systemuser);
        $this->em->flush();
    }

    /* Modifica el objeto usuario */

    public function modifySystemuser($systemuser) {
        $this->checkdataSystemuser($systemuser);
        
        if (!$this->getSystemuserPerm('modify', $systemuser))
            throw new Exception('No tiene permiso para modificar el usuario');

        $this->em->flush();
    }

    /* Obtiene un usuario */

    public function getSystemuser($id) {
        $systemuser = $this->em->getRepository('SystemuserBundle:User')->getSystemuser($id);
        if (!$this->getSystemuserPerm('view', $systemuser))
            throw new Exception('No tiene permiso para ver el usuario');
        
        return array_merge($systemuser, array('typename' => $this->getUserType($systemuser, true)), array('type' => $this->getUserType($systemuser, false)));
    }

    /* Obtiene un usuario como  objeto */

    public function getSystemuserObject($id) {
        return $this->em->getRepository('SystemuserBundle:User')->findOneBy(array('id' => $id));
    }

    /*Cambia el avatar de un usuario*/
    public function changeAvatar($file, $systemuser) {
        if (is_numeric($systemuser))
            $systemuser = $this->getSystemuserObject($systemuser);

        if ($systemuser->getId() != $this->token->getToken()->getUser()->getId()) {
            if (!$this->getSystemuserPerm('modify', $systemuser))
                throw new \Exception('No tienes permiso para cambiar el avatar de este usuario');
        }
        $this->beinterfacetools->changeAvatar($file, $systemuser, 'uploads/systemuser/');
    }

    /*Elimina el usuario, también su avatar*/
    public function deleteSystemuser($systemuser) {
        if (is_numeric($systemuser)) $systemuser = $this->getSystemuserObject ($systemuser);
        if (!$this->getSystemuserPerm('delete', $systemuser))
            throw new Exception('No tiene permiso para eliminar el usuario');
        $this->beinterfacetools->deleteAvatar($systemuser, 'uploads/systemuser/');
        $this->em->remove($systemuser);
        $this->em->flush();
    }
}
