<?php

namespace Greetik\SystemuserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationFormType extends AbstractType {


    public function buildForm(FormBuilderInterface $builder, array $options) {

        $roles = array();
        foreach($options['_roles'] as  $k=>$role){
            $roles[$role] = $k;
        }
        
        $farms = array();
        foreach($options['_farms'] as  $k=>$farm){
            $farms[$farm] = $k;
        }
        
        $builder
                ->add('name', TextType::class, array('label' => 'Nombre', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'form-control')))
                ->add('surname', TextType::class, array('label' => 'Apellidos', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'form-control')))
                ->add('username', TextType::class, array('label' => 'Usuario', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'form-control')))
                ->add('email', TextType::class, array('label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'form-control')))
                ->add('phone', TextType::class, array('required' => false, 'label' => 'Teléfono', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'form-control')))
                ->add('address', TextType::class, array('required' => false, 'label' => 'Dirección', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'form-control')))
                ->add('birthdate', DateType::class, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Fecha de Nacimiento', 'label_attr' => array('class' => 'control-label'), 'attr' => array('class' => 'pickdate form-control')))
                ->add('farm', ChoiceType::class, array( 'choices'=>$farms, 'required'=>false));
        
        $builder->add('roles', ChoiceType::class, array_merge(array(
            'choices' => $roles,
            'expanded' => false,
            'multiple' => false,
            'required'=>true,
            'mapped'=>false),
                ((!empty($options['_role'])?array('data'=>$options['_role']):array()))
        ));
        
    }

    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getName() {
        return 'systemuser_registration';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            '_role'=>'',
            '_roles'=> array(),
            '_farms'=> array()
        ));
    }

}
