<?php

namespace Greetik\SystemuserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends Controller {
    
    /*Mientras tenemos más opciones, redirigimos a su perfil*/
    public function indexAction(Request $request)
    {
        return $this->redirect($this->get('router')->generate('fos_user_profile_show'));
    }

    /* Cambiar el avatar del usuario conectado */
    public function changemyavatarAction(Request $request) {
        try {
            $this->get('systemuser.tools')->changeAvatar($request->files->all()['avatar'], $this->getUser()->getId());
        } catch (\Exception $e) {
            if ($e->getCode() == 11)
                $this->addFlash('warning', $e->getMessage());
            else
                $this->addFlash('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('fos_user_profile_show'));
    }
    
}
