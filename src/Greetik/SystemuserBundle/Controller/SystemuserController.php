<?php

namespace Greetik\SystemuserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\SystemuserBundle\Entity\User;
use Greetik\SystemuserBundle\Form\Type\RegistrationFormType;
use AppBundle\Form\Type\SystemuserinsertType;
use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use FOS\UserBundle\Form\Type\ResettingFormType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\Serializer;

class SystemuserController extends Controller {
    
    /* listar usuarios */
    public function indexAction() {
        $data = $this->get('systemuser.tools')->getSystemusers();

        return $this->render('SystemuserBundle:Systemuser:index.html.twig', array(
                    'data' => $data,
                    'insertAllow' => $this->get('systemuser.tools')->getSystemuserPerm('insert')
        ));
    }
    
    /* listar farmers */
    public function farmersAction() {
        $data = $this->get('app.systemusers')->getFarmers();

        return $this->render('SystemuserBundle:Farmer:index.html.twig', array(
                    'data' => $data,
                    'insertAllow' => $this->get('systemuser.tools')->getSystemuserPerm('insert')
        ));
    }
    
    /* obtiene por búsqueda una lista en formato array('text'=>nombre, 'id'=>id) */

    public function getforselectAction(Request $request) {
        if ($request->getMethod() != 'POST')
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
        try {
            $data = array();
            foreach ($this->get('systemuser.tools')->getSystemusersBySearch($request->get('q')) as $k => $v) {
                $data[] = array('text' => $v['username'], 'id' => $v['id']);
            }
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 0, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 0, 'data' => $data)), 200, array('Content-Type' => 'application/json'));
    }    
    
    /* insertar un usuario */

    public function insertAction(Request $request, $role='') {
        if (!empty($role)){
            $role = $this->get('systemuser.tools')->getRoleId($role);
        }
            
        $systemuser = $this->get('fos_user.user_manager')->createUser();
        
        $newForm = $this->createForm(SystemuserinsertType::class, $systemuser, array('_farms'=>$this->get('farms.tools')->getFarms(true), '_role'=>$role, '_roles'=>$this->get('systemuser.tools')->getRolesAccess($this->getUser()->getRoles(), true)));
        //$newForm = $this->createForm(RegistrationFormType::class, $systemuser, array('_role'=>$role, '_roles'=>$this->get('systemuser.tools')->getRolesAccess($this->getUser()->getRoles(), true)));

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);

            if ($newForm->isValid()) {
                try {
                    $systemuser->setRoles(array($newForm->get('roles')->getData()));
                    $this->get('app.systemusers')->insertSystemuser($systemuser, true, $newForm->get('farm_name')->getData(), $newForm->get('farm_abb')->getData(), $newForm->get('farm_rega')->getData(), $newForm->get('farm_description')->getData());
                    //$this->get('systemuser.tools')->insertSystemuser($systemuser, true);
                } catch (\Exception $e) {
                        $newForm->addError(new FormError($e->getMessage()));
                        return $this->render('SystemuserBundle:Systemuser:insert.html.twig', array('role'=>$role, 'genpass' => $this->get('beinterface.tools')->generateRandomPassword(), 'genuser' => $systemuser->getUsername(), 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('systemuser_listsystemusers', array()));
            }else {
                $this->addFlash('error', $newForm->getErrors(true, false));
                return $this->render('SystemuserBundle:Systemuser:insert.html.twig', array('role'=>$role,'genpass' => $this->get('beinterface.tools')->generateRandomPassword(), 'genuser' => $systemuser->getUsername(), 'new_form' => $newForm->createView()));
            }
        }

        return $this->render('SystemuserBundle:Systemuser:insert.html.twig', array('role'=>$role,'genpass' => $this->get('beinterface.tools')->generateRandomPassword(), 'genuser' => $this->get('beinterface.tools')->generateRandomUsername(rand(6, 12)), 'new_form' => $newForm->createView()));
    }
    
    

    /*ver los datos de un usuario*/    
    public function viewAction($id) {
        $systemuserobject = $this->get('systemuser.tools')->getSystemuserObject($id);

        $editForm = $this->createForm(RegistrationFormType::class, $systemuserobject, array('_farms'=>$this->get('farms.tools')->getFarms(true), '_role'=>$this->get('systemuser.tools')->grantrole($systemuserobject->getRoles()), '_roles'=>$this->get('systemuser.tools')->getRolesAccess($this->getUser()->getRoles(), true)));
        $changepassform = $this->createForm(ChangePasswordFormType::class);
        
        
        $systemuser = $this->get('app.systemusers')->getSystemuser($id);
        return $this->render('AppBundle:Systemuser:view.html.twig', array(
                    'item' => $systemuser,
                    'new_form' => $editForm->createView(),
                    'new_form_pass' => $changepassform->createView(),
                    'modifyAllow' => $this->get('systemuser.tools')->getSystemuserPerm('modify', $systemuserobject)
        ));
    }
    
    /*modificar los datos de un usuario*/
    public function modifyAction(Request $request, $id) {
        $systemuserobject = $this->get('systemuser.tools')->getSystemuserObject($id);
        if (!$this->get('systemuser.tools')->getSystemuserPerm('modify', $systemuserobject)){
            $this->addFlash('error', 'No tienes permiso para modificar el usuario');
            return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id' => $id)));
        }
        
        $editForm = $this->createForm(RegistrationFormType::class, $systemuserobject, array('_farms'=>$this->get('farms.tools')->getFarms(true), '_roles'=>$this->get('systemuser.tools')->getRolesAccess($this->getUser()->getRoles(), true)));
        
        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                try {
                    
                        $systemuserobject->setRoles(array($editForm->get('roles')->getData()));
                        $this->get('systemuser.tools')->modifySystemuser($systemuserobject, true);
                } catch (\Exception $e) {
                        $this->addFlash('error', $e->getMessage());
                }
                return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id' => $id)));
            }
        }

        $this->addFlash('error', $editForm->getErrors(true, false));
        return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id' => $id)));
    }


    /* cambiar avatar */

    public function changeavatarAction(Request $request, $id) {
        try {
            $this->get('systemuser.tools')->changeAvatar($request->files->all()['avatar'], $id);
        } catch (\Exception $e) {
            if ($e->getCode() == 11)
                $this->addFlash('warning', $e->getMessage());
            else
                $this->addFlash('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id' => $id)));
    }  
    
    /* cambiar avatar del usuario conectado */

    public function changemyavatarAction(Request $request) {
        try {
            $this->get('systemuser.tools')->changeAvatar($request->files->all()['avatar'], $this->getUser()->getId());
        } catch (\Exception $e) {
            if ($e->getCode() == 11)
                $this->addFlash('warning', $e->getMessage());
            else
                $this->addFlash('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('fos_user_profile_show'));
    }      
    
    
    public function deleteAction(Request $request){
        $id = $request->get('id');
        if ($request->getMethod()!='POST'){
            $this->addFlash('error', 'No tiene permiso para hacer esta operación.');
            return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id' => $id)));
        }
        
        try{
            $this->get('systemuser.tools')->deleteSystemuser($id);
        }catch(\Exception $e){
            $this->addFlash('error', $e->getMessage());
            return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id' => $id)));
        }
            
        return $this->redirect($this->generateUrl('systemuser_listsystemusers'));
    }
    
    /*exportar farmers*/
    public function exportExcelAction(Request $request, $ids=''){
        // ask the service for a excel object
       $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

       $phpExcelObject->getProperties()->setCreator("EBS")
           ->setLastModifiedBy("EBS")
           ->setTitle("Farmers - ".date('d/m/Y'))
           ->setSubject("Exportación de los farmers");
       
       $this->get('app.systemusers')->exportFarmers($phpExcelObject, explode(',', $ids));

       $phpExcelObject->getActiveSheet()->setTitle('Farmers');
       // Set active sheet index to the first sheet, so Excel opens this as the first sheet
       $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'farmers.xlsx'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;        
    }
    
    
    
}
